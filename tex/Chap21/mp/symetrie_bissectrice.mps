%!PS
%%BoundingBox: -22 -22 171 108 
%%HiResBoundingBox: -21.50984 -21.50984 170.32874 107.30116 
%%Creator: MetaPost 2.000
%%CreationDate: 2018.02.20:1432
%%Pages: 1
%*Font: cmmi5 4.98132 4.98132 4d:8000000001
%*Font: cmr5 4.98132 4.98132 28:c
%*Font: cmsy5 4.98132 4.98132 00:8
%%BeginProlog
%%EndProlog
%%Page: 1 1
 1 0 0 setrgbcolor 0 2 dtransform truncate idtransform setlinewidth pop
 [] 0 setdash 1 setlinecap 1 setlinejoin 10 setmiterlimit
newpath 17.00761 106.30116 moveto
18.0684 99.14685 19.48732 92.05093 21.25984 85.03937 curveto
22.46921 80.25447 23.84206 75.51045 25.51207 70.8657 curveto
26.75128 67.41864 28.15138 64.03061 29.7643 60.7412 curveto
31.04243 58.13432 32.45163 55.59363 34.01653 53.1483 curveto
35.32515 51.10394 36.74083 49.129 38.26875 47.24294 curveto
39.60333 45.59564 41.0216 44.01776 42.52098 42.51839 curveto
43.87633 41.16304 45.2959 39.87389 46.77321 38.65285 curveto
48.14478 37.5194 49.56435 36.44627 51.02544 35.43155 curveto
52.40868 34.47133 53.82825 33.56302 55.27766 32.70595 curveto
56.66998 31.88263 58.08891 31.10536 59.52989 30.37027 curveto
60.92871 29.6566 62.34698 28.98314 63.78212 28.34537 curveto
65.18547 27.72188 66.60374 27.13213 68.03435 26.5735 curveto
69.44159 26.02397 70.85922 25.50429 72.28658 25.01054 curveto
73.69577 24.52264 75.11404 24.0607 76.5388 23.62082 curveto
77.94994 23.18547 79.36821 22.77155 80.79103 22.37772 curveto
82.20346 21.9865 83.6211 21.61473 85.04326 21.25919 curveto
86.457 20.9056 87.87462 20.56888 89.29549 20.24641 curveto
90.70987 19.92526 92.1275 19.61967 93.54771 19.32642 curveto
94.96275 19.03445 96.38037 18.75484 97.79994 18.48622 curveto
99.21562 18.21828 100.6326 17.96199 102.05217 17.71545 curveto
103.46785 17.46956 104.88548 17.23404 106.3044 17.00696 curveto
107.72073 16.78053 109.13771 16.56319 110.55663 16.35298 curveto
111.97296 16.14342 113.39058 15.94164 114.80885 15.747 curveto
116.22519 15.55301 117.6428 15.36615 119.06108 15.18513 curveto
120.47806 15.00412 121.89503 14.82959 123.31331 14.6609 curveto stroke
 0 0 0 setrgbcolor 0 0.5 dtransform truncate idtransform setlinewidth pop
 [3 3 ] 0 setdash
newpath -10.62993 -10.62993 moveto
85.03937 85.03937 lineto stroke
24.25984 83.79402 moveto
(M) cmmi5 4.98132 fshow
31.19914 83.79402 moveto
(\() cmr5 4.98132 fshow
33.89734 83.79402 moveto
(t) cmmi5 4.98132 fshow
36.70514 83.79402 moveto
(\)) cmr5 4.98132 fshow
 0 3 dtransform truncate idtransform setlinewidth pop [] 0 setdash
newpath 21.25984 85.03937 moveto 0 0 rlineto stroke
74.76932 14.52385 moveto
(M) cmmi5 4.98132 fshow
81.70862 14.52385 moveto
(\() cmr5 4.98132 fshow
84.40681 14.52385 moveto
(\000) cmsy5 4.98132 fshow
89.80331 14.52385 moveto
(t) cmmi5 4.98132 fshow
92.61111 14.52385 moveto
(\)) cmr5 4.98132 fshow
newpath 85.03937 21.25984 moveto 0 0 rlineto stroke
 0.5 0 dtransform exch truncate exch idtransform pop setlinewidth
newpath 0 -4.25223 moveto
0 4.25223 lineto stroke
newpath 42.51968 -4.25223 moveto
42.51968 4.25223 lineto stroke
newpath 85.03937 -4.25223 moveto
85.03937 4.25223 lineto stroke
newpath 127.55905 -4.25223 moveto
127.55905 4.25223 lineto stroke
newpath 170.07874 -4.25223 moveto
170.07874 4.25223 lineto stroke
 0 0.5 dtransform truncate idtransform setlinewidth pop
newpath -4.25223 0 moveto
4.25223 0 lineto stroke
newpath -4.25223 42.51968 moveto
4.25223 42.51968 lineto stroke
newpath -4.25223 85.03937 moveto
4.25223 85.03937 lineto stroke
newpath -21.25984 0 moveto
170.07874 0 lineto stroke
newpath 166.38338 -1.5307 moveto
170.07874 0 lineto
166.38338 1.5307 lineto
 closepath
gsave fill grestore stroke
 0.5 0 dtransform exch truncate exch idtransform pop setlinewidth
newpath 0 -21.25984 moveto
0 106.29921 lineto stroke
 0 0.5 dtransform truncate idtransform setlinewidth pop
newpath 1.5307 102.60385 moveto
0 106.29921 lineto
-1.5307 102.60385 lineto
 closepath
gsave fill grestore stroke
showpage
%%EOF
