\objectif{Dans ce chapitre, on va étudier et représenter des fonctions à valeurs dans $\R^n$. Ces courbes sont très souvent utilisées en Physique et en SI (par exemple, pour représenter la trajectoire d'un objet en fonction du temps).}

\textit{La liste ci-dessous représente les éléments à maitriser absolument. Pour cela, il faut savoir refaire les exemples et exercices du cours, ainsi que ceux de la feuille de TD.}

\begin{numerote}
	\item Connaître les notions générales  :
			\begin{itemize}[label=\textbullet]
				\item \hyperref[objectif-21-1]{connaître la définition de fonctions vectorielles, et les propriétés (dérivabilité, $\vdots$)}\dotfill $\Box$
				\item \hyperref[objectif-21-2]{connaître la définition du vecteur vitesse, du vecteur accélération et comment les obtenir}\dotfill $\Box$
				\item \hyperref[objectif-21-3]{connaître la définition de tangente, de vecteur tangent et de droite normale}\dotfill $\Box$
				\item \hyperref[objectif-21-4]{savoir obtenir un vecteur tangent et un vecteur normal}\dotfill $\Box$
			\end{itemize}
	\item Savoir étudier une courbe paramétrée :
			\begin{itemize}[label=\textbullet]
				\item \hyperref[objectif-21-5]{utiliser les symétries pour réduire l'intervalle d'étude}\dotfill $\Box$
				\item \hyperref[objectif-21-6]{déterminer les branches infinies lorsqu'il y en a}\dotfill $\Box$
				\item \hyperref[objectif-21-7]{savoir dresser un tableau de variations conjointes}\dotfill $\Box$
				\item \hyperref[objectif-21-8]{savoir tracer une courbe paramétrée en utilisant toutes les propriétés obtenues}\dotfill $\Box$
			\end{itemize}
	\item \hyperref[objectif-21-9]{Savoir calculer la longueur d'une courbe}\dotfill $\Box$
\end{numerote}

\newpage 

L'idée de ce chapitre est de représenter des courbes paramétrées, et de faire le lien avec la représentation du mouvement en physique, où l'on récupère régulièrement non pas $y$ en fonction de $x$, mais $x$ et $y$ en fonction du temps $t$.

Dans l'ensemble de ce chapitre, et quand c'est nécessaire, le plan est muni d'un repère orthonormé $(O,\vv{i},\vv{j})$.

\section{Généralités}

	\subsection{Fonctions vectorielles}
	
\label{objectif-21-1}
\definition{On appelle \textbf{fonction vectorielle} une fonction définie sur un intervalle de $\R$ et à valeurs dans $\R^n$. Par exemple, une fonction vectorielle du plan est une fonction définie sur un intervalle $I$ de $\R$ et à valeur dans $\R^2$.
}

\remarque{\danger~Si $f \colon I\to\R^2$ est une fonction vectorielle du plan, $f(t)$ (pour $t\in I$) est un point. En général, on note, pour tout $t\in I$, $x(t)$ l'abscisse et $y(t)$ l'ordonnée du point $f(t)$.}

\definition{Soit $f \colon I\to \R^2$ une fonction vectorielle du plan. Pour tout $t\in I$, on note $(x(t), y(t))$ les coordonnées du point $f(t)$. Alors, on appelle \textbf{courbe paramétrée} par $f$ l'ensemble des points 
\[ \Gamma=\left \{ (x(t), y(t)),\quad t \in I \right \} \]
% FIGURE %
On dit également que $\Gamma$ est le \textbf{support} de $f$, et $f$ est un \textbf{paramétrage} de $\Gamma$.}


\exemple{[Courbe d'une fonction] Si $f \colon I \to \R$ est une fonction à valeur réelle, son support est $\left \{ (x, f(x)),\quad x\in I\right\}$,  c'est-à-dire que son support est paramétrée par la courbe
\[ g\colon \begin{array}{lcl} I&\to& \R^2\\t &\mapsto& \systeme*{x(t)=t,y(t)=f(t)} \end{array} \]
}

\exemple{[Droite et cercle] On a vu dans le chapitre de géométrie dans le plan qu'une droite peut être paramétrée par la fonction $f \colon \R \to \R^2$ définie par
\[ f\colon t \mapsto  \systeme*{x(t)=x_0+at,y(t)=y_0+bt} \]
 où $\matrice{a\\b}$ désigne un vecteur directeur de la droite, et $(x_0,y_0)$ un point quelconque de la droite.
 
 De même, le cercle de centre $(a,b)$ et de rayon $r$ peut être paramétrée par la fonction $g \colon \R\to \R^2$ définie par 
\[ g\colon t \mapsto \systeme*{x(t)=a+r\cos(t),y(t)=b+r\sin(t)} \]	
}

\remarque{Un même support peut avoir plusieurs paramétrages possibles : par exemple, une droite admet une infinité de paramétrage (on peut par exemple changer le vecteur directeur par un autre - colinéaire au premier).
}

	\subsection{Vecteur vitesse, vecteur accélération}
	
\remarque{Etudier la continuité (respectivement la dérivabilité, ou les limites) d'une fonction vectorielle, c'est équivalent à étudier la continuité (resp. la dérivabilité ou les limites) de chacune des fonctions coordonnées, qui elles sont à valeurs réelles.}

\remarque{L'ensemble des fonctions $f \colon I\to \R^n$ est un espace vectoriel.}

\exemple{Si $f \colon t\mapsto (x(t),y(t))$ est une fonction vectorielle du plan, $f$ est continue en $t$ si et seulement si $x$ est continue en $t$ et $y$ est continue en $t$.

$f$ est dérivable en $t$ si et seulement si $x$  et $y$ sont dérivables en $t$. Dans ce cas, $f'(t)=(x'(t), y'(t))$. On constate alors que $f'(t)$ peut être vu comme les coordonnées d'un vecteur.}

\label{objectif-21-2}
\definition{[Vecteur vitesse] Soit $f \colon I\to \R^n$ une fonction vectorielle, et $t_0\in I$. Pour tout $t\in I$, on note $M(t)$ le point de paramètre $t$, c'est-à-dire de coordonnées $f(t)$ (tel que $\vv{OM}(t)=f(t)$). 
	On suppose que $f$ est dérivable en $t_0$. On appelle \textbf{vecteur vitesse} de $f$ en $t_0$ le vecteur $f'(t_0)$.

	Si $f'(t_0)=\vv{0}$, on dit que le point $M(t_0)$ est \textbf{stationnaire} ou singulier. Sinon, on dit qu'il est \textbf{régulier}.
}

\remarque{\danger~Attention : $f'(t_0)$ est un vecteur. Si $f$ est une fonction vectorielle du plan, $f'(t_0) = (x'(t_0), y'(t_0))$.}

\definition{[Vecteur accélération] Soit $f \colon I\to \R^n$ une fonction vectorielle, et $t_0\in I$. 
	On suppose que $f$ est deux fois dérivable en $t_0$. On appelle \textbf{vecteur accélération} de $f$ en $t_0$ le vecteur $f''(t_0)$.
}

\exemple{Soit $f \colon \R\to \R^2$ définie par \[ f \colon  t \mapsto \systeme*{x(t)=a+r\cos(t),y(t)=b+r\sin(t)} \]
Donner vecteur vitesse et vecteur accélération de $f$ au point $t\in \R$. Montrer que dans ce cas, pour tout réel $t$, $f'(t)$ et $f''(t)$ sont orthogonaux, et que $t\mapsto ||f'(t)||$ est constante.}

\begin{prof}
\solution{Remarquons que $x$ et $y$ sont de classe $\CC^\infty$ sur $\R$. Ainsi, $f$ possède un vecteur vitesse et un vecteur accélération en tout point $t \in \R$, et on a
\[ f'(t)=\left ( -r\sin(t),r\cos(t) \right) \qeq  f''(t)=\left ( -r\cos(t),-r\sin(t) \right)\]
On a alors, pour tout $t \in \R$
\[ f'(t)\cdot f''(t)=-r\sin(t)(-r\cos(t))+r\cos(t)(-r\sin(t)) = 0 \]
et
\[ ||f'(t)||=\sqrt{(-r\sin(t))^2+(r\cos(t))^2}=\sqrt{r^2(\cos^2(t)+\sin^2(t))}=r \]
}	
\end{prof}

\lignes{12}

\discipline{En cinétique, une fonction vectorielle peut être vue comme le vecteur position d'un point mobile en fonction du temps. Le support est alors la trajectoire de ce point, et c'est cela qui justifie la terminologie de vecteur vitesse et de vecteur accélération.\\
  Le mouvement sera uniforme si $t\mapsto ||f'(t)||$ est constant, et rectiligne si le support est une droite.}
  
 	\subsection{Tangente et vecteur tangent}

L'idée est d'étendre la notion de tangente connue dans le cas d'une fonction $f \colon \R\to \R$ pour le cas des fonctions vectorielles du plan.

\label{objectif-21-3}
\definition{Soit $f \colon I\to\R^2$ une fonction vectorielle du plan. On note, pour tout $t\in I$ $f((t)=(x(t),y(t))$. Soit $M(t_0)$ le point de paramètre $t_0$ de la courbe $\Gamma$ paramétrée par $f$. 

On s'intéresse à la limite 
\[ \lim_{t\to t_0} \frac{y(t)-y(t_0)}{x(t)-x(t_0)} \]
\begin{itemize}[label=\textbullet]
	\item Si cette limite existe et est finie, alors la droite d'équation $y=a(x-x(t_0))+y(t_0)$ (où $a$ représente la limite précédente) est appelée la \textbf{tangente} à $\Gamma$ en $M(t_0)$.
	\item Si cette limite est infinie, la droite d'équation $x=x(t_0)$ est la tangente (verticale) à $\Gamma$ en $M(t_0)$.
\end{itemize}

On appellera alors \textbf{normale} à $\Gamma$ au point $M(t_0)$ la droite, passant par $M(t_0)$ est perpendiculaire à la tangente en $\Gamma$ en $M(t_0)$.

% FIGURE %
}

On peut obtenir rapidement un vecteur directeur de la tangente en dérivant simplement :
\label{objectif-21-4}
 
\begin{proposition}
Soit $f \colon I\to \R^2$ une fonction vectorielle du plan suffisamment dérivable. Soit $t_0\in I$. Un vecteur directeur de la tangente à la courbe paramétrée par $f$ au point de paramètre $t_0$ est $f'(t_0)=(x'(t_0),y'(t_0))$ si celui-ci n'est pas nul, ou bien $f^{(n)}(t_0)$ si celui-ci n'est pas nul mais que \\$f'(t_0), f''(t_0),\cdots, f^{(n-1)}(t_0)$ sont nuls.

On appelle \textbf{vecteur tangent} tout vecteur directeur de la tangente.	
\end{proposition}


\preuve{Cela repose sur les développements limités : on écrit le développement limité de $x(t)$ et $y(t)$ au voisinage de $t_0$ et on cherche la première fois où l'un des coefficients n'est pas nul.}

\exemple{Soit $f \colon \R\to \R^2$ définie par 
\[ f \colon  t \mapsto \systeme*{x(t)=\cos^2(t),y(t)=\sin^2(t)} \]
Déterminer l'équation de la tangente au point $M\left(\frac{\pi}{4}\right)$. Déterminer un vecteur normal en ce point.
}

\begin{prof}
\solution{$f$ est de classe $\CC^\infty$ sur $\R$ et on a, pour tout $t\in \R$,
\[ f'(t)= \systeme*{x'(t)=-2\sin(t)\cos(t),y'(t)=2\cos(t)\sin(t)} \]
On a alors $f'\left(\frac{\pi}{4}\right)=(-1,1)$. Ainsi, $(-1,1)$ est un vecteur directeur de la tangente, et donc $\frac{1}{-1}=-1$ est le coefficient directeur. L'équation de la tangente au point $M\left(\frac{\pi}{4}\right)$ est donc $y=-\left(x-x\left(\frac{\pi}{4}\right)\right)+y\left(\frac{\pi}{4}\right) = -x+1$.
}	
\end{prof}

\lignes{8}

\section{Etude d'une courbe paramétrée du plan}

Pour étudier une courbe paramétrée du plan, il faut tout d'abord déterminer l'ensemble des éléments caractéristiques de la courbe : variations, symétrie,...

Dans l'ensemble de cette partie, on se donne $f \colon I\to \R^2$ une fonction vectorielle, dont les coordonnées sont notées $(x(t), y(t))$ pour $t\in I$, et on note $\Gamma$ son support. On note $M(t)$ le point de coordonnées $(x(t), y(t))$ pour $t\in I$.

	\subsection{Symétries}\label{objectif-21-5}
	
On va utiliser les symétries des fonctions $x$ et $y$ pour en déduire des symétries de $\Gamma$. 

\begin{proposition}
On suppose que $I$ est symétrique par rapport à $0$.
\begin{itemize}[label=\textbullet]
	\item Si, pour tout $t \in I$, $x(-t)=x(t)$ et $y(-t)=y(t)$, alors les points $M(t)$ et $M(-t)$ sont confondus.
	\item Si, pour tout $t \in I$, $x(-t)=-x(t)$ et $y(-t)=y(t)$, alors les points $M(t)$ et $M(-t)$ sont symétriques par rapport à l'axe des ordonnées.
		\begin{center}\includegraphics{tex/Chap21/mp/symetrie_ordonnee.mps}\end{center}
	\item Si, pour tout $t \in I$, $x(-t)=x(t)$ et $y(-t)=-y(t)$, alors les points $M(t)$ et $M(-t)$ sont symétriques par rapport à l'axe des abscisses.
		\begin{center}\includegraphics{tex/Chap21/mp/symetrie_abscisse.mps}\end{center}
	\item Si, pour tout $t \in I$, $x(-t)=-x(t)$ et $y(-t)=-y(t)$, alors les points $M(t)$ et $M(-t)$ sont symétriques par rapport à l'origine.
		\begin{center}\includegraphics{tex/Chap21/mp/symetrie_origine.mps}\end{center}
	\item Si, pour tout $t \in I$, $x(-t)=y(t)$ et $y(-t)=x(t)$, alors les points $M(t)$ et $M(-t)$ sont symétriques par rapport à la droite $y=x$.
		\begin{center}\includegraphics{tex/Chap21/mp/symetrie_bissectrice.mps}\end{center}
	\item Si, pour tout $t \in I$, $x(-t)=-y(t)$ et $y(-t)=-x(t)$, alors les points $M(t)$ et $M(-t)$ sont symétriques par rapport à la droite $y=-x$.
		\begin{center}\includegraphics{tex/Chap21/mp/symetrie_bissectrice2.mps}\end{center}
\end{itemize}
Pour chacun des cas précédents, on peut réduire l'intervalle d'étude en ne l'étudiant que sur la partie positive de $I$.	
\end{proposition}

On peut également utiliser les périodicités pour réduire l'intervalle d'étude.

\begin{proposition}
Si $x$ et $y$ sont périodiques de période $T$, il suffit d'étudier $x$ et $y$ sur un intervalle de longueur $T$.	
\end{proposition}


\exemple{Si $x$ et $y$ sont $2\pi$-périodiques, il suffit de les étudier sur $[0;2\pi]$, ou sur $[-\pi;\pi]$.}

Enfin, on peut utiliser des symétries lorsqu'on a réduit suffisamment l'intervalle.

\begin{proposition}
Si on a restreint l'étude de $x$ et $y$ sur l'intervalle $[a;b]$, on peut calculer $x(a+b-t)$ et $y(a+b-t)$. Les résultats obtenus permettent d'obtenir d'autres symétriques en se ramenant aux symétries vues plus haut.	
\end{proposition}


On obtient ainsi la méthode suivante :
\begin{methode}
Pour déterminer l'intervalle d'étude de $x$ et $y$ :
\begin{numerote}
	\item On déterminer la périodicité minimale commune de $x$ et $y$.
	\item On utilise les parités pour réduire l'intervalle d'étude.
	\item Enfin, on utilise la dernière proposition jusqu'à ce qu'il ne soit plus possible de réduire l'intervalle d'étude.	
\end{numerote}	
\end{methode}


\exemple{[Astroïde] Soit $f \colon \R\to \R^2$ la fonction vectorielle définie  pour tout $t$ par 
\[ f(t)= \systeme*{x(t)=\cos^3(t),y(t)=\sin^3(t)} \]
Déterminer l'intervalle d'étude minimal.}

\begin{prof}
\solution{Par $2\pi$-périodicité de $\cos$ et $\sin$, $x$ et $y$ sont $2\pi$-périodiques. On peut donc étudier $x$ et $y$ sur $[-\pi;\pi]$.

De plus, $x$ et $y$ sont définies sur $\R$, et pour tout réel $t$, $x(-t)=x(t)$ et $y(-t)=-y(t)$. Ainsi, la courbe est symétrique par rapport à l'axe des abscisses, et on peut restreindre l'intervalle à $[0;\pi]$.

Remarquons que $x(\pi-t)=-x(t)$ et $y(\pi-t)=y(t)$. Ainsi, la courbe est symétrique par rapport à l'axe des ordonnées. On restreint l'intervalle à $\left[0;\frac{\pi}{2}\right]$.

Enfin, $x\left(\frac{\pi}{2}-t\right)=y(t)$ et $y\left(\frac{\pi}{2}-t\right)=x(t)$. La courbe est symétrique par rapport à la droite $y=x$, et on peut restreindre l'intervalle à $\left[0;\frac{\pi}{4}\right]$.
 }	
\end{prof}

\lignes{18}

	\subsection{Branches infinies}\label{objectif-21-6}

La méthode de détermination des branches infinies est la même que celle connue dans le cas d'une fonction à valeurs réelles, exceptée qu'il faut tenir compte de $x$ et $y$ simultanément.

\definition{Soit $t_0$ une extrémité de $I$ (éventuellement infinie). Alors
\begin{itemize}[label=\textbullet]
	\item la courbe paramétrée $f$ admet une branche infinie en $t_0$ si et seulement si \[\lim_{t\to t_0} ||f(t)||=+\infty\]
	\item la droite $(D)$ est asymptote à la courbe de $f$ en $t_0$ si et seulement si \[ \lim_{t\to t_0} d(M(t), (D)) = 0 \]
	ce qui est équivalent à, si $(D):ax+by+c=0$ :
	\[ \lim_{t\to t_0} ax(t)+by(t)+c=0 \] 
\end{itemize}
}
 
\begin{methode}
La méthode est similaire à celle déjà vue :
\begin{itemize}[label=\textbullet]
	\item Si $\ds{\lim_{t\to t_0} x(t)=\pm \infty}$ et $\ds{\lim_{t\to t_0} y(t)=y_0}$, la courbe admet la droite $y=y_0$ comme asymptote horizontale.
	\item Si $\ds{\lim_{t\to t_0} x(t)=x_0}$ et $\ds{\lim_{t\to t_0} y(t)=\pm \infty}$, la courbe admet la droite $x=x_0$ comme asymptote verticale.
	\item Si $\ds{\lim_{t\to t_0} x(t)=\pm \infty}$ et $\ds{\lim_{t\to t_0} y(t)=\pm \infty}$ :
		\begin{itemize}[label=$\diamond$ ]
			\item si $\ds{\lim_{t\to t_0} \frac{y(t)}{x(t)} = 0}$, la courbe admet une branche parabolique de direction l'axe des abscisses.
			\item si $\ds{\lim_{t\to t_0} \frac{y(t)}{x(t)} = \pm \infty}$, la courbe admet une branche parabolique de direction l'axe des ordonnées.
			\item si $\ds{\lim_{t\to t_0} \frac{y(t)}{x(t)} = a\in \R^*}$ :
				\begin{itemize}
					\item la courbe admet une asymptote oblique d'équation $y=ax+b$ si $\ds{\lim_{t\to t_0} y(t)-ax(t)=b}$.
					\item la courbe admet une branche parabolique de direction la droite $y=ax$ si \[\lim_{t\to t_0} y(t)-ax(t)=\pm \infty\] 
				\end{itemize}
		\end{itemize}
\end{itemize} 	
\end{methode}

	\subsection{Variations conjointes}\label{objectif-21-7}
	
Une fois l'intervalle d'étude réduit, on étudie les fonctions $x$ et $y$ sur cet intervalle, et on représente les variations sur un même tableau de variations du type :

\[ \begin{array}{|c|ccc|}\hline 
  t & 0 & &\cdots\\\hline 
  x'(t) & & +&\cdots \\\hline 
  x(t) & 0 & \nearrow &\cdots \\\hline 
  y(t) & 1 & \searrow &\cdots\\\hline 
  y'(t) &  & -&\cdots\\\hline 
  \text{Tangente} & & &\cdots	\\\hline 
 \end{array} \]

Pour chaque point important (extrémité ou point d'annulation d'une des dérivées), on exprime un vecteur tangent, pour connaître la direction de la courbe.


\exemple{Dresser le tableau de variations conjointes de l'astroïde.}

\begin{prof}
\solution{D'après l'étude précédente, on dresse le tableau de variations sur $\left[0;\frac{\pi}{4}\right]$. $x$ et $y$ sont de classe $\CC^\infty$ sur $\R$ et on a, pour $t\in \left[0;\frac{\pi}{4}\right]$ :
\[ x'(t)=-3\sin(t)\cos^2(t) \qeq y'(t)=3\cos(t)\sin^2(t) \]
Sur $\left]0;\frac{\pi}{4}\right]$, $x'(t)< 0$ et $y'(t)> 0$. 

En $\frac{\pi}{4}$, on a $f'\left(\frac{\pi}{4}\right)=3\frac{\sqrt{2}}{4}(-1; 1)$. En $0$, $f'(0)=(0,0)$. En calculant $x''$ et $y''$, on constate alors que
\[ x''(t)=-3(\cos^3(t)-2\sin(t)^2\cos(t)) \qeq y''(t)=3(-\sin^3(t)+2\cos(t)^2\sin(t) \]
et donc $f''(0)=(-3,0)$ et la tangente est donc horizontale. 

On obtient le tableau de variations conjointes suivant :
\input{tex/Chap21/tab/astroidetab}
}	
\end{prof}
\lignes{15}

	\subsection{Tracé}\label{objectif-21-8}
	
Une fois le tableau de variations conjointes obtenu, on peut tracer la courbe, d'abord sur le plus petit intervalle obtenu et en utilisant les tangentes obtenues, puis en utilisant les différentes symétries détectées.

\exemple{Tracer la courbe de l'astroïde.}

\begin{prof}
\solution{On trace d'abord la courbe sur $\left[0;\frac{\pi}{4}\right]$, puis dans l'ordre : symétrie par rapport à la droite $y=x$, symétrie par rapport à l'axe des ordonnées puis symétrie par rapport à l'axe des abscisses. On obtient alors la courbe suivante :
\begin{center}\includegraphics[width=8cm]{tex/Chap21/mp/astroide.mps}\end{center}
}	
\end{prof}

\lignes{15}

\begin{methode}
Pour étudier et tracer une courbe paramétrée :
\begin{numerote}
	\item On réduit l'intervalle d'étude en utilisant les symétries, et périodicité.
	\item On étudie les variations conjointes de $x$ et $y$ sur l'intervalle précédemment obtenu, en n'omettant pas d'indiquer les vecteurs tangents aux points importants.
	\item On trace enfin la courbe en utilisant les résultats précédents, tout d'abord sur l'intervalle d'étude, puis sur le domaine de définition en utilisant les variations. 
\end{numerote}	
\end{methode}


\exercice{[Cardioïde] Soit $f$ la fonction vectorielle du plan définie sur $\R$ par 
\[ f(t)= \systeme*{x(t)=2\cos(t)(1+\cos(t)),y(t)=2\sin(t)(1+\cos(t)) }\]
Représenter la courbe de $f$.}

\begin{prof}
\solution{Constatons tout d'abord que $x$ et $y$ sont $2\pi$-périodiques. On étudie alors $f$ sur $[-\pi;\pi]$, symétrique par rapport à $0$. De plus, pour tout $t\in [-\pi; \pi]$, en utilisant la parité de $\cos$ et l'imparité de $\sin$ :
\[ x(-t)=x(t)\qeq y(-t)=-y(t) \]
Ainsi, la courbe de $f$ est symétrique par rapport à l'axe des abscisses, et on peut restreindre l'intervalle d'étude à $[0;\pi]$. On constate enfin que $x(\pi-t)$ et $y(\pi-t)$ ne peuvent s'exprimer en fonction de $x(t)$ et $y(t)$.\\\textbf{Bilan} : on étudie $f$ sur $[0;\pi]$ et on utilise la symétrie par rapport à l'axe des abscisses.

$x$ et $y$ sont de classe $\CC^\infty$ sur $\R$ et on a, pour tout réel $t$ :
\begin{align*}
    x'(t)&=-2\sin(t)(1+\cos(t))+2\cos(t)(-\sin(t)) \\&= -2\sin(t)(1+2\cos(t)) \\
	\text{et }y'(t)&=2\cos(t)(1+\cos(t))+2\sin(t)(-\sin(t)) \\&= 2\cos(t)+2\cos^2(t)-2\sin^2(t)\\&= 2(\cos(t)+\cos(2t))\\&= 4\cos\left(\frac{3t}{2}\right)\cos\left(\frac{t}{2}\right)
\end{align*} 
Remarquons que $1+2\cos(t)\geq 0 \Leftrightarrow \cos(t)\geq -\dfrac{1}{2}$ et donc, sur $[0;\pi]$, $1+2\cos(t)\geq 0 \Leftrightarrow t\in \left[0;\frac{2\pi}{3}\right]$.

De même, sur $[0;\pi]$, $\cos\left(\dfrac{3t}{2}\right)\geq 0$ si et seulement si $t\in\left[0;\dfrac{\pi}{3}\right]$ et $\cos\left(\dfrac{t}{2}\right)$ est toujours positif sur $[0;\pi]$.

On obtient le tableau de variations suivant :
\input{tex/Chap21/tab/cardioidetab}
Pour la tangente en $\pi$, puisque $f'(\pi)=(0,0)$, on détermine $f''(\pi)$ 
\[ x''(t)=-2\cos(t)(1+2\cos(t))-2\sin(t)(-2\sin(t)) \qeq y''(t)=-2\sin(t)-4\sin(2t) \]
et on a alors $f''(\pi)=(-2; 0)$ et la tangente est horizontale.

On obtient alors la courbe représentative suivante, que l'on trace sur $[0;\pi]$ et qu'on complète par symétrie par rapport à l'axe des abscisses :
\begin{center}\includegraphics[width=8cm]{tex/Chap21/mp/cardioide.mps}\end{center}
}	
\end{prof}

\lignes{40}
\lignes{20}

	\subsection{Longueur d'une courbe}\label{objectif-21-9}
	
\definition{Soient $a<b$ et $f$ une fonction vectorielle de classe $\mathcal{C}$ sur $[a;b]$. La longueur de la courbe paramétrée par $f$ est
\[ L_{[a,b]}(f) = \int_a^b ||f'(t)||\dd t \]
}

\exemple{Calculer la longueur de l'astroïde.}

\begin{prof}
\solution{La courbe est parcourue une et seulement fois si $t$ parcourt $[0;2\pi]$ (ou tout intervalle de longueur $2\pi$). Par symétrie, la longueur de l'astroïde est égale à $8$ fois celle obtenue sur $[0;\frac{\pi}{4}]$. Puisque, pour tout $t$, on 
\[ x'(t) = -3\sin(t)\cos^2(t)\qeq y'(t)=3\cos(t)\sin^2(t) \]
on a alors $(x'(t))^2=9\sin^2(t)\cos^4(t)$ et $(y'(t))^2=9\cos^2(t)\sin^4(t)$ et donc
\[ ||f'(t)||^2=(x'(t))^2+(y'(t))^2 = 9 \sin^2(t)\cos^2(t)(\cos^2(t)+\sin^2(t)) = 9\sin^2(t)\cos^2(t) \]
et donc
\[ ||f'(t)|| = 3 |\sin(t)||\cos(t)| \]
On a alors
\begin{align*}
	L_{[0;\pi/4]}(f) &= \int_0^{\frac{\pi}{4}} 3|\sin(t)||\cos(t)|dt \\
					 &= \int_0^{\frac{\pi}{4}} 3\sin(t)\cos(t)dt \\
					 &= 3\int_0^{\frac{\pi}{4}} \frac{1}{2}\sin(2t)dt \\
					 &= \frac{3}{2} \left[ \frac{-\cos(2t)}{2}\right]_0^{\frac{\pi}{4}} \\
					 &= \frac{3}{4} 
\end{align*} 
Ainsi, 
\[ \boxed{L_{[0;2\pi]}(f)=8\int_0^{\frac{\pi}{4}} ||f'(t)||dt = 6 }\]
}	
\end{prof}

\lignes{17}
