\objectif{Dans ce chapitre, on va décomposer, dans certains cas, certaines fonctions comme somme de fonctions trigonométriques. Cela est très souvent utilisé en électricité et dans le cas du traitement des ondes.}

\textit{La liste ci-dessous représente les éléments à maitriser absolument. Pour cela, il faut savoir refaire les exemples et exercices du cours, ainsi que ceux de la feuille de TD.}

\begin{numerote}
	\item Connaître les coefficients de Fourier  :
			\begin{itemize}[label=\textbullet]
				\item connaître la notion de polynôme trigonométrique\dotfill $\Box$
				\item connaître la définition des coefficients de Fourier\dotfill $\Box$
				\item connaître les propriétés des coefficients de Fourier dans le cas d'une fonction paire ou impaire\dotfill $\Box$
			\end{itemize}
	\item Concernant les séries de Fourier :
			\begin{itemize}[label=\textbullet]
				\item connaître la définition des sommes de Fourier partielles\dotfill $\Box$
				\item connaître la définition de la série de Fourier\dotfill $\Box$
			\end{itemize}
	\item Connaître les cas de convergence :
			\begin{itemize}[label=\textbullet]
				\item connaître la définition de la régularisée d'une fonction\dotfill $\Box$
				\item connaître les conditions de convergence ponctuelle de la série de Fourier\dotfill $\Box$
				\item savoir utiliser la convergence ponctuelle pour déterminer la valeur des sommes de séries particulières\dotfill $\Box$
				\item connaître les conditions d'application de l'identité de Parseval et savoir l'appliquer\dotfill $\Box$
			\end{itemize}
\end{numerote}

\newpage 

Dans ce chapitre, nous allons, sous certaines hypothèses, écrire une fonction périodique comme somme d'une série de fonctions trigonométriques.

\begin{histoire}
Même si des prémices de la décomposition en série de Fourier se trouvent dans les années 1400 en Inde, on doit la théorie à \textit{Joseph Fourier} (1768-1830) qui introduisit l'équation de la chaleur en 1807, puis donna la théorie des séries de Fourier dans son traité \emph{Théorie analytique de la chaleur}. Cette théorie fut corrigée par \textit{Dirichlet} (en 1829) puis finalisée par \textit{Jordan} en 1881.	
\end{histoire}


Dans l'ensemble de ce chapitre, $T>0$ désignera une période, et $\omega=\frac{2\pi}{T}$ la pulsation associée.

\section{Coefficients de Fourier}

	\subsection{Polynômes trigonométriques}
	
\definition{Soit $\omega >0$. On appelle \textbf{polynôme trigonométrique} toute combinaison linéaire des fonctions
\[ t\mapsto 1,\quad t\mapsto \cos(n\omega t) \qeq t\mapsto \sin(n\omega t) \text{ pour }n\geq 1 \]
}

\exemple{$x\mapsto \cos(2x)+\sin(3x)-1$ est un polynôme trigonométrique (avec $\omega =1$).}

\exercice{Montrer que $\cos^2$ et $\sin^2$ peuvent s'écrire comme des polynômes trigonométriques.}

\begin{prof}
\solution{En effet, d'après les formules de linéarisation, on a, pour tout réel $t$ :
\begin{align*}
 \cos^2(t)&=\frac{1+\cos(2t)}{2}=\frac{1}{2}+\frac{1}{2}\cos(2t) \\
\text{et}\quad \sin^2(t)&=\frac{1-\cos(2t)}{2}=\frac{1}{2}-\frac{1}{2}\cos(2t)	
\end{align*}
}	
\end{prof}

\lignes{8}

	\subsection{Coefficients d'un polynôme de Fourier}

\definition{Soit $f$ une fonction $T$-périodique, et continue par morceaux sur $\R$. On appelle \textbf{coefficients de Fourier} de la fonction $f$ les réels $\ds{	a_0(f) = \frac{1}{T}\int_0^T f(t)\dd t }$ et pour tout entier $n\geq 1$ :
\begin{align*}
	a_n(f) &= \frac{2}{T}\int_0^T f(t)\cos(n\omega t) \dd t \\
	b_n(f) &= \frac{2}{T}\int_0^T f(t)\sin(n\omega t) \dd t
\end{align*}
}

\remarque{Dans le cas classique où $T=2\pi$ et $\omega =1$, on obtient les coefficients 
\[ a_0(f) = \frac{1}{2\pi}\int_0^T f(t)\dd t, \quad\text{et}\quad\forall~n\geq 1,\quad 
	a_n(f) = \frac{1}{\pi}\int_0^T f(t)\cos(n t) \dd t \qeq
	b_n(f) = \frac{1}{\pi}\int_0^T f(t)\sin(n t) \dd t
\]
}

\remarque{Puisque l'intégrale d'une fonction $T$-périodique est la même sur tout intervalle de longueur $T$, on peut remplacer l'intervalle $[0,T]$ d'intégration par n'importe quel intervalle de longueur $T$, par exemple $\left[-\frac{T}{2},\frac{T}{2}\right]$. Ainsi, si $T=2\pi$, on peut choisir $\ds{a_0=\frac{1}{2\pi}\int_{-\pi}^\pi f(t)\dd t}$.
}

\exemple{Soit $f$ la fonction $2\pi$-périodique, constante égale à $1$. Déterminer ses coefficients de Fourier.}

\begin{prof}
\solution{On calcule rapidement
\[ a_0(f)=\frac{1}{2\pi}\int_0^{2\pi}1\dd t = 1 \]
et pour $n\geq 1$ :
\begin{align*}
	a_n(f)&=\frac{1}{\pi}\int_0^{2\pi} \cos(nt)\dd t\\
		   &= \frac{1}{\pi}\left[ \frac{\sin(nt)}{n}\right]_0^{2\pi}\\
		   &= 0\\
\text{et}\quad	b_n(f)&=\frac{1}{\pi}\int_0^{2\pi} \sin(nt)\dd t\\
		   &= \frac{1}{\pi}\left[ -\frac{\cos(nt)}{n}\right]_0^{2\pi}\\
		   &= 0
\end{align*}
}	
\end{prof}

\lignes{15}


	\subsection{Cas des fonctions paires et impaires}
	
\remarque{D'après la remarque précédente, on a ainsi que 
\[ a_0(f)=\frac{1}{T}\int_{-\frac{T}{2}}^{\frac{T}{2}} f(t)\dd t,\quad a_n(f)=\frac{2}{T}\int_{-\frac{T}{2}}^{\frac{T}{2}} f(t)\cos(n\omega t)\dd t \qeq b_n(f)=\frac{2}{T}\int_{-\frac{T}{2}}^{\frac{T}{2}} f(t)\sin(n\omega t)\dd t \]
Si $f$ est paire, alors $t\mapsto f(t)\sin(n\omega t)$ est impaire, et donc $b_n(f)=0$. De même, si $f$ est impaire, $t\mapsto f(t)\cos(n\omega t)$ est impaire, et donc $a_n(f)=0$.
} 

\begin{proposition}
Soit $f$ une fonction $T$-périodique, et continue par morceaux sur $\R$.
\begin{itemize}[label=\textbullet]
	\item Si $f$ est paire, alors pour tout $n\in \N^*$, $b_n(f)=0$ et pour $n\geq 1$ 
		\[ a_n(f) = \frac{4}{T}\int_0^{\frac{T}{2}} f(t)\cos(n\omega t)\dd t \]
	\item Si $f$ est impaire, alors pour tout $n\in \N$, $a_n(f)=0$ et pour tout $n\geq 1$
		\[ b_n(f) = \frac{4}{T}\int_0^{\frac{T}{2}} f(t)\sin(n\omega t)\dd t \]
\end{itemize}	
\end{proposition}


\exercice{[Signal rectangulaire] Soit $f$ la fonction définie sur $\R$, $2\pi$-périodique, impaire, vérifiant pour tout $x\in ]0,\pi[$, $f(x)=1$, et pour tout entier $n\in \Z$, $f(n\pi)=0$. Représenter $f$, et calculer ses coefficients de Fourier.}

\begin{prof}
\solution{En utilisant l'imparité et la $2\pi$-périodicité, on obtient :
\input{tex/Chap22/pic/exo2}
Par imparité de $f$, on a
\[ \forall~n\in \N,\quad a_n(f)=0 \]
Enfin, toujours par imparité de $f$, pour $n\geq 1$ :
\begin{align*}
	b_n(f) &= \frac{2}{\pi}\int_0^{\pi} f(t)\sin(nt) \dd t \\
	       &= \frac{2}{\pi}\int_0^{\pi} \sin(nt) \dd t \\
	       &= \frac{2}{\pi} \left[ -\frac{\cos(nt)}{n}\right]_0^{\pi}\\
	       &=\frac{2}{n\pi}\left( -(-1)^n +1\right)
\end{align*}
Ainsi, $b_{2n}(f)=0$ et $b_{2n+1}(f)=\dfrac{4}{(2n+1)\pi}$ pour $n\geq  0$.
}	
\end{prof}

\lignes{20}

\remarque{Pour simplifier les calculs, on peut introduire les coefficients de Fourier complexes : pour $n\in \Z$, on pose
\[ c_n(f) = \frac{1}{T}\int_0^T f(t)\eu{in\omega t} \dd t \]
Ainsi, on récupère les coefficients réels en calculant
\[ a_0(f)=c_0(f),\quad \forall~n\in \N^*,\quad a_n(f)=c_n(f)+c_{-n}(f) \qeq b_n(f)=i(c_{-n}(f)-c_n(f)) \]
}

\section{Séries de Fourier}

	
\definition{Soit $f$ une fonction $T$-périodique, et continue par morceaux sur $\R$. On appelle \textbf{somme de Fourier partielle} de rang $n$ de $f$ le polynôme trigonométrique  défini par 
\[ S_n(f) : t \mapsto  a_0(f) + \sum_{k=1}^n (a_k(f)\cos(k\omega t)+b_k(f)\sin(k\omega t)) \]

}

\remarque{Pour $t$ réel fixé, on peut s'intéresser à la suite $(S_n(f)(t))_{n\in \N}$ qui est une série, appelée \textbf{série de Fourier} de $f$ évaluée en $t$.}

Puisqu'il s'agit d'une série, on peut s'intéresser à sa convergence.

\definition{Soit $f$ une fonction $T$-périodique, et continue par morceaux sur $\R$, et $t$ un réel. Si la série $(S_n(f)(t))$ converge, on appelle alors \textbf{somme de Fourier} de $f$, évaluée en $t$, et on note $S(f)(t)$, la somme de la série :
\[ S(f)(t) = a_0(f) + \sum_{n=1}^{+\infty} (a_n(f)\cos(n\omega t)+b_n(f)\sin(n\omega t)) \]
}

\remarque{\danger~Attention : il n'y a aucune raison que la somme $S(f)(t)$ soit finie, car rien ne garantit a priori la convergence de la série.}

\exemple{On reprend l'exemple du créneau : $f$ est la fonction définie sur $\R$, $2\pi$-périodique, impaire, vérifiant pour tout $x\in ]0,\pi[$, $f(x)=1$, et pour tout entier $n\in \Z$, $f(n\pi)=0$. On représente ci-dessous les premiers termes de la somme de Fourier partielle, pour $n=1,3,5$ et $7$ (puisque les coefficients de terme pair sont nuls) : 

\begin{center}
\hspace*{-1cm}
\begin{tabular}{cc}
\input{tex/Chap22/pic/creneau1} & \input{tex/Chap22/pic/creneau3}\\	$n=1$ & $n=3$
\end{tabular}
\hspace*{-1cm}
\begin{tabular}{cc}
\input{tex/Chap22/pic/creneau5} & \input{tex/Chap22/pic/creneau7}\\
$n=5$&$n=7$
\end{tabular}
\end{center}
}

\remarque{On constate sur l'exemple précédent qu'il semblerait que la série de Fourier partielle se rapproche de la fonction $f$ quand $n$ grandit. Ce résultat n'est pas vrai dans le cas général, mais l'est sous certaines conditions que nous nous allons voir.}

\section{Convergence de la série de Fourier} 

	\subsection{Régularisée d'une fonction}
	
\definition{Soit $f$ une fonction continue par morceaux sur un intervalle ouvert $I$. On appelle \textbf{régularisée} de $f$, et on note $\tilde{f}$, la fonction définie sur $I$ par 
\[ \forall~x\in I,\quad \tilde{f}(x)=\frac{1}{2}\left(\lim_{t\rightarrow x^+} f(t)+\lim_{t\rightarrow x^-} f(t)\right) = \frac{f(x^+)+f(x^-)}{2} \]
}

\remarque{Si $f$ est continue en $x$, alors $f(x^+)=f(x^-)=f(x)$ et donc $\tilde{f}(x)=f(x)$.\\
Si $f$ n'est pas continue en $x$, alors $\tilde{f}(x)$ est égale à la moyenne de la limite à gauche et de la limite à droite de $f$ en $x$.}

\exemple{Si on reprend l'exemple du créneau, $f$ est continue sur $]0;\pi[$ et sur $]-\pi;0[$, donc sur ces intervalles, $\tilde{f}(x)=f(x)$. En $0$, on constate alors que 
\[ \lim_{x\rightarrow 0^-} f(x)=-1 \qeq \lim_{x\rightarrow O^+} f(x)=1 \]
donc $\tilde{f}(0)=\frac{-1+1}{2}=0=f(x)$.\\C'est pour cela qu'on avait posé $f(n\pi)=0$, car dans ce cas $\tilde{f}=f$.
}

\exercice{Soit $f$ la fonction $2\pi$-périodique, impaire, définie sur $]-\pi;\pi]$ par $f(x)=x$. Représenter $f$ et déterminer $\tilde{f}$.}

\begin{prof}
\solution{En utilisant la définition de $f$, on obtient rapidement :
\begin{center}\input{tex/Chap22/pic/exo3}\end{center}
Sur chacun des intervalles $]-\pi+2k\pi,\pi+2k\pi[$, $f$ est continue, donc $\tilde{f}=f$.\\
En $x=(2k+1)\pi$, on a rapidement
\[ \lim_{t\rightarrow x^-} f(t)=\pi \qeq \lim_{t\rightarrow x^+} f(t)=-\pi \]
donc $\tilde{f}(x)=\frac{\pi+(-\pi)}{2}=0$.
}	
\end{prof}

\lignes{15}

	\subsection{Convergence ponctuelle}
	
\begin{theoreme}[Théorème de Dirichlet] 
Soit $f$ une fonction $T$-périodique et de classe $\CC^1$ par morceaux. Alors la série de Fourier de $f$ converge ponctuellement vers la régularisée de $f$ :

\[ \boxed{\forall~x\in \R,\quad S(f)(x) = \tilde{f}(x) } \]
Si $f$ est continue sur $\R$ et de classe $\CC^1$ par morceaux, alors la série de Fourier converge ponctuellement vers $f$.	
\end{theoreme}

\begin{methode}
Pour déterminer la convergence d'une série de Fourier :
\begin{enumerate}
	\item On vérifie que la fonction est périodique.
	\item On vérifie que la fonction est de classe $\CC^1$ par morceaux sur $\R$. 
\end{enumerate}
On peut alors conclure que la série de Fourier converge vers la régularisée.	
\end{methode}


	\subsection{Application}

La convergence de la série de Fourier permet de calculer des valeurs de somme de séries qu'on ne savait pas calculer précédemment.

\exemple{\label{exemple5} En reprenant la fonction créneau, celle-ci est bien de classe $\CC^1$ par morceaux (puisque en escalier). Ainsi, sa série de Fourier converge vers sa régularisée, ce qui dans notre cas, vaut $f$. On a alors 
\[ \forall~x\in \R,\quad a_0(f)+\sum_{n=1}^{+\infty} (a_n(f)\cos(nx)+b_n(f)\sin(nx)) = f(x) \]
soit, en reprenant les valeurs de $a_n(f)$ et $b_n(f)$ :  
\[ \forall~x\in \R,\quad \sum_{n=0}^{+\infty} \frac{4}{\pi} \frac{\sin((2n+1)x)}{2n+1} = f(x) \]
En posant $x=\frac{\pi}{2}$, et en constant que $\sin\left((2n+1)\frac{\pi}{2}\right)=(-1)^n$ et que $f\left(\frac{\pi}{2}\right)=1$, on en déduit que
\[ \boxed{\sum_{n=0}^{+\infty} \frac{(-1)^n}{2n+1} = \frac{\pi}{4}} \]
} 


\exercice{\label{exercice.triangle}Soit $f$ la fonction définie sur $\R$, $2\pi$-périodique, définie sur $]-\pi;\pi]$ par $f(x)=|x|$.\begin{enumerate}
		\item Représenter $f$, puis déterminer ses coefficients de Fourier. 
		\item Sa série de Fourier converge-t-elle ?
		\item En déduire la somme de la série $\ds{\sum_{n=0}^{+\infty} \frac{1}{(2n+1)^2}}$, puis celle de $\ds{\sum_{n=1}^{+\infty} \frac{1}{n^2}}$	
\end{enumerate}
}

\begin{prof}
\solution{~\begin{enumerate}
\item La fonction $f$ est continue donc continue par morceaux. On utilise la définition de $f$ et on obtient rapidement :

La fonction $f$ étant paire, on en déduire que pour tout entier $n\geq 1$, $b_n(f)=0$. De plus,
\begin{align*}
 a_0(f)&=\frac{1}{2\pi}\int_{-\pi}^{\pi} f(t)\dd t \\ &= \frac{1}{\pi}\int_0^\pi |t|\dd t\\
 &= \frac{1}{\pi} \left[ \frac{t^2}{2}\right]_0^\pi = \frac{\pi}{2} 
\end{align*}
et pour tout entier $n\geq 1$ :
\begin{align*}
 a_n(f) &= \frac{1}{\pi}\int_{-\pi}^\pi f(t)\cos(nt)\dd t\\&= \frac{2}{\pi}\int_0^\pi |t|\cos(nt)\dd t\\
 &\overset{I.P.P}{=} \frac{2}{\pi} \left( \left[\frac{\sin(nt)}{n}t\right]_0^\pi - \int_0^\pi \frac{\sin(nt)}{n}\dd t \right)	\\
 &= 0 - \frac{2}{\pi n}\left[ -\frac{\cos(nt)}{n}\right]_0^\pi \\
 &= -\frac{2((-1)^n-1)}{\pi n^2}
\end{align*}
Ainsi,  pour $n\geq 0$, $a_{2n}(f)=0$ et pour $n\geq 0$, $a_{2n+1}(f)=-\dfrac{4}{\pi (2n+1)^2}$
\item La fonction $f$ est de classe $\CC^1$ par morceaux. D'après le théorème de Dirichlet, la série de Fourier de $f$ converge donc vers la régularisée de $f$. Or ici, \[ \lim_{t\rightarrow \pi^-} f(t) = \pi \qeq \lim_{t\rightarrow \pi^+}=\lim_{t\rightarrow (-\pi)^+}=\pi \]
	Ainsi, $f$ est continue en $\pi$ et est donc continue sur $\R$ par $2\pi$-périodicité : la série de  Fourier de $f$ converge vers $f$.
	\item Ainsi, on peut écrire pour tout réel $t$:
	\[ Sf(t) = \frac{\pi}{2}+\sum_{n=0}^{+\infty} -\frac{4}{\pi (2n+1)^2}\cos((2n+1)t) = f(t) \]
	En prenant $t=0$ (puisqu'il y a convergence), on en déduit
	\begin{align*}
	    \frac{\pi}{2}-\sum_{n=0}	^{+\infty} \frac{4}{\pi (2n+1)^2} &= 0 \\
	    \text{soit} \quad \sum_{n=0}^{+\infty} \frac{1}{(2n+1)^2}& = \frac{\pi^2}{8}
	\end{align*}
 Remarquons enfin que, en séparant termes pairs et impairs : \[ \sum_{n=1}^{+\infty}\frac{1}{n^2}=\sum_{n=1}^{+\infty} \frac{1}{(2n)^2} + \sum_{n=0}^{+\infty} \frac{1}{(2n+1)^2} \]
 soit encore \[ \sum_{n=1}^{+\infty} \frac{1}{n^2} = \frac{1}{4}\sum_{n=1}^{+\infty} \frac{1}{n^2} + \sum_{n=0}^{+\infty} \frac{1}{(2n+1)^2}\]
 et donc
 \[ \boxed{\sum_{n=1}^{+\infty} \frac{1}{n^2}=\frac{4}{3}\sum_{n=0}^{+\infty} \frac{1}{(2n+1)^2} = \frac{\pi^2}{6}}\]
\end{enumerate}
}	
\end{prof}

\lignes{60}

	\subsection{Identité de Parseval}
	
Nous verrons dans un chapitre ultérieur une version améliorée du Théorème de Pythagore. Appliqué dans le cas des séries de Fourier, cela donne l'identité de Parseval :

\begin{theoreme}[Identité de Parseval] 
Soit $f$ une fonction $T$-périodique et continue par morceaux sur $\R$. Alors les séries de terme général $|a_n(f)|^2$ et $|b_n(f)|^2$ convergent et on a
\[ \boxed{|a_0(f)|^2 + \frac{1}{2}\sum_{n=1}^{+\infty} \left( |a_n(f)|^2 + |b_n(f)|^2\right) = \frac{1}{T} \int_0^T |f(t)|^2\dd t} \]	
\end{theoreme}

\exemple{En reprenant l'exemple du créneau, la fonction $f$ est bien continue par morceaux, donc d'après l'identité de Parseval :
\[ \frac{1}{2}\sum_{n=0}^{+\infty} \left|\frac{4}{\pi(2n+1)}\right|^2 = \frac{1}{2\pi} \int_0^{2\pi} |f(t)|^2\dd t \]
$f$ étant impaire, $|f|^2$ est paire, et on a donc
\begin{align*}
	\frac{1}{2\pi}\int_0^T |f(t)|^2\dd t &= \frac{1}{\pi} \int_0^\pi |f(t)|^2\dd t\\
	&= \frac{1}{\pi} \int_0^\pi 1 \dd t = 1
\end{align*} 
et donc
\[ \frac{8}{\pi^2}\sum_{n=0}^{+\infty} \frac{1}{(2n+1)^2} = 1 \quad \text{soit} \quad \sum_{n=0}^{+\infty} \frac{1}{(2n+1)^2} = \frac{\pi^2}{8} \]
}

\exercice{En reprenant la fonction de l'exercice \ref{exercice.triangle}, déterminer $\ds{\sum_{n=0}^{+\infty} \frac{1}{(2n+1)^4}}$ puis $\ds{\sum_{n=1}^{+\infty} \frac{1}{n^4}}$.}

\begin{prof}
\solution{La fonction $f$ est bien continue par morceaux (elle était même continue) et $2\pi$- périodique. D'après l'identité de Parseval :
\[ \left(\frac{\pi}{2}\right)^2 + \frac{1}{2}\sum_{n=0}^{+\infty} \left(\frac{4}{\pi(2n+1)^2}\right)^2 = \frac{1}{2\pi} \int_{-\pi}^\pi f^2(t)\dd t \]
Or, par parité 
\begin{align*}
 \int_{-\pi}^\pi f^2(t)\dd t &= 2\int_0^\pi t^2 \dd t\\ &= 2\left[\frac{t^3}{3}\right]_0^\pi = \frac{2\pi^3}{3} 	
\end{align*}
Ainsi,
\[ \frac{\pi^2}{4}+ \frac{8}{\pi^2} \sum_{n=0}^{+\infty} \frac{1}{(2n+1)^4} = \frac{\pi^2}{3}\]
soit \[ \boxed{\sum_{n=0}^{+\infty} \frac{1}{(2n+1)^4} = \frac{\pi^4}{96} }\]
Puis, par un raisonnement similaire à l'exercice \ref{exercice.triangle}, on en déduit 
\[ \sum_{n=1}^{+\infty} \frac{1}{n^4}=\sum_{n=1}^{+\infty} \frac{1}{(2n)^4}+\sum_{n=0}^{+\infty} \frac{1}{(2n+1)^4} \]
soit 
\[ \boxed{\sum_{n=1}^{+\infty} \frac{1}{n^4} = \frac{16}{15} \sum_{n=0}^{+\infty} \frac{1}{(2n+1)^4} = \frac{\pi^4}{90}}\]
}	
\end{prof}


\lignes{25}