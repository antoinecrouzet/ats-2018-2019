// Auteur : Crouzet
// Date : 21/06/2015
// Résumé : fonction appliquant l'algorithme de dichotomie dans le cas où la fonction f est 
// définie et croissante sur l'intervalle wde départ [x;y]. On cherche avec une précision eps.

function [a,b] = dicho (x,y, eps)
a=x
b=y
while(b-a>eps) do
    m=(a+b)/2
    if f(a)*f(m) <= 0 then
        b=m
    else
        a=m
    end
end
endfunction
