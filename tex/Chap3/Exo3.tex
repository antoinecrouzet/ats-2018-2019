%%%%%%%%%%%%%%%%%
%%% EXERCICES %%%
%%%%%%%%%%%%%%%%%
\newpage
\section*{Exercices}
\addcontentsline{toc}{section}{Exercices}

\subsection*{Généralités}

\exercice{[Forme algébrique] Déterminer la forme algébrique des nombres complexes suivants :
\[ \frac{2+i}{1-2i}\quad \quad \frac{1+i}{1+2i}+\frac{1-i}{1-2i} \quad \quad \eu{-i\frac{\pi}{2}}\quad \quad \eu{\pi}\quad \quad \frac{4+3i}{i-3} \]
}

\begin{prof}
\solution{On a rapidement
\[ \frac{2+i}{1-2i} = \frac{(2+i)(1+2i)}{1+4}=i \quad \quad
\frac{1+i}{1+2i}+\frac{1-i}{1-2i} = \frac{6}{5} \quad \quad \eu{-i\frac{\pi}{2}} = -i \]
$\eu{\pi}$ est déjà sous forme algébrique car réel. Enfin
\[ \frac{4+3i}{i-3}=\frac{(4+3i)(-i-3)}{1+9} = -\frac{9}{10}-\frac{13}{10}i \]
}
\end{prof}

\exercice{[Forme exponentielle] Déterminer la forme exponentielle des nombres complexes suivants :
\[ \sqrt{3}+i\quad \quad \sqrt{6}-\sqrt{2}i\quad \quad -3-3i\quad \quad -3 \quad \quad \frac{-1-i}{\sqrt{3}-i}\quad \quad (1-i)^n ~(n\in \Z)\]
}

\begin{prof}
\solution{Par la méthode habituelle, on obtient les résultats suivants (pour les quotients, on détermine d'abord la forme exponentielle du numérateur et du dénominateur, puis on conclut).
\[ \sqrt{3}+i=2\eu{i\frac{\pi}{6}} \quad \quad \sqrt{6}-\sqrt{2}i = 2\sqrt{2}\eu{-i\frac{\pi}{6}} \quad \quad -3-3i=3\sqrt{2}\eu{-i\frac{3\pi}{4}}\quad \quad -3=3\eu{i\pi} \]
Comme $\ds{-1-i=\sqrt{2}\eu{-i\frac{3\pi}{4}}}$ et $\ds{\sqrt{3}-i=2\eu{-i\frac{\pi}{6}}}$, on a
\[ \frac{-1-i}{\sqrt{3}-i} = \frac{\sqrt{2}\eu{-i\frac{3\pi}{4}}}{2\eu{-i\frac{\pi}{6}}} = \frac{\sqrt{2}}{2} \eu{i(-\frac{3\pi}{4}+\frac{\pi}{6})} = \frac{\sqrt{2}}{2} \eu{-i\frac{7\pi}{12}} \]
Enfin, $\ds{1-i=\sqrt{2}\eu{-i\frac{\pi}{4}}}$, et donc
\[ (1-i)^n = \sqrt{2}^n \eu{-in\frac{\pi}{4}} \]
}
\end{prof}

\exercice{[Calcul de $\cos\left(\frac{\pi}{12}\right)$ et $\sin\left(\frac{\pi}{12}\right)$] On note $z=1+i$ et $z'=\sqrt{3}+i$. En exprimant $\frac{z}{z'}$ sous forme algébrique et exponentielle, déterminer les valeurs de $\cos\left(\frac{\pi}{12}\right)$ et $\sin\left(\frac{\pi}{12}\right)$.}

\begin{prof}
\solution{Tout d'abord, par calcul rapide, on a :
\[ \frac{z}{z'}=\frac{1+i}{\sqrt{3}+i} = \frac{(1+i)(\sqrt{3}-i)}{4}=\frac{\sqrt{3}+1}{4}+i\frac{\sqrt{3}-1}{4} \]
De plus, \[ z=\sqrt{2}\eu{i\frac{\pi}{4}} \qeq z'=2\eu{i\frac{\pi}{6}} \] et donc
\[ \frac{z}{z'}=\frac{\sqrt{2}\eu{i\frac{\pi}{4}}}{2\eu{i\frac{\pi}{6}}}= \frac{\sqrt{2}}{2} \eu{i\frac{\pi}{12}} = \frac{\sqrt{2}}{2}\left(\cos\left(\frac{\pi}{12}\right)+i\sin\left(\frac{\pi}{12}\right)\right)\]
Par identification des parties réelles et imaginaires :
\[ \frac{\sqrt{2}}{2}\cos\left(\frac{\pi}{12}\right)=\frac{\sqrt{3}+1}{4}\qeq \frac{\sqrt{2}}{2}\sin\left(\frac{\pi}{12}\right)=\frac{\sqrt{3}-1}{4} \]
c'est-à-dire :
\[ \boxed{\cos\left(\frac{\pi}{12}\right) = \frac{\sqrt{6}+\sqrt{2}}{4} \qeq \sin\left(\frac{\pi}{12}\right) = \frac{\sqrt{6}-\sqrt{2}}{4}}\]
}	
\end{prof}


\exercice{[Identité du parallélogramme] Montrer que, pour tous nombres complexes $z$ et $z'$ on a 
\[ |z+z'|^2+|z-z'|^2=2(|z|^2+|z'|^2) \]
Interpréter géométrique.}

\begin{prof}
\solution{Première méthode : on pose $z=a+ib$, $z'=a'+ib'$ (avec $a,b,a',b'$ des réels) et on développe. Autre méthode :
\begin{align*}
	|z+z'|^2+|z-z'|^2 &= (z+z')(\overline{z+z'}) + (z-z')(\overline{z-z'}) \\
	&= z\conj{z}+z\conj{z'}+z'\conj{z}+z'\conj{z'}+z\conj{z}-z\conj{z'}-z'\conj{z}+z'\conj{z'}\\
	&= 2 z\conj{z}+2z'\conj{z'} = 2|z|^2+2|z'|^2
\end{align*}

}	
\end{prof}


\subsection*{Racines $n$-ièmes}

\exercice{[ATS 2010] Résoudre dans $\C$ l'équation $\ds{\frac{1}{z^2}+z^2+1=0}$.}

\begin{prof}
\solution{Remarquons tout d'abord que, nécessairement, $z\neq 0$. Soit $z\in \C^*$. L'équation $(E):\quad \ds{\frac{1}{z^2}+z^2+1=0}$ est alors équivalente à $z^4+z^2+1=0$.

Soit $Z=z^2 \in \C^*$. L'équation devient alors $Z^2+Z+1=0$, qui a pour solutions \[ Z_1=\frac{-1+i\sqrt{3}}{2} \qeq Z_2=\frac{-1-i\sqrt{3}}{2} \]
On revient à la variable de départ : $z$ est solution de $(E)$ si et seulement si 
\[ z^2=\frac{-1+i\sqrt{3}}{2} \quad\text{ou}\quad z^2=\frac{-1-i\sqrt{3}}{2} \]
On constate (ou on montre) que $Z_1=\eu{\frac{2i\pi}{3}}$ et $Z_2=\eu{-\frac{2i\pi}{3}}$.
En notant alors $z=r\eu{i\theta}$ avec $r>0$, on en déduit alors que 
\[ \left \{ \begin{array}{l} r^2=1\\2\theta = \frac{2\pi}{3}+2k\pi,k \in \Z \end{array}\right. \quad \text{ou}\quad \left \{ \begin{array}{l} r^2=1\\2\theta = -\frac{2\pi}{3}+2k\pi,k \in \Z \end{array}\right. \]
Soit
\[ \left \{ \begin{array}{l} r=1\\\theta = \frac{\pi}{3}+k\pi,k \in \Z \end{array}\right. \quad \text{ou}\quad \left \{ \begin{array}{l} r=1\\\theta = -\frac{\pi}{3}+k\pi,k \in \Z \end{array}\right. \]
L'équation $(E)$ possède donc $4$ solutions : 

\[ \mathcal{S}=\left \{ \eu{-i\frac{\pi}{3}}, \eu{-i(\frac{\pi}{3}+\pi)}, \eu{i\frac{\pi}{3}}, \eu{i(\frac{\pi}{3}+\pi)}\right \} \]
soit
\[ \boxed{\mathcal{S}= \left \{\eu{-i\frac{\pi}{3}},\eu{-i\frac{2\pi}{3}},\eu{i\frac{\pi}{3}} ,\eu{i\frac{2\pi}{3}}\right \} } \]
}	
\end{prof}


\exercice{[BTS-DUT 2011] Soit $n$ un entier naturel non nul. Résoudre, dans $\C$, l'équation \[ (z-1)^n=(z+1)^n \]}

\begin{prof}
\solution{Remarquons tout d'abord que $z=1$ n'est pas solution (car $0 \neq 2^n$). Ainsi :
\[ (z-1)^n=(z+1)^n \Longleftrightarrow \frac{(z+1)^n}{(z-1)^n}=1=\left(\frac{z+1}{z-1}\right)^n \]
Posons $Z=\dfrac{z+1}{z-1}$. L'équation s'écrit alors $Z^n=1$, c'est-à-dire \[ Z = \eu{2i\frac{k\pi}{n}},\quad k\in \ll 0; n-1\rr\]
On revient à la variable de départ :
\begin{eqnarray*}
 Z=\eu{2i\frac{k\pi}{n}} &\Longleftrightarrow& 	\frac{z+1}{z-1}=\eu{2i\frac{k\pi}{n}} \\
 & \text{soit }& z\left(1-\eu{2i\frac{k\pi}{n}}\right)= -1-\eu{2i\frac{k\pi}{n}}\\
 & \text{ou encore }& z = - \frac{1+\eu{2i\frac{k\pi}{n}}}{1-\eu{2i\frac{k\pi}{n}}} \text{ si }k\in \ll 1;n-1 \rr\\
 & \text{donc} & z= - \frac{\eu{-i\frac{k\pi}{n}} + \eu{i\frac{k\pi}{n}}}{\eu{-i\frac{k\pi}{n}} - \eu{i\frac{k\pi}{n}}}= \frac{-i}{\tan\left(\frac{k\pi}{n}\right)}
\end{eqnarray*}
Ainsi, l'équation $(z-1)^n=(z+1)^n$ admet $n-1$ solutions : \[ \mathcal{S}=\left \{ \frac{-i}{\tan\left(\frac{k\pi}{n}\right)},\quad k\in \ll 1;n-1\rr \right \}\]
\remarque{On a bien $n-1$ solutions et non $n$ solutions, car l'équation n'est pas de degré $n$, mais de degré $n-1$ (les termes en $z^n$ se simplifiant).}

}	
\end{prof}



\exercice{[ATS 2014] Déterminer les racines cubiques de $-i$.}

\exercice{[ATS 2009] Déterminer les racines carrées du nombre $-7+5i$.}

\begin{prof}
\solution{Il est difficile d'exprimer $-7+5i$ sous forme exponentielle. Soit donc $z=x+iy$ ($x,y\in \R^2$) vérifiant $z^2=-7+5i$. Mais alors 
\[ x^2-y^2 +2ixy=-7+5i \quad \text{avec}\quad |x+iy|^2=|-7+5i|=\sqrt{74} \]
Ainsi, $z$ est une racine carrée de $-7+5i$ si et seulement si
\[ \left \{ \begin{array}{ccc} x^2-y^2&=&-7 \\ x^2+y^2&=&\sqrt{74}\\2xy&=&5 \end{array} \right. \Leftrightarrow \left \{ \begin{array}{ccc} x^2&=&\frac{\sqrt{74}-7}{2}\\y^2 &=& \frac{\sqrt{74}+7}{2}\\2xy&=&5 \end{array} \right. \Leftrightarrow \left \{ \begin{array}{ccc}x&=&\sqrt{\frac{\sqrt{74}-7}{2}}\\y&=& \sqrt{\frac{\sqrt{74}+7}{2}}\end{array}\right. \text{ou}\quad \left \{ \begin{array}{ccc}x&=&-\sqrt{\frac{\sqrt{74}-7}{2}}\\y&=& -\sqrt{\frac{\sqrt{74}+7}{2}}\end{array}\right.
\]
Ainsi, les deux racines de $-7+5i$ sont 
\[ \boxed{ \sqrt{\frac{\sqrt{74}-7}{2}}+i\sqrt{\frac{\sqrt{74}+7}{2}}; -\sqrt{\frac{\sqrt{74}-7}{2}}-i\sqrt{\frac{\sqrt{74}+7}{2}}}\]
}	
\end{prof}


\subsection*{Trigonométrie}

\exercice{[ATS 2009] Calculer, pour $\theta \in \R$ \[ \sum_{p=0}^n \binom{n}{p} \sin(\theta p) \]}

\exercice{Déterminer $\ds{\integrer{0}{\frac{\pi}{2}}{\cos(2x)\sin(3x)}{x}}$.}

\begin{prof}
\solution{On va linéariser $\cos(2x)\sin(3x)$. On constate que :
\begin{eqnarray*}
	\cos(2x)\sin(3x) &=& \frac{\eu{2ix}+\eu{-2ix}}{2} \frac{\eu{3ix}-\eu{-3ix}}{2i} \\
	&=& \frac{\eu{5ix} +\eu{ix} -\eu{-ix}-\eu{-5ix}}{4i} \\
	&=& \frac{1}{2}\left( \frac{\eu{5ix}-\eu{-5ix}}{2i} + \frac{\eu{ix}-\eu{-ix}}{2i}\right) \\
	&=& \frac{\sin(5x)+\sin(x)}{2}
\end{eqnarray*}

Ainsi,
\begin{eqnarray*}
	\integrer{0}{\frac{\pi}{2}}{\cos(2x)\sin(3x)}{x} &=& \integrer{0}{\frac{\pi}{2}}{~\frac{\sin(5x)+\sin(x)}{2}}{x}\\
	&=& \left [ -\frac{1}{2}\frac{\cos(5x)}{5}-\frac{1}{2}\cos(x)\right ]_0^{\frac{\pi}{2}}\\
	&=& \frac{3}{5}
\end{eqnarray*}

}	
\end{prof}


\exercice{[ATS 2014] Pour $n\in \N$, et $x\in \R \backslash \{k\pi|k\in \Z\}$, on pose 
\[ T_n(x)=\sum_{k=0}^n \eu{2ikx} \qeq S_n(x)=2\Re(T_n(x))-1 \]
\begin{enumerate}
	\item Calculer $T_n(x)$, en déduire que \[ \Re(T_n(x)) = \cos(nx)\frac{\sin\left((n+1)x\right)}{\sin(x)} \]
	\item Montrer que 
	\[ S_n(x)=\frac{\sin\left((2n+1)x\right)}{\sin(x)}=1+2\sum_{k=1}^n\cos(2kx) \]
\end{enumerate}
}

\begin{prof}
\solution{~
\begin{enumerate}
	\item En utilisant la somme des termes d'une suite géométrique (et en remarquant que $\eu{2ix}\neq 1$ car $x\not \in \{k\pi,k\in \Z\}$) :
	\[ T_n(x) = \frac{1-(\eu{2ix})^{n+1})}{1-\eu{2ix}} = \frac{1-\eu{2i(n+1)x}}{1-\eu{2ix}} \]
	On utilise alors la méthode classique :
	\[ 1-\eu{2ix} = \eu{ix}\left(\eu{-ix}-\eu{ix}\right)=-2i\eu{ix}\sin(x) \]
	Ainsi
	\begin{eqnarray*}
		T_n(x) &=& \frac{1-\eu{2i(n+1)x}}{-2i\eu{ix}\sin(x)}\\
		 	   &=& \frac{\eu{-ix} -\eu{i(2n+1)x}}{-2i\sin(x)} \\
		 	   &=& \frac{\eu{inx}\left(\eu{-(n+1)ix}-\eu{(n+1)ix}\right)}{-2i\sin(x)} \quad\quad\text{même méthode} \\
		 	   &=& \eu{inx}\frac{-2i\sin((n+1)x)}{-2i\sin(x)} \\
		 	   &=& \frac{\sin((n+1)x)}{\sin(x)} \left( \cos(nx)+i\sin(nx)\right)
	\end{eqnarray*}
	Ainsi, $\Re(T_n(x))=\cos(nx)\frac{\sin((n+1)x)}{\sin(x)}$.
	\item On a alors, d'après le résultat précédent :
	\begin{eqnarray*}
		S_n(x) &=& 2\Re(T_n(x))-1 \\
		&=& 2 \cos(nx)\frac{\sin( (n+1)x)}{\sin(x)} - 1
	\end{eqnarray*}
	or $\sin((n+1)x)=\sin(x)\cos((nx)+\cos(x)\sin(nx)$. Ainsi
	\begin{eqnarray*}
		S_n(x) &=& 2\cos(nx) \frac{\cos(nx)\sin(x)+\cos(x)\sin(nx)}{\sin(x)}  -1\\
		&=& 2\cos^2(nx) + 2 \frac{\cos(nx)\sin(nx)\cos(x)}{\sin(x)}-1 
	\end{eqnarray*}
	Or, $2\cos^2(nx)-1=\cos(2nx)$. Donc
	\begin{eqnarray*}
		S_n(x) &=& \cos(2nx) + 2 \frac{\cos(nx)\cos(x)\sin(nx)}{\sin(x)} \\
		&=& 1- 2 \sin(nx)\frac{\sin(nx)\sin(x)-\cos(x)\cos(nx)}{\sin(x)}\\
		&=& 1-2 \sin(nx) \frac{-\cos((n+1)x}{\sin(x)}
	\end{eqnarray*}
\end{enumerate}
}	
\end{prof}


\subsection*{Géométrie}

\exercice{Déterminer l'ensemble des points $M$ d'affixe $z$ telle que
\[ |z-2i|=2 \quad \quad |z-1+i|<2 \quad \quad |z+2-i|=|z-1-i|\quad \quad \arg(z)=\frac{\pi}{2} ~[\pi]\quad \quad \arg\left(\frac{z-1}{z-i}\right)=\frac{\pi}{2}~[\pi]\]}

\exercice{[ATS 2014] Soit $(ABC)$ un triangle, et $p\in ]0;1[$. On considère les points $A', B'$ et $C'$ définis par
\[ \left\{ \begin{array}{l}\vv{AA'}=p\vv{AB} \\ \vv{BB'}=p\vv{BC} \\\vv{CC'}=p\vv{CA} \end{array}\right. \]
Dans le plan complexe, on note $a,b$ et $c$ les affixes des points respectifs $A$, $B$, $C$ et $a', b'$ et $c'$ les affixes des points respectifs $A'$, $B'$ et $C'$.\\On note $\ds{\omega = \frac{-1+i\sqrt{3}}{2}}$.
\begin{enumerate}
	\item Montrer que $\omega^3=1$, $1+\omega+\omega^2=0$ et $\omega^2=\conj{\omega}$.
	\item Déterminer $a'$, $b'$ et $c'$ en fonction de $a,b,c$ et $p$.
	\item Démontrer que $(ABC)$ et $(A'B'C')$ ont le même centre de gravité.
\end{enumerate}
}

\exercice{Soit $A$ le point d'affixe $1$ du plan complexe. On considère la transformation du plan $f$ qui, à tout point $M$ d"affixe $z$, et distinct de $A$, associe le point $M'$ d'affixe \[ z'=\frac{1-z}{\conj(z)-1}\]
	\begin{enumerate}
		\item Déterminer l'ensemble des antécédents de $A$ par $f$.
		\item Calculer $|z'|$.
		\item Démontrer que, pour tout point $M\neq A$, $A, M$ et $M'$ sont alignés.
		\item En déduire une méthode de construction du point $M'$.
	\end{enumerate}
}

\exercice{Soit $n$ un entier $n\geq 3$. Pour tout entier $k\in \{0,\cdots, n\}$, on définit le point $M_k$ d'affixe $\ds{z_k=\eu{\frac{2ik\pi}{n}}}$. On rappelle que les points $M_k$ forment un polygone régulier, inscrit dans le cercle de centre $O$ et de rayon $1$. On souhaite calculer le périmètre de ce cercle.
\begin{enumerate}
	\item Montrer que, pour tout $k\in \{0,\cdots, n-1\}$, $M_kM_{k+1}=2\sin\left(\frac{\pi}{n}\right)$.
	\item Déterminer le périmètre du polygone $M_0M_1\cdots M_n$.
	\item Déterminer la limite de ce périmètre lorsque $n$ tend vers $+\infty$. Interpréter. 
\end{enumerate}
}

\exercice{Soit $ABC$ un triangle, dont le centre du cercle circonscrit est l'origine $O$. On note $G$ le centre de gravité, et $a,b,c$ et $g$ les affixes respectives des points $A,B,C$ et $G$. On note enfin $H$ le point d'affixe $h=a+b+c$.
\begin{enumerate}
	\item Déterminer l'affixe de $G$.
	\item Montrer que $|a|=|b|=|c|$.
	\item Montrer que le nombre complexe $\ds{\frac{h-a}{c-b}}$ est imaginaire pur. Interpréter.
	\item Montrer que $H$ est l'orthocentre du triangle $ABC$.
	\item Montrer enfin que $O$, $G$ et $H$ sont alignés.
\end{enumerate}
\begin{histoire}
Ceci est général : pour tout triangle non plat, le centre du cercle circonscrit, le centre de gravité et l'orthocentre sont alignés sur une droite appelée \textit{Droite d'Euler.}	
\end{histoire}
}