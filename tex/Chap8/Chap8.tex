\objectif{Dans ce chapitre, on introduit un objet qui servira régulièrement, et qui a déjà été vu, en partie, au lycée.}

\large \textbf{Objectifs} \normalsize

\textit{La liste ci-dessous représente les éléments à maitriser absolument. Pour cela, il faut savoir refaire les exemples et exercices du cours, ainsi que ceux de la feuille de TD.}

\begin{numerote}
				\item Savoir manipuler la notion de degré (définition, opérations)\dotfill $\Box$
				\item Savoir manipuler la notion de dérivation de polynôme\dotfill $\Box$ 
				\item Connaître le principe de la division euclidienne\dotfill $\Box$
				\item Connaître la notion de racine, et de multiplicité\dotfill $\Box$
				\item Savoir factoriser un polynôme dans $\R$ et dans $\C$\dotfill $\Box$
				\item Connaître la notion de pôle d'une fraction rationnelle\dotfill $\Box$
				\item Savoir déterminer la décomposition en éléments simples d'une fraction rationnelle\dotfill $\Box$
\end{numerote}


\newpage
Dans l'ensemble de ce chapitre, $\K$ désigne le corps des réels $\R$ ou le corps des complexes $\C$.

%%%%%%%%%%%%%%%%%%%%%
\section{Définitions}
%%%%%%%%%%%%%%%%%%%%%
	
	%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
    \subsection{Notion de polynômes}
    %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
    
\notation{Pour tout $i \in \N^*$, on introduit les fonctions $\begin{array}{rrcl} X^i:&\K &\mapsto& \K\\ &x &\mapsto& x^i\end{array}$ et la fonction  $\begin{array}{rrcl}X^0: &\K &\mapsto& \K\\ &x &\mapsto& 1\end{array}$
}

\definition{On appelle \textbf{polynôme à coefficients dans $\K$} toute somme finie d'applications précédentes, c'est-à-dire toute fonction s'écrivant sous la forme 
		\[P(X)=a_nX^n+a_{n-1}X^{n-1}+\cdots +a_1X+a_0=\sum_{k=0}^n a_kX^k\]
avec $n \in \N$ et $\forall~k \in \ll 0,n \rr, a_k\in \K$.
\begin{itemize}[label=\textbullet]
    \item Les nombres $a_k$ sont appelés les \textbf{coefficients} du polynôme $P$.
    \item Si $a_n\neq 0$, on dit que $P$ est de \textbf{degré} $n$ et on note $\deg(P)=n$. Dans ce cas, $a_n$ est appelé \textbf{coefficient dominant} du polynôme $P$.
    \item La \textbf{fonction polynôme associée} au polynôme $P$ est la fonction, encore notée $P$, définie sur $\K$ par \\$P:x\mapsto a_nx^n+\cdots +a_1x+a_0$.
    \item Un polynôme est dit \textbf{unitaire} si son coefficient dominant vaut $1$.
\end{itemize}
}

\exemple{Le polynôme $P(X)=X^4-2X^2+1$ est un polynôme de degré $4$ et de coefficient dominant $1$. Sa fonction polynôme associée est la fonction $P:x \mapsto x^4-2x^2+1$. Ainsi, $P(1)=1^4-2\times 1^2+1=0$.}

\remarque{Attention : la notation $P(X)$ représente une fonction (c'est une représentation \textbf{formelle}). On peut ainsi parler du polynôme $P(X)=X^2-1$ plutôt que de parler de la fonction polynôme $P$ définie pour tout $x\in \K$ par $P(x)=x^2-1$.}

\notation{On note $\K[0]$ l'ensemble des polynômes à coefficients dans $\K$, et $\K[-n]$ l'ensemble des polynômes à coefficients dans $\K$ de degré inférieur ou égal à $n$. \\Ainsi, $\K[-1]=\{aX+b, (a,b)\in \K^2\}$ et $\K[-2]=\{aX^2+bX+c, (a,b,c)\in \K^3\}$.}

	%%%%%%%%%%%%%%%%%%
    \subsection{Degré}
    %%%%%%%%%%%%%%%%%%
   
\propriete{Soient $P$ et $Q$ deux polynômes.
\begin{itemize}[label=\textbullet]
    \item Le degré d'un polynôme constant non nul est $0$.
    \item $\deg(P+Q)\leq \max(\deg(P),\deg(Q))$
    \item $\deg(PQ)=\deg(P)+\deg(Q)$.
\end{itemize} }

\begin{prof}
\preuve{On note $n$ le degré de $P$ et $m$ le degré de $Q$. Ainsi, $P(X)=a_nX^n+\cdots +a_0$ et $Q(X)=b_mX^m+\cdots+b_0$ avec $a_n\neq 0$ et $b_m\neq 0$.
\begin{itemize}[label=\textbullet]
    \item On a $(P+Q)(X)=a_nX^n+\cdots+a_0+b_mX^m+\cdots+b_0$.  Par construction, le degré de $P+Q$ est égale au plus grand des deux degrés, sauf si $n=m$ et $a_n=-b_m$, et dans ce cas, le degré est strictement inférieur à $n$. Dans tous les cas $\deg(P+Q)\leq \max(\deg(P),\deg(Q))$.
    \item Quand on développe $P(X)Q(X)$, le terme de plus haut degré est le terme $a_nX^nb_mX^m=a_nb_mX^{n+m}$ (avec $a_n\neq 0$ et $b_m\neq 0$). Donc $\deg(PQ)=\deg(P) + \deg(Q)$.
\end{itemize}}
\end{prof}

\begin{eleve} 
\begin{solu_eleve}
\notes[1.8ex]{12}{\textwidth}
\end{solu_eleve}
\end{eleve}

\remarque{Le polynôme nul est le polynôme dont tous les coefficients sont nuls. Par convention, on dit qu'il est degré $-\infty$.}

	%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
    \subsection{Dérivée d'un polynôme}
    %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
    
\definition{Soit $P(X)=a_nX^n+\cdots +a_1X+a_0 \in \K[0]$ un polynôme. On appelle \textbf{polynôme dérivé}, et on note $P'$, le polynôme définie par
\[P'(X)=na_nX^{n-1}+(n-1)a_{n-1}X^{n-2}+\cdots+2a_2X+a_1\]}

\remarque{On peut dérivée à nouveau $P'$ et on note $P''$ ou $P^{(2)}$ la dérivée seconde de $P$. Plus généralement, on note $P^{(k)}$ la dérivée $k$-ième de $P$.}

\exemple{Soit $P(X)=3X^3+2X^2-1$. Alors $P'(X)=9X^2+4X$, $P^{(2)}(X)=18X+4$, $P^{(3)}(X)=18$ et $P^{(4)}(X)=0$.}

\remarque{~\begin{itemize}[label=\textbullet]
    \item Si $\deg(P)=n\geq 1$, alors $\deg(P')=n-1$.
    \item Si $\deg(P)=n$, alors $\forall~k>n,\quad P^{(k)}=0$.
\end{itemize}}

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
\section{Egalité de polynômes et identification}
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
   
   	   %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
       \subsection{Egalité de polynômes}
       %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
      
\begin{theoreme}
Soient $\ds{P(X)=\sum_{k=0}^n a_kX^k}$ et $\ds{Q(X)=\sum_{k=0}^m b_kX^k}$ deux polynômes. Alors $P=Q$ si et seulement si 
	\[n=m \qeq \forall~k \in \ll 0;n \rr, \quad a_k=b_k\]
Ainsi, deux polynômes sont égaux si et seulement s'ils ont même degré, et mêmes coefficients.	
\end{theoreme}


\exemple{Soient $P(X)=2X^2+3X-1$ et $Q(X)=2X^2+aX-b$. Alors $P=Q$ si et seulement si $a=3$ et $b=1$.}

		%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
        \subsection{Application à l'identification}
        %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
        
\exemple{Soit $P(X)=X^3-X^2-3X+2$. Déterminer trois réels $a,b,c$ tels que
\[\forall~x\in\R, \quad P(x)=(x-2)(ax^2+bx+c)\]}

\begin{prof}
\solution{
Soit $Q(X)=(X-2)(aX^2+bX+c)$. En développant,
\[Q(X)=aX^3+bX^2+cX-2aX^2-2bX-2c=aX^3+(b-2a)X^2+(c-2b)X-2c\]
Pour que $P=Q$, par unicité de l'écriture d'un polynôme, on identifie les coefficients :
\[\left\{\begin{array}{rcl}
 a &=& 1 \\
 b-2a &=& -1 \\
 c-2b &=& -3 \\
 -2c &=& 2
\end{array}\right. \Longleftrightarrow 
\left\{\begin{array}{rcl}
 a &=& 1 \\
 b &=& 1 \\
 c &=& -1 
\end{array}\right.\]
Ainsi, $P(X)=(X-2)(X^2+X-1)$.
}
\end{prof}

\begin{eleve} 
\begin{solu_eleve}
\notes[1.8ex]{10}{\textwidth}
\end{solu_eleve}
\end{eleve}

\exercice{Trouver deux réels $a$ et $b$ tels que, \[\forall~x\in\R\backslash\{-1;1\}, \quad \frac{1}{x^2-1}=\frac{a}{x-1}+\frac{b}{x+1}\]}

\begin{prof}
\solution{En mettant au même dénominateur, on cherche $a$ et $b$ tels que, pour tout $x \in \R \backslash \{-1;1\}$, on a
\[\frac{1}{x^2-1} = \frac{a(x+1)+b(x-1)}{x^2-1}=\frac{(a+b)x+a-b}{x^2-1}\]
Par identification des coefficients, on a donc
\[\left \{ \begin{array}{ccc} a+b &=& 0 \\ a-b&=&1 \end{array}\right. \Longleftrightarrow \left \{ \begin{array}{ccc} a&=&\frac{1}{2}\\b&=&-\frac{1}{2} \end{array}\right.\]
Ains, pour tout réel $x$ différent de $1$ et $-1$, on a
\[\frac{1}{x^2-1}=\frac{1}{2(x-1)}-\frac{1}{2(x+1)}\]
}
\end{prof}


\begin{eleve} 
\begin{solu_eleve}
\notes[1.8ex]{10}{\textwidth}
\end{solu_eleve}
\end{eleve}


%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
\section{Racines d'un polynôme}
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

	%%%%%%%%%%%%%%%%%%%%%%%
    \subsection{Définition}
    %%%%%%%%%%%%%%%%%%%%%%%
    
\definition{Soit $P\in \K[0]$ et $a$ un élement de $\K$. On dit que $a$ est une \textbf{racine} de $P$ si $P(a)=0$.}

\remarque{Chercher les racines de $P$, c'est donc résoudre l'équation $P(X)=0$}

\exemple{$1$ est une racine du polynôme $P(X)=X^2-1$. En effet, $P(1)=0$.}

\definition{Soit $P \in \K[0]$ et $a$ un réel. On dit que le polynôme $P$ se \textbf{factorise} par $X-a$ (ou que $X-a$ \textbf{divise} $P$) s'il existe un polynôme $Q\in \K[0]$ tel que $P(X)=(X-a)Q(X)$.}

\exemple{Le polynôme $P(X)=X^3-X^2+X-1$ se factorise par $X-1$. En effet, on a $P(X)=(X-1)(X^2+1)$.}

\begin{theoreme}
Soit $P \in \K[0]$ et $a$ un réel. Alors
\begin{center}
$a$ est une racine de $P$ si et seulement s'il existe un polynôme $Q(X)$ tel que $P(X)=(X-a)Q(X)$.
\end{center}
Dans ce cas, on a alors $\deg(P)=\deg(X-a) +\deg(Q)$ c'est-à-dire $\deg(Q)=\deg (P)-1$.	
\end{theoreme}


\exemple{Soit $P$ le polynôme défini par $P(X)=X^3 - 2X +1$. Montrer que $1$ est racine, puis factoriser $P$ par $(X-1)$.}

\begin{prof}
\solution{On constate que $P(1)=0$. Ainsi, $1$ est racine. Par théorème, on peut donc factoriser $P$ par $X-1$ : il existe $Q$ un polynôme tel que $P(X)=(X-1)Q(X)$ avec 
$\deg(Q)=\deg(P)-1=2$. On cherche donc $a, b$ et $c$ tel que 
\[X^3-2X+1=(X-1)(aX^2+bX+c)\]
Après développement, on obtient 
\[X^3-2X+1=aX^3+(b-a)X^2+(c-b)X-c\]
Par identification, on obtient $a=1,~b=1$ et $c=-1$. Ainsi
\[X^3-2X+1=(X-1)(X^2+X-1)\]
}
\end{prof}

\begin{eleve} 
\begin{solu_eleve}
\notes[1.8ex]{7}{\textwidth}
\end{solu_eleve}
\end{eleve}

\definition{[Multiplicité d'une racine] On dit qu'une racine $a$ d'un polynôme $P$ est de \textbf{multiplicité} $k\in \N^*$ si $P(a)=P'(a)=\cdots=P^{(k-1)}(a)=0$ et $P^{(k)}(a)\neq 0$. Une racine de multiplicité $1$ est dite simple, de multiplicité $2$ est dite double.}

\exemple{Montrer que $2$ est racine double du polynôme $P(X)=X^3-3X^2+4$.}

\begin{prof}
\solution{En effet, on a $P(2)=0$, et puisque $P'(X)=3X^2-6X$, on a également $P'(2)=0$. Enfin, $P^{(2)}(X)=6X-6$ et $P^{(2)}(2)\neq 0$. $2$ est donc bien une racine double de $P$.
}	
\end{prof}

\begin{eleve} 
\begin{solu_eleve}
\notes[1.8ex]{4}{\textwidth}
\end{solu_eleve}
\end{eleve}

\begin{theoreme}
$a$ est une racine de multiplicité $k\in \N^*$ d'un polynôme $P$ de degré $n$ si et seulement s'il existe un polynôme $Q$ tel que $P(X)=(X-a)^kQ(X)$, avec $Q(a)\neq 0$.	
\end{theoreme}


\remarque{Ainsi, si la racine est double, on peut mettre $(X-a)^2$ en facteur.}

	%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
    \subsection{Nombre de racines d'un polynôme}
    %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

Dans $\C$, on dispose d'un théorème fondamental en analyse :

\begin{theoreme}[Théorème de d'Alembert-Gauss] 
Tout polynôme non constant de $\C[0]$ admet au moins une racine.	
\end{theoreme}


\preuve{Théorème admis.}

\consequence{Ainsi, tout polynôme de degré $n$ ($n\geq 1$) admet exactement $n$ racines complexes, éventuellement non distinctes. Si $P$ est de degré $n$, et de coefficient dominant $a_n$, il existe $p$ racines $\alpha_1,\cdots, \alpha_p$ de multiplicités respectives $m_1,\cdots,m_p$, tels que
	\[ P(X)=a_n\prod_{i=1}^p (X-\alpha_i)^{m_i} \quad \text{avec}\quad m_1+\cdots+m_p=n \]
}

\consequence{Soit $P\in \C[-n]$. Si $P$ a $n+1$ racines, alors $P$ est le polynôme nul.}

\preuve{En effet, $P$ est au plus de degré $n$. Il ne peut pas admettre plus de $n$ racines, sauf s'il est nul.}

\definition{Un polynôme $P$ de $\K[0]$ est dit irréductible si et seulement si $P=AB \implies A$ ou $B$ constant. Ainsi, on ne peut pas les factoriser.}

\remarque{Dans $\C[0]$, seuls les polynômes constants et de degré $1$ sont irréductibles.}

	%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
    \subsection{Cas des polynômes de degré $2$ sur $\R[0]$}
    %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
    
\begin{theoreme}
Soit $P(X)=aX^2+bX+c$ un polynôme de degré $2$ ($a\neq 0$). Soit $\Delta=b^2-4ac$ son discriminant.
\begin{itemize}[label=\textbullet]
    \item Si $\Delta>0$, le polynôme $P$ possède deux racines réelles distinctes
    \[x_1=\frac{-b-\sqrt{\Delta}}{2a} \qeq x_2=\frac{-b+\sqrt{\Delta}}{2a}\]
    On a alors $P(X)=a(X-x_1)(X-x_2)$.
    \item Si $\Delta=0$, le polynôme $P$ possède une unique racine réelle dite double
    \[x_0=-\frac{b}{2a}\]
    On a alors $P(X)=a(X-x_0)^2$. 
    \item Si $\Delta<0$, le polynôme $P$ ne possède pas de racines réelles, et ne se factorise donc pas dans $\R[0]$. 
\end{itemize}	
\end{theoreme}


\exemple{Factoriser $P(X)=2X^2+2X-4$}

\begin{prof}
\solution{On a $\Delta=36>0$ donc le polynôme $P$ possède deux racines réelles
\[x_1=\frac{-2-\sqrt{36}}{2\times 2}=-2 \qeq x_2=1\]
Donc $P(X)=2(X-1)(X+2)$.}
\end{prof}

\begin{eleve} 
\begin{solu_eleve}
\notes[1.8ex]{4}{\textwidth}
\end{solu_eleve}
\end{eleve}

\remarque{Attention : on n'oubliera pas, lors de la factorisation, de mettre en facteur le coefficient de plus haut degré (dans l'exemple précédent, le $2$).}

\remarque{Ainsi, dans $\R[0]$, les polynômes de degré $2$, dont le discriminant est strictement négatif, sont irréductibles. On dispose alors du résultat suivant :}

\begin{proposition}
Tout polynôme non nul de $\R[0]$ est le produit de son coefficient dominant $a_n$, de polynômes unitaires de degré $1$, et de polynômes unitaires de degré $2$ à discriminant strictement négatif.	
\end{proposition}


	%%%%%%%%%%%%%%%%%%%%%%%%
    \subsection{Cas général}
    %%%%%%%%%%%%%%%%%%%%%%%%
    
\begin{methode}
Lorsqu'on doit factoriser un polynôme $P$ (ou déterminer ses racines) :
\begin{itemize}[label=\textbullet]
    \item On cherche des racines évidentes ($-2,-1,0,1,2$) pour se ramener à un (ou  des) polynôme(s) de degré $2$.
    \item On factorise le polynôme $P$ par $X-a$ où $a$ désigne une racine évidente.
    \item Une fois ramené à un (ou des) polynômes de degré $2$, on utilise la méthode classique du discriminant.
\end{itemize}	
\end{methode}


\exemple{Soit $P(X)=X^3-2X^2-X+2$. Déterminer les racines de $P$.}

\begin{prof}
\solution{~
\begin{itemize}[label=\textbullet]
    \item Premier étape : on cherche une ``racine évidente'' : $-2, -1, 0, 1, 2$. Ici, on constate que $P(1)=1-2-1+2=0$ donc $1$ est racine évidente.
    \item Deuxième étape : on factorise $P$ par $X-1$ : on écrit $P(X)=(X-1)Q(X)$ avec $\deg(Q)=\deg(P)-1=2$. Donc $Q$ peut s'écrire $aX^2+bX+c$.\\
    On a alors
    \[P(X)=(X-1)(aX^2+bX+c)=aX^3+(b-a)X^2+(c-b)X-c\]
    Par unicité de l'écriture d'un polynôme, on identifie les coefficients :
    \[\left\{\begin{array}{rcl}
 a &=& 1 \\
 b-a &=& -2 \\
 c-b &=& -1 \\
 -c &=& 2
\end{array}\right. \Longleftrightarrow 
\left\{\begin{array}{rcl}
 a &=& 1 \\
 b &=& -1 \\
 c &=& -2 
\end{array}\right.\]
Donc $P(X)=(X-1)(X^2-X-2)$.
\item Troisième étape : on détermine les racines du polynôme $Q$. Ici, $Q$ est du second degré, dont le discriminant vaut $\Delta=9$. Donc $Q$ possède deux racines :
		\[x_1=\frac{1-\sqrt{9}}{2}=-1 \qeq x_2=2\]
Donc $Q(X)=(X-2)(X+1)$
    \item Quatrième étape : on conclut. On a donc
    \[P(X)=(X-1)(X-2)(X+1)\]
    et $P$ possède trois racines réelles : $1, 2$ et $-1$.
\end{itemize}
}  
\end{prof}

\begin{eleve} 
\begin{solu_eleve}
\notes[1.8ex]{16}{\textwidth}
\end{solu_eleve}
\end{eleve}

\exercice{Factoriser $X^4+X^3-X^2+X-2$ sur $\C[0]$.}

\begin{prof}
\solution{Remarquons que $P(1)=0$ et $P(-2)=0$. On peut donc factoriser par $(X-1)(X+2)$. Après factorisation, on obtient
\[ P(X)=(X-1)(X+2)(X^2+1) \]
et donc
\[ P(X)=(X-1)(X+2)(X-i)(X+i) \]
}
\end{prof}

\begin{eleve} 
\begin{solu_eleve}
\notes[1.8ex]{14}{\textwidth}
\end{solu_eleve}
\end{eleve}

\remarque{Dans $\R[0]$, $X^4+X^3-X^2+X-2$ se factorise en $(X-1)(X+2)(X^2+1)$ car $X^2+1$ est irréductible dans $\R[0]$.}

	%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
    \subsection{Division euclidienne}
    %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
    
\remarque{Pour factoriser, plutôt que de procéder par identification, on peut également utiliser la division euclidienne.}

\begin{proposition}
Soient $A$ et $B$ deux polynômes de $\K[0]$, avec $B$ non nul. Alors, il existe deux polynômes uniques $Q$ et $R$, avec $\deg R<\deg B$, tels que $A=BQ+R$. Le polynôme $Q$ est le quotient, et $R$ est le reste de la division euclidienne de $A$ par $B$.	
\end{proposition}


\remarque{En pratique, on pose la division et on effectue la division comme pour la division entière.}

\exemple{Effectuer la division euclidienne de $X^3-2X^2-X+2$ par $X-1$.}

\begin{prof}
\begin{equation*}
\renewcommand{\arraystretch}{1.2}
\renewcommand{\arraycolsep}{2pt}
  \begin{array}{rrrr|rrr}
 X^3&-2X^2 & -X  &+2&X  &-1 &  \\
\cline{5-7}
-X^3&+X^2 &   &  &X^2&-X&-2\\
\cline{1-2}
    &-1X^2 & -X  &  &   &   &  \\
    &X^2&-X&  &   &   &  \\
    \cline{2-3}
    &     &-2X & +2 &   &   &  \\
    &     &2X&-2&   &   &  \\
              \cline{3-4}
    &     &   &0&   &   &  \\ 
  \end{array}
\end{equation*}
\end{prof}

\begin{eleve} 
\begin{solu_eleve}
\begin{equation*}
\renewcommand{\arraystretch}{1.2}
\renewcommand{\arraycolsep}{2pt}
  \begin{array}{rrrr|rrr}
 X^3&-2X^2 & -X  &+2&X  &-1 &  \\
\cline{5-7}
%-X^3&+X^2 &   &  &X^2&-X&-2
&&&&&&~\\
%\cline{1-2}
 %   &-1X^2 & -X  &  &   &   &  
&&&&&& \\
 %   &X^2&-X&  &   &   &  
&&&&&& \\
% +   \cline{2-3}
%  &     &-2X & +2 &   &   &  
&&&&&&\\
%    &     &2X&-2&   &   &  
&&&&&&\\
%              \cline{3-4}
%    &     &   &0&   &   &  
&&&&&&\\ 
  \end{array}
\end{equation*}
\end{solu_eleve}
\end{eleve}

\exercice{Effectuer la division euclidienne de $2X^4-X^3+3X-5$ par $X^2-X+2$. \\Déterminer le reste de la division euclidienne de $(X-3)^{2n}-(X-2)^n-2$ par $(X-2)(X-3)$.}

\begin{prof}
\solution{Par division euclidienne, on obtient 
\[ 2X^4-X^3+3X-5 = (X^2-X+2)(2X^2+X-3) + (-2X+1)  \]
Notons $Q$ et $R$ le quotient et le reste de la division euclidienne de $(X-3)^{2n}-(X-2)^n-2$ par $(X-2)(X-3)$. $\deg(R)<\deg((X-2)(X-3))=2$ et donc $R$ s'écrit $aX+b$ avec $a$ et $b$ deux réels. On a donc
\[ (X-3)^{2n}-(X-2)^n-2=(X-2)(X-3)Q(X)+aX+b \]
En prenant $X=2$, on obtient $2a+b = (-1)^{2n}-2=-1$ et en prenant $X=3$, on obtient $3a+b=-(1)^n-2=-3$. Ainsi, $a=-2$ et $b=3$ et le reste s'écrit donc $-2X+3$.
}	
\end{prof}


\begin{eleve}
\begin{solu_eleve}
\notes[1.8ex]{7}{\textwidth}
\end{solu_eleve}	
\end{eleve}

	%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
	\subsection{Relations racines-coefficients}
	%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
	
\begin{proposition}
Soient $P(X)=a_nX^n+\cdots+a_0$ un polynôme de $\C[0]$ de degré $n$, avec $n\geq 1$ et $a_n\neq 0$, et $\alpha_1,\cdots,\alpha_n$ ses racines complexes (éventuellement non distinctes). Alors
\[ \alpha_1+\cdots +\alpha_n = -\frac{a_{n-1}}{a_n} \qeq \alpha_1\alpha_2\cdots \alpha_n=(-1)^{n}\frac{a_0}{a_n} \]	
\end{proposition}


\preuve{En effet, par factorisation, on obtient
\[ P(X)=a_nX^n+\cdots+a_0= a_n(X-\alpha_1)\cdots(X-\alpha_n) \]
En développant, on obtient
\[ P(X)=a_nX^n + a_n(-\alpha_1-\cdots-\alpha_n)X^{n-1}+\cdots + a_n(-1)^n\alpha_1\cdots\alpha_n \]
On conclut par identification des coefficients.
}

\consequence{Pour un polynôme de degré $2$, $P(X)=aX^2+bX+c$ ($a\neq 0$), en notant $z_1$ et $z_2$ ses racines complexes, on a alors
\[ z_1+z_2=-\frac{b}{a} \qeq z_1z_2=\frac{c}{a} \]
}

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
\section{Fractions rationnelles}
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

	%%%%%%%%%%%%%%%%%%%%%%%%
	\subsection{Définitions}
	%%%%%%%%%%%%%%%%%%%%%%%%
	
\definition{Soient $A$ et $B$ deux polynômes de $\K[0]$, avec $B\neq 0$. Le quotient $\frac{A}{B}$ est appelé \textbf{fraction rationnelle} à coefficient dans $\K$. \\$A$ est le numérateur de la fraction, et $B$ le dénominateur.}

\exemple{Par exemple, $\ds{\frac{X^2+1}{X-2} }$ est une fraction rationnelle.}

\notation{On note $\K(X)$ l'ensemble des fractions rationnelles à coefficients dans $\K$.}

\begin{proposition}
On munit $\K(X)$ de différentes relations :
\begin{itemize}[label=\textbullet]
	\item $\ds{\frac{A}{B}=\frac{C}{D}}$ si et seulement si $AD=BC$.
	\item $\ds{\frac{A}{B}+\frac{C}{D} = \frac{AD+BC}{BD}}$ et $\ds{\frac{A}{B}\times\frac{C}{D}=\frac{AC}{BD}}$.
\end{itemize}	
\end{proposition}


\remarque{On remarque qu'il n'y a pas unicité de l'écriture d'une fraction rationnelle. Par exemple,
\[ \frac{X^2+X^3}{X^2+X} = \frac{X+X^2}{X+1} \]
puisque $(X^2+X^3)(X+1)=(X^2+X)(X+X^2)$. On dit que ce sont des \textbf{représentants} de la même fraction rationnelle.\\
On dit alors qu'une fraction est \textbf{irréductible} si numérateur et dénominateur n'ont pas de facteurs irréductibles unitaires communs. Il existe alors un seul représentant irréductible d'une fraction rationnelle avec un dénominateur unitaire. 
}

\begin{methode}
Pour déterminer un représentant irréductible d'une fraction rationnelle, on factorise numérateur et dénominateur et on simplifie les éventuels facteurs communs.	
\end{methode}


\exemple{Mettre $\ds{\frac{X^2-1}{X^3-6X+5}}$ sous forme irréductible.}

\begin{prof}
\solution{En cherchant les racines, on obtient
\[ X^2-1=(X-1)(X+1) \qeq X^3-6X+5=(X-1)(X^2+X-5) \]
Ainsi,
\[ \frac{X^2-1}{X^3-6X+5}=\frac{X+1}{X^2+X-5} \]
et la fraction est irréductible, puisque $-1$ n'est pas racine de $X^2+X-5$.
}
\end{prof}


\begin{eleve}
\begin{solu_eleve}
\notes[1.8ex]{7}{\textwidth}
\end{solu_eleve}	
\end{eleve}

	%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
	\subsection{Degré et partie entière d'une fraction}
	%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

On définit le degré d'une fraction rationnelle par analogie :

\definition{Soient $A$ et $B$ deux polynômes de $\K[0]$, avec $B\neq 0$. Alors, on pose
\[ \deg \left(\frac{A}{B}\right) =\deg A-\deg B \]
avec la convention $\deg(0)=-\infty$.}

\remarque{Ainsi, le degré d'une fraction rationnelle ne dépend pas de son représentant.}

\propriete{Soient $F$ et $G$ deux fractions rationnelles de $\K(X)$, et $k\in \K^*$.
\begin{numerote}
	\item $\deg F \in \Z\cup\{-\infty\}$.
	\item $\deg \frac{1}{F}=-\deg(F)$.
	\item $\deg (F+G)\leq \max(\deg F,\deg G)$.
	\item $\deg FG=\deg F +\deg G$.
	\item $\deg (kF)=\deg(F)$.
\end{numerote}
}

\exemple{Ainsi, la fraction rationnelle $F(X)=\frac{X+1}{X^2+1}$ est de degré $\deg F = 1-2=-1$.}

\begin{proposition}[Partie entière] 
Soit $F\in \K(X)$. Il existe un unique polynôme $P\in \K[0]$ et une unique fraction rationnelle $G\in \K(X)$, tel que $F=P+G$, avec $\deg (G)<0$. Le polynôme $P$ est appelé \textbf{partie entière} de $F$.	
\end{proposition}



\begin{methode}
Pour déterminer la partie entière de $F=\frac{A}{B}$ (avec $A,B$ des polynômes), on détermine la division euclidienne de $A$ par $B$. En notant $Q$ le quotient, et $R$ le reste, on a alors
\[ A=BQ+R \Leftrightarrow \frac{A}{B} = Q + \frac{R}{B} \]
avec $\ds{\deg\left(\frac{R}{B}\right)<0}$.
$Q$ représente donc la partie entière de $F$.	
\end{methode}


\exemple{Déterminer la partie entière de $\ds{\frac{X^3-X^2+1}{X^2+1}}$.}

\begin{prof}
\solution{Par division euclidienne, on obtient
\[ X^3-X^2+1=(X^2+1)(X-1)-X+2 \]
avec $\deg (-X+2) < \deg(X^2+1)$. On a alors
\[ \frac{X^3-X^2+1}{X^2+1} = X-1+ \frac{-X+2}{X^2+1} \]
et donc $X-1$ est la partie entière de $\ds{\frac{X^3-X^2+1}{X^2+1}}$.
}	
\end{prof}

\begin{eleve}
\begin{solu_eleve}
\notes[1.8ex]{7}{\textwidth}
\end{solu_eleve}	
\end{eleve}

	%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
	\subsection{Pôles et décomposition en éléments simples}
	%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
	
\definition{Soit $F\in \K(X)$ une fraction rationnelle. On appelle :
\begin{itemize}[label=\textbullet]
	\item \textbf{zéros} de $F$ les racines du numérateur d'un représentant irréductible de $F$.
	\item \textbf{pôles} de $F$ les racines du dénominateur d'un représentant irréductible de $F$.
	\item \textbf{multiplicité} d'un pôle la multiplicité de la racine correspondante du dénominateur d'un représentant irréductible de $F$.
\end{itemize}
}

\exemple{Pour $\ds{F(X)=\frac{X^2-1}{X+2}}$, $1$ et $-1$ sont les racines de $F$, et $-2$ est un pôle simple de $F$.}

\begin{proposition}[Décomposition en éléments simples dans $\C$] 
Soit $F \in \C(X)$ une fraction rationnelle. En prenant un représentant irréductible de $F$, on l'écrit sous la forme factorisée 
\[ F(X) = \frac{P(X)}{k \ds{\prod_{i=1}^n (X-a_i)^{\alpha_i}}} \]
où les $a_i$ sont des nombres complexes, et les $\alpha_i$ des entiers naturels non nuls. Alors, il existe un unique polynôme $E$, et des uniques nombres complexes $c_{i,j}$, vérifiant
\[ F(X) = E(X) + \sum_{i=1}^n \left (\sum_{j=1}^{\alpha_i} \frac{c_{i,j}}{(X-a_i)^j} \right) \]
Cette décomposition est appelée \textbf{décomposition en éléments simples} dans $\C$ de $F$.	
\end{proposition}


\begin{methode}
Pour déterminer la décomposition en éléments simples dans $\C$, on écrit l'allure de la décomposition, puis on détermine les nombres complexes $c_{i,j}$ en utilisant différentes méthodes :
\begin{itemize}[label=\textbullet]
	\item On peut multiplier par $(X-a_i)^{\alpha_i}$ puis prendre $X=a_i$ pour déterminer les coefficients d'ordre le plus élevé.
	\item On peut utiliser les propriétés du polynôme (parité, imparité) pour déterminer certains coefficients.
	\item Enfin, on peut soit prendre des valeurs particulières (et résoudre un système), soit multiplier par $X^k$ et prendre la limite en $+\infty$ pour déterminer certaines valeurs restantes.
\end{itemize}	
\end{methode}


\exemple{Décomposer en éléments simples la fraction rationnelle 
\[ F(X) = \frac{X+1}{(X-i)X(X+2)^2} \] 
}

\begin{prof}
\solution{Il existe quatre complexes $a, b, c$ et $d$ tels que
\[ \frac{X+1}{(X-i)X(X+2)^2} = \frac{a}{X-i}+\frac{b}{X}+\frac{c}{X+2}+\frac{d}{(X+2)^2} \]
En multipliant par $X-i$ et prenant $X=i$, on obtient 
\[ a = \frac{i+1}{i(i+2)^2} = \frac{1-7i}{25} \]
De même pour $b$ et $d$ (en multipliant respectivement par $X$ et $(X+2)^2$), on obtient
\[ b = \frac{1}{4}i \qeq d=\frac{-2+i}{10} \]
Pour obtenir $c$, on peut multiplier par $X$ et faire tendre $X$ vers $+\infty$. On obtient alors $0 = a+b+c$, soit $c=-a-b=\frac{1}{25}+\frac{3}{100}i$.
}	
\end{prof}


\begin{eleve}
\begin{solu_eleve}
\notes[1.8ex]{15}{\textwidth}
\end{solu_eleve}	
\end{eleve}

\begin{proposition}[Décomposition en éléments simples dans $\R$] 
Dans le cas réel, il faut se souvenir que les polynômes irréductibles ne sont pas uniquement de la forme $(X-a_i)$ mais peuvent être des polynômes de degré $2$ à discriminant strictement négatif. Les décompositions s'écriront alors sous la forme $\frac{a}{(X-a_i)^{p}}$ avec $a\in \R$, ou $\frac{aX+b}{(aX^2+bX+c)^p}$ dans le cas d'un trinôme à discriminant strictement négatif.
\end{proposition}


\exemple{Décomposer en éléments simples la fraction rationnelle
\[ F(X) = \frac{X+1}{(X-2)(X^2+X+1)} \]
}

\begin{prof}
\solution{Le trinôme $X^2+X+1$ est irréductible dans $\R[0]$ (car à discriminant strictement négatif). Ainsi, il existe trois réels $a,b$ et $c$ tels que
\[ F(X)=\frac{a}{X-2}+\frac{bX+c}{X^2+X+1} \]
Par la méthode classique, on obtient $a=\frac{3}{7}$. Calculons alors $F(X)-\frac{3/7}{X-2}$ : 
\[ \frac{X+1 - 3/7(X^2+X+1)}{(X-2)(X^2+X+1)} = \frac{-3/7X^2+4/7X+4/7}{(X-2)(X^2+X+1)} \]
soit
\[ \frac{X+1 - 3/7(X^2+X+1)}{(X-2)(X^2+X+1)} = \frac{1}{7} \frac{-3X^2+4X+4}{(X-2)(X^2+X+1)}  = \frac{1}{7} \frac{(X-2)(-3X-2)}{(X-2)(X^2+X+1)}=\frac{1}{7}\frac{-3X-2}{X^2+X+1}\]
Ainsi, $b=-\frac{3}{7}$ et $c=-\frac{2}{7}$.
}	
\end{prof}



\begin{eleve}
\begin{solu_eleve}
\notes[1.8ex]{15}{\textwidth}
\end{solu_eleve}	
\end{eleve}