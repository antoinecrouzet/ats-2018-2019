%!PS
%%BoundingBox: -128 -15 128 128 
%%HiResBoundingBox: -127.80904 -14.42323 127.80904 127.80904 
%%Creator: MetaPost 2.000
%%CreationDate: 2018.01.24:1422
%%Pages: 1
%*Font: cmr10 9.96265 9.96265 2d:9f
%%BeginProlog
%%EndProlog
%%Page: 1 1
 1 0 0 setrgbcolor 0 2 dtransform truncate idtransform setlinewidth pop
 [] 0 setdash 1 setlinecap 1 setlinejoin 10 setmiterlimit
newpath -113.3858 113.3858 moveto
0 0 lineto
113.3858 113.3858 lineto stroke
 0 0 0 setrgbcolor 0.5 0 dtransform exch truncate exch idtransform pop setlinewidth
newpath -113.3858 -2.83482 moveto
-113.3858 2.83482 lineto stroke
-117.53691 -12.25522 moveto
(-4) cmr10 9.96265 fshow
newpath -85.03935 -2.83482 moveto
-85.03935 2.83482 lineto stroke
-89.19046 -12.25522 moveto
(-3) cmr10 9.96265 fshow
newpath -56.6929 -2.83482 moveto
-56.6929 2.83482 lineto stroke
-60.84401 -12.25522 moveto
(-2) cmr10 9.96265 fshow
newpath -28.34645 -2.83482 moveto
-28.34645 2.83482 lineto stroke
-32.49756 -12.25522 moveto
(-1) cmr10 9.96265 fshow
newpath 28.34645 -2.83482 moveto
28.34645 2.83482 lineto stroke
25.8558 -12.25522 moveto
(1) cmr10 9.96265 fshow
newpath 56.6929 -2.83482 moveto
56.6929 2.83482 lineto stroke
54.20226 -12.25522 moveto
(2) cmr10 9.96265 fshow
newpath 85.03935 -2.83482 moveto
85.03935 2.83482 lineto stroke
82.5487 -12.25522 moveto
(3) cmr10 9.96265 fshow
newpath 113.3858 -2.83482 moveto
113.3858 2.83482 lineto stroke
110.89516 -12.25522 moveto
(4) cmr10 9.96265 fshow
 0 0.5 dtransform truncate idtransform setlinewidth pop
newpath -2.83482 28.34645 moveto
2.83482 28.34645 lineto stroke
-7.9813 25.13625 moveto
(1) cmr10 9.96265 fshow
newpath -2.83482 56.6929 moveto
2.83482 56.6929 lineto stroke
-7.9813 53.4827 moveto
(2) cmr10 9.96265 fshow
newpath -2.83482 85.03935 moveto
2.83482 85.03935 lineto stroke
-7.9813 81.82915 moveto
(3) cmr10 9.96265 fshow
newpath -2.83482 113.3858 moveto
2.83482 113.3858 lineto stroke
-7.9813 110.1756 moveto
(4) cmr10 9.96265 fshow
-7.08128 -12.20557 moveto
(0) cmr10 9.96265 fshow
newpath -127.55904 0 moveto
127.55904 0 lineto stroke
newpath 123.86191 -1.53143 moveto
127.55904 0 lineto
123.86191 1.53143 lineto
 closepath
gsave fill grestore stroke
 0.5 0 dtransform exch truncate exch idtransform pop setlinewidth
newpath 0 -14.17323 moveto
0 127.55904 lineto stroke
 0 0.5 dtransform truncate idtransform setlinewidth pop
newpath 1.53113 123.86267 moveto
0 127.55904 lineto
-1.53113 123.86267 lineto
 closepath
gsave fill grestore stroke
showpage
%%EOF
