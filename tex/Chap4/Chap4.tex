\objectif{Ce chapitre est souvent délaissé par les élèves au concours. Lors des épreuves écrites, il est souvent demandé certaines propriétés après avoir étudié une courbe. \\
Ce chapitre repose sur les bases connues des années précédentes, en les consolidant.
}

\large \textbf{Objectifs} \normalsize

\textit{La liste ci-dessous représente les éléments à maitriser absolument. Pour cela, il faut savoir refaire les exemples et exercices du cours, ainsi que ceux de la feuille de TD.}

\begin{numerote}
	\item Concernant le produit scalaire :
			\begin{itemize}[label=\textbullet]
				\item Connaître les définitions de produit scalaire\dotfill $\Box$
				\item Savoir déterminer une mesure d'angle entre deux vecteurs à l'aide du produit scalaire\dotfill $\Box$ 
			\end{itemize}
	\item Concernant le déterminant :
			\begin{itemize}[label=\textbullet]
				\item Connaître la notion de déterminant et les différentes définitions.\dotfill $\Box$
				\item Savoir utiliser le déterminant pour déterminer l'aire d'un parallélogramme\dotfill $\Box$
			\end{itemize}
	\item Concernant les droites :
		\begin{itemize}[label=\textbullet]
			\item savoir déterminer les équations cartésiennes de droite\dotfill $\Box$
			\item savoir déterminer un système d'équations paramétriques de droite\dotfill $\Box$
			\item savoir déterminer les intersections éventuelles de deux droites\dotfill $\Box$
			\item savoir déterminer la distance d'un point à une droite\dotfill $\Box$
		\end{itemize}
	\item Concernant les cercles :
		\begin{itemize}[label=\textbullet]
			\item savoir déterminer une équation cartésienne de cercle\dotfill $\Box$
			\item savoir déterminer les caractéristiques d'un cercle concernant une équation cartésienne\dotfill $\Box$
		\end{itemize}
\end{numerote}

\newpage

\section{Repérage dans le plan}

Dans cette partie, on revient sur les bases de la géométrie du plan. On voit également les différents types de coordonnées : cartésiennes, polaires.

	\subsection{Vecteurs}
	
\rappel{Un \textbf{vecteur} $\vectu$ non nul est la donnée de trois éléments :
\begin{itemize}[label=\textbullet]
	\item sa direction : la droite qui porte le vecteur.
	\item son sens : on oriente la droite (de $A$ vers $B$ pour le vecteur $\vecteur{AB}$.
	\item une longueur, appelée \textbf{norme}, et notée $||\vectu||$ (la longueur $AB$ pour le vecteur $\vecteur{AB}$).
\end{itemize}
On définit un vecteur de longueur $0$, appelé \textbf{vecteur nul}, et noté $\vecteur{0}$.
}
	
\rappel{[Addition de deux vecteurs] Soient $\vectu$ et $\vectv$ deux vecteurs. On introduit trois points $A,B$ et $C$ tels que $\vecteur{AB}=\vectu$ et $\vecteur{BC}=\vectv$. On définit alors $\vectu+\vectv = \vecteur{AC}$.
\begin{center}\input{tex/Chap4/pic/somme}\end{center}
}

\begin{proposition}[Relation de Chasles] 
Pour tous points $A,B$ et $C$ du plan, on a \[ \vecteur{AB}+\vecteur{BC}=\vecteur{AC} \]	
\end{proposition}


\rappel{[Multiplication par un scalaire] Soit $\vectu$ un vecteur du plan, et $k$ un réel. On définit le vecteur $k\vectu$ comme le vecteur ayant la même direction que $\vectu$, le même sens si $k>0$, le sens contraire si $k<0$, et de norme $|k|. ||\vectu||$.
\begin{center}\input{tex/Chap4/pic/scalaire}\end{center}
}
	
\definition{L'ensemble des vecteurs du plan, muni de l'addition et la multiplication par un scalaire, vérifie certaines propriétés : 
\begin{itemize}[label=\textbullet]
	\item le vecteur nul est le neutre de l'addition : $\vectu+\vecteur{0}=\vecteur{0}+\vectu=\vectu$.
	\item l'addition est commutative : $\vectu+\vectv=\vectv+\vectu$.
	\item l'addition est associative : $(\vectu+\vectv)+\vecteur{w}=\vectu+(\vectv+\vecteur{w})$.
	\item tout vecteur admet un opposé : $\vectu+(-\vectu)=(-\vectu)+\vectu=\vecteur{0}$.
	\item $1$ est le neutre de la multiplication : $1.\vectu=\vectu$.
	\item la multiplication est distributive sur l'addition :
		\[ k.(\vectu+\vectv)=k\vectu+k\vectv \qeq (k+k').\vectu=k\vectu+k'\vectu \]
\end{itemize}
Les quatre premières propriétés font de l'espace des vecteurs, muni de l'addition, un \textbf{groupe abélien}. L'ensemble de ces propriétés forme un \textbf{espace vectoriel}. Nous y reviendrons plus tard cette année.}

\rappel{Deux vecteurs du plan $\vectu$ et $\vectv$ sont \textbf{colinéaires} si et seulement s'il existe un réel $k$ tel que $\vectu=k\vectv$ ou $\vectv=k\vectu$.}

\remarque{~\begin{itemize}
	\item Deux vecteurs colinéaires ont donc la même direction.	
	\item Le vecteur nul est colinéaire à tout vecteur du plan.
\end{itemize}
}

\definition{Soient $\vectu$ et $\vectv$ deux vecteurs du plan. On dit que la famille $(\vectu;\vectv)$ est \textbf{libre} si les deux vecteurs ne sont pas colinéaires. Sinon, on dit que la famille est \textbf{liée}.}

 	
	\subsection{Base du plan et repère}

\definition{On appelle \textbf{base} du plan la donnée de deux vecteurs $\base$ non colinéaires.\\On dit que la base est 
\begin{itemize}[label=\textbullet]
	\item \textbf{orthogonale} si et seulement si $\vecti$ et $\vectj$ sont orthogonaux.
	\item \textbf{orthonormée} ou orthonormale si et seulement si elle est orthogonale, et si $||\vecti||=||\vectj||$. 
\end{itemize}
\begin{center}\input{tex/Chap4/pic/base}\end{center}
}

\begin{proposition}
Soit $\base$ une base du plan. Alors pour tout vecteur $\vectu$ du plan, il existe deux réels uniques $x$ et $y$ tels que \[ \vectu=x\vecti+y\vectj \]
$x$ est appelé \textbf{abscisse} de $\vectu$, $y$ est appelé \textbf{ordonnée} et $(x,y)$ représente les \textbf{coordonnées} du vecteur $\vectu$ dans la base $\base$.\\On notera 
\[ \vectu\matrice{x\\y} \quad \text{ou}\quad \vectu(x;y) \]
\begin{center}\input{tex/Chap4/pic/coordvec}\end{center}	
\end{proposition}


\definition{[Repère] On appelle \textbf{repère} du plan la donnée d'un point $O$, appelé \textbf{origine} du repère, et d'une base $\base$.\\Le repère est dit orthogonal si la base $\base$ est orthogonale, et orthonormé si la base $\base$ est orthonormée.}

\begin{proposition}
Soit $\repere$ un repère du plan. Pour tout point $A$ du plan, il existe deux réels $x$ et $y$ uniques vérifiant \[ \vecteur{OA}=x\vecti+y\vectj \]
$x$ est appelé abscisse du point $A$, $y$ ordonnée du point $A$. Le couple $(x;y)$ représente les \textbf{coordonnées cartésiennes} de $A$, et on note $A(x;y)$.	
\end{proposition}



	\subsection{Coordonnées polaires}
	
\definition{Soit $\repere$ un repère orthonormé du plan. 	Soit $M$ un point du plan, distinct de l'origine. $M$ est uniquement déterminé par une mesure de l'angle $\theta=(\vecti;\vecteur{OM})$ et par la longueur $OM$.
\begin{center}
	\input{tex/Chap4/pic/polaire}
\end{center}
	Ainsi, $(OM,\theta)$ identifie $M$, et représente les \textbf{coordonnées polaires} du point $M$.\\Le point $O$ n'a pas de coordonnées polaires.}
	
\remarque{Dans le plan complexe $\repcom$, si $M$ (distinct de l'origine) a pour forme exponentielle $\ds{r\eu{i\theta}}$, alors $(r,\theta)$ représente des coordonnées polaires de $M$.}

\begin{proposition}
Soit $\repere$ un repère orthonormé, et $M$ un point distinct de $O$. 
\begin{itemize}[label=\textbullet]
	\item Si $M$ a pour coordonnées cartésiennes $(x;y)$ alors $r=\sqrt{x^2+y^2}$ et $\theta$, identifié par $\cos(\theta)=\frac{x}{r}$ et $\sin(\theta)=\frac{y}{r}$ forme les coordonnées polaires de $M$.
	\item Réciproquement, si $M$ a pour coordonnées polaires $(r, \theta)$ alors $x=r\cos(\theta)$ et $y=r\sin(\theta)$ représentent les coordonnées cartésiennes de $M$.
\end{itemize} 	
\end{proposition}


\preuve{En se plaçant dans le plan complexe, on revient à la conversion forme algébrique / forme exponentielle, vue dans le chapitre $3$.}		

\exercice{Soit $\repere$ un repère orthonormé. Soit $A$ de coordonnées cartésienne $(2;-2)$ et $B$ de coordonnées polaire $\left(2; \frac{\pi}{6}\right)$. Déterminer les coordonnées polaires de $A$ et les coordonnées cartésiennes de $B$.}

\begin{prof}
\solution{On note $r=OA$. Alors $r=\sqrt{2^2+(-2)^2}=\sqrt{8}=2\sqrt{2}$. Enfin, $\theta$ vérifie
\[ \left \{ \begin{array}{l}
		\cos(\theta)=\frac{2}{2\sqrt{2}}=\frac{\sqrt{2}}{2}\\
		\sin(\theta)=\frac{-2}{2\sqrt{2}}=-\frac{\sqrt{2}}{2}
\end{array}\right. \Leftrightarrow \theta=-\frac{\pi}{4} ~[2\pi]\]
Ainsi, $\left(2\sqrt{2}; -\frac{\pi}{4}\right)$ représente les coordonnées polaires de $A$.\\
Enfin, $x_B=2\cos\left(\frac{\pi}{6}\right)=\sqrt{3}$ et $y_B=2\sin\left(\frac{\pi}{6}\right)=1$ représentent les coordonnées cartésiennes de $B$.
}	
\end{prof}

\begin{eleve} 
\begin{solu_eleve}
\notes[1.8ex]{6}{\textwidth}
\end{solu_eleve}
\end{eleve}

\section{Produit scalaire}

Dans l'ensemble de cette partie, $\repere$ représente un repère orthonormé direct.	

	\subsection{Définitions}	
	
\definition{Soient $\vectu$ et $\vectv$ deux vecteurs non nuls du plan. On appelle \textbf{produit scalaire} de $\vectu$ et $\vectv$, et on note $\vectu\cdot \vectv$, le nombre réel
\[ \vectu\cdot \vectv = ||\vectu||.||\vectv||.\cos(\vectu; \vectv) \]
Si $\vectu$ ou $\vectv$ sont nuls, on pose $\vectu\cdot \vectv=0$.}


\exemple{Puisque $\repere$ est orthonormé, on a
\[ \vecti\cdot \vecti = ||\vecti||.||\vecti||.\cos(\vecti;\vecti)=1,\quad \vectj\cdot \vectj = ||\vectj||.||\vectj||.\cos(\vectj;\vectj)=1 \qeq \vecti\cdot \vectj = ||\vecti||.||\vectj||.\cos(\vecti;\vectj)=0 \]
}

\propriete{Pour tout vecteur $\vectu$, on a
\[ \vectu\cdot \vectu=||\vectu||^2 \]
}

\begin{prof}
\solution{En effet, 
\begin{eqnarray*}
	\vectu\cdot \vectu &=& ||\vectu||.||\vectu||.\cos(\underbrace{\vectu;\vectu}_{=0~[2\pi]}) \\
		&=& ||\vectu||^2
\end{eqnarray*}
}	
\end{prof}

\begin{eleve} 
\begin{solu_eleve}
\notes[1.8ex]{4}{\textwidth}
\end{solu_eleve}
\end{eleve}

\begin{proposition}
On dispose des définitions équivalentes suivantes. Pour tous vecteurs $\vectu\matrice{x\\y}$ et $\vectv\matrice{x'\\y'}$, on a 
\begin{itemize}[label=\textbullet]
	\item $\vectu\cdot \vectv =xx'+yy'$.
	\item $\ds{\vectu\cdot \vectv= \frac{||\vectu+\vectv||^2-||\vectu||^2-||\vectv||^2}{2}}$.
\end{itemize}	
\end{proposition}


\exemple{Soient $\vectu\matrice{2\\1}$ et $\vectv\matrice{3\\4}$ deux vecteurs. Déterminer $\vectu\cdot \vectv$.
}

\begin{prof}
\solution{On a $\vectu\cdot \vectv=2\times 3+1\times 4 = 10$.
}
\end{prof}

\begin{eleve} 
\begin{solu_eleve}
\notes[1.8ex]{2}{\textwidth}
\end{solu_eleve}
\end{eleve}


\begin{proposition}
Soient $\vectu$ et $\vectv$ deux vecteurs non nuls. Soient $A$ et $B$ deux points du plan, tels que $\vecteur{OA}=\vectu$ et $\vecteur{OB}=\vectv$. On note $H$ le projeté orthogonal de $B$ sur $(OA)$. 
\begin{center}
\input{tex/Chap4/pic/ortho}	
\end{center}

Alors, $\vectu\cdot \vectv = OA \times OH$ si $\vecteur{OA}$ et $\vecteur{OH}$ sont dans le même sens, $\vectu\cdot \vectv=-OA\times OH$ sinon. \\On notera \[ \vectu\cdot \vectv = \overline{OA}\times \overline{OH} \]	
\end{proposition}


\exemple{Soit $ABCD$ un carré de centre $O$. Calculer $\vecteur{AB}\cdot \vecteur{AO}$.}

\begin{prof}
\solution{Le projeté orthogonal de $O$ sur la droite $(AB)$ est le point $I$ milieu de $[AB]$. Alors
\[ \vecteur{AB}\cdot \vecteur{AO} = AB\times AI = \frac{1}{2}AB^2 \]
}	
\end{prof}


\begin{eleve} 
\begin{solu_eleve}
\notes[1.8ex]{4}{\textwidth}
\end{solu_eleve}
\end{eleve}

\propriete{Pour tous vecteurs $\vectu$,$\vectv$, et $\vecteur{w}$, on a :
\begin{itemize}[label=\textbullet]
	\item Symétrie : $\vectu\cdot \vectv=\vectv\cdot \vectu$.
	\item Bilinéarité : $(\vectu+\vectv)\cdot \vecteur{w}=\vectu\cdot \vecteur{w} + \vectv\cdot \vecteur{w}$ et $\vectu\cdot (\vectv+\vecteur{w})=\vectu\cdot \vectv+\vectu\cdot \vecteur{w}$.
\end{itemize}
}


	\subsection{Applications géométriques}

\begin{proposition}
Deux vecteurs $\vectu$ et $\vectv$ sont orthogonaux si et seulement si $\vectu\cdot \vectv = 0$.	
\end{proposition}


\begin{proposition}
Deux vecteurs $\vectu$ et $\vectv$ sont colinéaires si et seulement si $|\vectu\cdot \vectv|=||\vectu||.||\vectv||$.	
\end{proposition}


\begin{methode}
Pour déterminer une mesure de l'angle $(\vectu;\vectv)$, on détermine le produit scalaire de deux manières différentes : avec les coordonnées, et avec la définition. Cela permet d'obtenir $\cos(\vectu;\vectv)$.	
\end{methode}


\exemple{Soient $\vectu\matrice{2\\1}$ et $\vectv\matrice{\frac{1}{2}\\\frac{3}{2}}$. Déterminer une mesure de l'angle $(\vectu;\vectv)$.}

\begin{prof}
\solution{D'une part, on a \[ \vectu\cdot \vectv = 2\times \frac{1}{2}+1\times \frac{3}{2}=\frac{5}{2} \]
D'autre part, par définition
\[ \vectu\cdot \vectv = ||\vectu||.||\vectv||.\cos(\vectu;\vectv) \]
avec 
\[ ||\vectu||=\sqrt{5} \qeq ||\vectv||=\frac{\sqrt{10}}{2} \]
Ainsi,
\[ \frac{\sqrt{5}\sqrt{10}}{2} \cos(\vectu;\vectv)= \frac{5}{2} \Leftrightarrow \frac{5\sqrt{2}}{2}\cos(\vectu;\vectv)=\frac{5}{2} \Leftrightarrow \cos(\vectu;\vectv)=\frac{\sqrt{2}}{2} \]
Ainsi, $(\vectu;\vectv) = \pm \frac{\pi}{4} ~[2\pi]$. D'après l'orientation, on en déduit donc que $(\vectu;\vectv)=\frac{\pi}{4}$
}	
\end{prof}

\begin{eleve} 
\begin{solu_eleve}
\notes[1.8ex]{10}{\textwidth}
\end{solu_eleve}
\end{eleve}

\remarque{Pour conclure, il manque une information de signe (pour déterminer une mesure de l'angle). On va donc introduire la notion de déterminant.}

\section{Déterminant dans une base orthonormée directe}

Dans l'ensemble de cette partie, $\repere$ représente un repère orthonormé direct.

	\subsection{Définition}

\definition{Soient $\vectu$ et $\vectv$ deux vecteurs non nuls du plan. On appelle \textbf{déterminant}, et on note $[\vectu,\vectv]$, le nombre réel
\[ [\vectu,\vectv ] = ||\vectu||.||\vectv||.\sin(\vectu;\vectv) \]
Si $\vectu$ ou $\vectv$ sont nuls, on pose $[\vectu,\vectv]=0$.\\
On le note également $\det(\vectu,\vectv)$.}

\remarque{Soient $A,B,C$ et $D$ quatre points du plan, tels que $\vecteur{AB}=\vectu$, $\vecteur{AC}=\vectv$ et $ABDC$ est un parallélogramme. 
\begin{center}
	\input{tex/Chap4/pic/para}
\end{center}
Alors $[\vectu,\vectv]$ représente l'aire du parallélogramme $ABDC$.}

\begin{proposition}[Avec coordonnées] 
Soient $\vectu\matrice{x\\y}$ et $\vectv\matrice{x'\\y'}$ deux vecteurs du plan. Alors
\[ [ \vectu, \vectv ] = \left | \begin{array}{cc}x&x'\\y&y'\end{array} \right | = xy'-x'y \]	
\end{proposition}



\exemple{Soient $\vectu\matrice{2\\1}$ et $\vectv\matrice{3\\4}$ deux vecteurs. Déterminer $[\vectu,\vectv]$.
}

\begin{prof}
\solution{On a $[\vectu,\vectv]=2\times 4-3\times 1 = 5$.
}
\end{prof}

\begin{eleve} 
\begin{solu_eleve}
\notes[1.8ex]{2}{\textwidth}
\end{solu_eleve}
\end{eleve}

	\subsection{Propriétés}


\propriete{Pour tous vecteurs $\vectu$,$\vectv$, et $\vecteur{w}$, on a :
\begin{itemize}[label=\textbullet]
	\item Antisymétrie : $[\vectv, \vectu]=-[\vectu, \vectv]$.
	\item Bilinéarité : $[(\vectu+\vectv), \vecteur{w}]=[\vectu, \vecteur{w}] + [\vectv, \vecteur{w}]$ et $[\vectu, (\vectv+\vecteur{w})]=[\vectu, \vectv]+[\vectu, \vecteur{w}]$.
\end{itemize}
}

\begin{proposition}
Deux vecteurs $\vectu$ et $\vectv$ sont colinéaires si et seulement si $[\vectu,\vectv]=0$.	
\end{proposition}


\preuve{~\begin{itemize}[label=\textbullet]
	\item Si $[\vectu,\vectv]=0$, alors soit $||\vectu||=0$, et donc $\vectu=\vecteur{0}$; soit $||\vectv=0||$, et donc $\vectv=\vecteur{0}$; soit $\sin(\vectu;\vectv)=0$, c'est-à-dire $(\vectu;\vectv)=0~[\pi]$. Dans tous les cas, $\vectu$ et $\vectv$ sont colinéaires.
	\item Réciproquement, si $\vectu$ et $\vectv$ sont colinéaires, soit $||\vectu||=0$ ou $||\vectv||=0$, soit $(\vectu;\vectv)=0~[\pi]$, c'est-à-dire $\sin(\vectu;\vectv)=0$ : ainsi, dans tous les cas $[\vectu;\vectv]=0$.
\end{itemize}
}

\section{Droites}

	\subsection{Définitions}

\definition{Soit $\vectu$ un vecteur non nul du plan, et $A$ et $B$ deux points distincts du plan. On appelle :
\begin{itemize}[label=\textbullet]
	\item \textbf{droite vectorielle} dirigée par $\vectu$ l'ensemble des vecteurs colinéaires à $\vectu$ :
		\[ \mathcal{D}_{\vectu} = \left \{ \lambda \vectu, ~\lambda \in \R \right \} \]
	\item \textbf{droite} (ou \textbf{droite affine}) passant par $A$ et dirigée par $\vectu$ l'ensemble des points $M$ tels que $\vecteur{AM}$ et $\vectu$ sont colinéaires :
		\[ \mathcal{D}_{A,\vectu} = \left \{ M~\text{du plan},\quad\exists~\lambda \in \R,\quad \vecteur{AM}=\lambda \vectu \right \} \]
		$\vectu$ est appelé \textbf{vecteur directeur} de la droite.
	\item droite passant par $A$ et $B$ la droite passant par $A$ et dirigée par $\vecteur{AB}$.
\end{itemize}
}

\definition{[Vocabulaire] ~\begin{itemize}[label=\textbullet]
	\item Des points sont dits \textbf{alignés} s'ils sont sur une même droite.
	\item Deux droites sont dites \textbf{parallèles} si leurs vecteurs directeurs sont colinéaires.
	\item Deux droites sont \textbf{perpendiculaires} si leurs vecteurs directeurs sont orthogonaux.
\end{itemize}
}

\remarque{A partir de ces définitions, on peut redémontrer les résultats connus, par exemple que si deux droites sont parallèles, toute droite perpendiculaire à l'une est perpendiculaire à l'autre.}

\definition{Soit $\mathcal{D}$ la droite passant par $A$ et de vecteur directeur $\vectu$. On appelle \textbf{vecteur normal} de la droite $\mathcal{D}$ tout vecteur orthogonal à $\vectu$.}

	\subsection{Représentations cartésiennes et paramétriques}

\begin{theoreme}
Soit $\mathcal{D}$ la droite passant par $A(x_A;y_A)$ et de vecteur directeur $\vectu\matrice{a\\b}$. $M(x;y)$ est sur la droite $\mathcal{D}$ si et seulement si $b(x-x_A)-a(y-y_A)=0$.\\Cette écriture est appelée \textbf{équation cartésienne} de la droite $\mathcal{D}$.\\~\\
Réciproquement, un ensemble ayant pour équation $ax+by+c=0$ est une droite, de vecteur directeur $\vectu\matrice{-b\\a}$ et de vecteur normal $\vecteur{n}\matrice{a\\b}$.	
\end{theoreme}


\preuve{Par définition, $M$ est sur la droite $\mathcal{D}$ si et seulement si $\vecteur{AM}$ et $\vectu$ sont colinéaires, et donc si et seulement si $[ \vecteur{AM},\vectu ]=0$. Cela donne donc
\[ (x-x_A)b - (y-y_A)a = 0 \]
La réciproque se fait également aisément, en prenant un point quelconque de la droite.
}

\begin{methode}
Pour déterminer une équation cartésienne de la droite passant par $A(x_A;y_A)$ et de vecteur directeur $\vectu\matrice{a\\b}$, on écrit son équation sous la forme $bx-ay+c=0$ et on cherche $c$ avec les coordonnées de $A$.	
\end{methode}


\exemple{Déterminer une équation cartésienne de la droite, passant par $A(1;1)$ et de vecteur directeur $\vectu\matrice{1\\2}$.}

\begin{prof}
\solution{Cette droite $\mathcal{D}$ a pour équation
\[ 2x-y+c=0 \quad\text{avec}\quad 2-1+c=0 \Leftrightarrow c=-1 \]
et donc $2x-y+1=0$ est une équation cartésienne de $\mathcal{D}$.}
\end{prof}

\begin{eleve} 
\begin{solu_eleve}
\notes[1.8ex]{6}{\textwidth}
\end{solu_eleve}
\end{eleve}


\begin{methode}
Pour déterminer une équation cartésienne de la droite passant par $A(x_A;y_A)$ et de vecteur normal $\vecteur{n}\matrice{a\\b}$, on écrit son équation sous la forme $ax+by+c=0$ et on cherche $c$ avec les coordonnées de $A$.	
\end{methode}


\exemple{Déterminer une équation cartésienne de la droite, passant par $A(1;1)$ et de vecteur normal $\vectu\matrice{1\\2}$.}

\begin{prof}
\solution{Cette droite $\mathcal{D}$ a pour équation
\[ x+2y+c=0 \quad\text{avec}\quad 1+2+c=0 \Leftrightarrow c=-3 \]
et donc $x+2y-3=0$ est une équation cartésienne de $\mathcal{D}$.}
\end{prof}

\begin{eleve} 
\begin{solu_eleve}
\notes[1.8ex]{6}{\textwidth}
\end{solu_eleve}
\end{eleve}

\begin{theoreme}
Soit $\mathcal{D}$ la droite passant par $A(x_A;y_A)$ et de vecteur directeur $\vectu\matrice{a\\b}$ non nul. Alors $M(x;y)$ est sur la droite $\mathcal{D}$ si et seulement si
\[ \left \{ \begin{array}{ccl} x&=&x_A+ta\\y&=&y_A+tb \end{array}\right. \quad \text{avec}\quad t\in \R \]
Cette représentation est appelée \textbf{système d'équations paramétriques}.	
\end{theoreme}


\preuve{$M(x;y)$ est sur la droite $\mathcal{D}$ si et seulement si $\vecteur{AM}$ et $\vectu$ sont colinéaires, c'est-à-dire s'il existe $t\in \R$ tel que $\vecteur{AM}=t\vectu$, ce qui s'écrit 
\[ \left \{ \begin{array}{ccl} x&=&x_A+ta\\y&=&y_A+tb \end{array}\right. \quad \text{avec}\quad t\in \R \]
}

\exemple{Soit $\mathcal{D}$ la droite, passant par $A(1;2)$ et de vecteur directeur $\vectu\matrice{1\\-1}$. Alors $\mathcal{D}$ a comme système d'équations paramétriques
\[ \left \{ \begin{array}{ccl} x&=&1+t\\y&=&2-t \end{array}\right. \quad t\in \R \]
}

\rappel{On appelle \textbf{médiatrice} du segment $[AB]$ l'ensemble des points $M$ a égale distance de $A$ et $B$.}

\exercice{Déterminer une équation cartésienne de la médiatrice du segment $[AB]$, avec $A(1;2)$ et $B(-1;4)$.}

\begin{prof}
\solution{$M(x;y)$ est sur la médiatrice du segment $[AB]$ si et seulement si $AM=BM$, c'est-à-dire
\begin{eqnarray*}
	AM=BM &\Leftrightarrow& AM^2=BM^2 \\&\Leftrightarrow& (x-1)^2+(y-2)^2 = (x+1)^2+(y-4)^2 \\ &\Leftrightarrow& -4x+4y-12 = 0
\end{eqnarray*}
Ainsi, une équation cartésienne de la médiatrice de $[AB]$ est $-x+y-3=0$.
}
\end{prof}

\begin{eleve} 
\begin{solu_eleve}
\notes[1.8ex]{6}{\textwidth}
\end{solu_eleve}
\end{eleve}


	\subsection{Intersection de deux droites}

\begin{proposition}
Deux droites peuvent avoir $0$ (cas des droites parallèles), $1$ (cas des droites sécantes) ou une infinité (cas des droites confondues) points d'intersection.	
\end{proposition}


\begin{methode}
Pour déterminer l'intersection éventuelle de deux droites on peut utiliser 
\begin{itemize}[label=\textbullet]
	\item Deux équations cartésiennes, et résoudre le système obtenu pour obtenir $(x;y)$;
	\item Une équation cartésienne et un système d'équations paramétriques, et on injecte pour déterminer $t$.
\end{itemize}
On peut également utiliser deux systèmes d'équations paramétriques et obtenir les valeurs de $t$ et $t'$.	
\end{methode}


\exemple{Soient $\mathcal{D}$ et $\mathcal{D}'$ deux droites d'équations cartésiennes respectives $x+y+3=0$ et $-x+2y-6=0$. Déterminer les intersections éventuelles de $\mathcal{D}$ et $\mathcal{D}'$.}

\begin{prof}
\solution{$M(x;y)$ est sur l'intersection si et seulement si
\[ \left \{ \begin{array}{lllllll} x&+&y&+&3&=&0 \\ -x&+&2y&-&6&=&0 \end{array} \right. \Leftrightarrow
	\left \{ \begin{array}{lllllll} x&+&y&+&3&=&0 \\ &3y&-&3&=&0 \end{array} \right. \Leftrightarrow
\left \{ \begin{array}{ccc} x&=&-4 \\ y&=&1 \end{array} \right. \]
Ainsi, $\mathcal{D}$ et $\mathcal{D}'$ ont un unique point d'intersection : $A(-4;1)$.
}	
\end{prof}

\begin{eleve} 
\begin{solu_eleve}
\notes[1.8ex]{6}{\textwidth}
\end{solu_eleve}
\end{eleve}

\exemple{Soient $\mathcal{D}$ d'équation cartésienne $x+2y+1=0$ et $\mathcal{D}'$ de système d'équations paramétriques $\left \{ \begin{array}{lll}x&=&2-t\\y&=&1+t \end{array}\right.$. Déterminer les intersections éventuelles de $\mathcal{D}$ et $\mathcal{D}'$.
}

\begin{prof}
\solution{$M(x;y)$ est sur l'intersection si et seulement si
\begin{eqnarray*}
	(2-t) + 2 (1+t) + 1 = 0 &\Leftrightarrow& 5+t=0\\&\Leftrightarrow& t=-5
\end{eqnarray*}
Ainsi, $\mathcal{D}$ et $\mathcal{D}'$ ont un unique point d'intersection : $x=2-(-5)=7$ et $y=1-5=-4$.
}	
\end{prof}

\begin{eleve} 
\begin{solu_eleve}
\notes[1.8ex]{6}{\textwidth}
\end{solu_eleve}
\end{eleve}

	\subsection{Projeté orthogonal d'un point sur une droite}

\begin{methode}
Pour déterminer les coordonnées du projeté orthogonal d'un point $A$ sur une droite de vecteur directeur $\vectu$, on cherche $M(x;y)$ sur la droite vérifiant $\vecteur{AM}\cdot \vectu=0$.	
\end{methode}


\exemple{Déterminer les coordonnées du projeté orthogonal du point $A(1;1)$ sur la droite $\mathcal{D}$ d'équation cartésienne $x+y-3=0$.}

\begin{prof}
\solution{Un vecteur directeur de la droite $\mathcal{D}$ est $\vectu\matrice{-1\\1}$. On cherche $M(x;y)$ vérifiant $x+y-3=0$ et tel que $\vecteur{AM}\cdot \vectu=0$, c'est-à-dire
\[ \left \{\begin{array}{l}x+y-3=0\\(x-1)(-1)+(y-1)1=0 \end{array}\right. \Leftrightarrow \left \{ \begin{array}{l} x+y-3=0 \\ -x+y=0 \end{array} \right. \Leftrightarrow \left \{\begin{array}{l} x= \frac{3}{2} \\y=\frac{3}{2} \end{array}\right.\]
 Ainsi $B\left(\frac{3}{2};\frac{3}{2}\right)$ est le projeté orthogonal de $A$ sur $\mathcal{D}$.
}	
\end{prof}

\begin{eleve} 
\begin{solu_eleve}
\notes[1.8ex]{6}{\textwidth}
\end{solu_eleve}
\end{eleve}


	\subsection{Distance d'un point à une droite}

\definition{Soit $\mathcal{D}$ une droite du plan, et $A$ un point. On appelle \textbf{distance} de $A$ à la droite $\mathcal{D}$, et on note $d(A,\mathcal{D})$, la distance $AM_0$, où $M_0$ est le projeté orthogonal de $A$ sur la droite $\mathcal{D}$.
\begin{center}
	\input{tex/Chap4/pic/distance}	
\end{center}
}

\begin{theoreme}
Soit $A(x_A;y_A)$ un point du plan, et $\mathcal{D}$ une droite d'équation cartésienne $ax+by+c=0$. Alors
\[ d(A, \mathcal{D}) = \frac{|ax_A+by_A+c|}{\sqrt{a^2+b^2}} \]	
\end{theoreme}


\preuve{Notons $M_0(x_0;y_0)$ le projeté orthogonal de $A$ sur la droite $\mathcal{D}$. Par définition, $\vecteur{AM_0}$ et le vecteur normal $\vecteur{n}\matrice{a\\b}$ sont colinéaires, et donc
\[ |\vecteur{M_0A}\cdot \vecteur{n}| = M_0A ||\vecteur{n}|| \]
Ainsi, 
\[ d(A, \mathcal{D}) = M_0A = \frac{|\vecteur{M_0A}\cdot \vecteur{n}|}{||\vecteur{n}||} \]
Or, $\vecteur{M_0A}\cdot \vecteur{n} = (x_A-x_0)a+(y_A-y_0)b = ax_A+bx_B-(ax_0+bx_0)$. Or, puisque $M_0\in \mathcal{D}$, $ax_0+bx_0+c=0$. Ainsi
\[ \vecteur{M_0A}\cdot \vecteur{n} = ax_A+bx_B+c \]
et $||\vecteur{n}|| = \sqrt{a^2+b^2}$, ce qui donne le résultat.
}

\exemple{Déterminer la distance du point $A(1;2)$ à la droite $\mathcal{D}$ d'équation $2x+3y-7=0$.}

\begin{prof}
\solution{On a 
\[ d(A,\mathcal{D}) = \frac{|2\times 1+3\times 2-7|}{\sqrt{2^2+3^2}} = \frac{1}{\sqrt{13}} \]

}
\end{prof}

\begin{eleve} 
\begin{solu_eleve}
\notes[1.8ex]{3}{\textwidth}
\end{solu_eleve}
\end{eleve}



\section{Cercles}

	\subsection{Définition}

\definition{Soit $A$ un point du plan. Le \textbf{cercle} $\mathcal{C}_{A,r}$ de centre $A$ et de rayon $r\geq 0$ est l'ensemble des points du plan à une distance $r$ de $A$ :
\[ \mathcal{C}_{A,r} = \left \{ M ~ \text{du plan},~AM=r \right \} \]
}

\remarque{Soit $A(x_A;y_A)$ un point du plan. $M(x;y)$ est sur le cercle, de centre $A$ et de rayon $r$ si et seulement si $AM=r$, c'est-à-dire
\[ (x-x_A)^2+(y-y_A)^2=r^2 \]
Cette écriture est appelée \textbf{équation cartésienne} du cercle.}

\definition{[Equation cartésienne] Tout cercle du plan admet une équation du type $x^2+y^2+ax+by+c=0$. Réciproquement, tout ensemble admettant une équation du type $x^2+y^2+ax+by+c=0$ est un cercle.}

\exemple{Le cercle de centre $A(1;1)$ et de rayon $1$ a pour équation cartésienne $(x-1)^2+(y-1)^2=1^2$, c'est-à-dire $x^2+y^2-2x-2y-1=0$.}

	\subsection{Méthodes}

\begin{methode}
Connaissant le centre $A$ et le rayon $r$ d'un cercle, pour déterminer une équation cartésienne, on dit que $M(x;y)$ est sur le cercle si et seulement si $AM^2=r^2$. On en déduit alors une équation cartésienne.	
\end{methode}


\exemple{Soit $\mathcal{C}$ le cercle de centre $A(1;2)$ et de rayon 4. Déterminer une équation cartésienne de $\mathcal{C}$.}

\begin{prof}
\solution{$M(x;y) \in \mathcal{C}$ si et seulement si $AM^2=r^2$, et donc
\begin{eqnarray*}
	AM^2=r^2 &\Leftrightarrow& (x-1)^2+(y-2)^2 = 4^2 \\
	&\Leftrightarrow& x^2+y^2-2x-4y -11 = 0
\end{eqnarray*}
Une équation cartésienne de $\mathcal{C}$ est donc $x^2+y^2-2x-4y-11=0$.
}	
\end{prof}
 
\begin{eleve} 
\begin{solu_eleve}
\notes[1.8ex]{6}{\textwidth}
\end{solu_eleve}
\end{eleve}

\begin{methode}
Connaissant une équation cartésienne d'un cercle, on obtient le centre et le rayon en utilisant la forme canonique sur les termes de la forme $x^2+ax$ et $y^2+bx$. On en déduira alors centre et rayon.	
\end{methode}


\exemple{Soit $\mathcal{C}$ le cercle d'équation cartésienne $x^2+y^2+2x-6y+6=0$. Déterminer son centre et son rayon.}

\begin{prof}
\solution{On factorise :
\begin{eqnarray*}
	x^2+y^2+2x-6y+6=0 &\Leftrightarrow& x^2+2x  + y^2-6y + 6 = 0 \\
	&\Leftrightarrow& (x+1)^2-1 + (y-3)^2-9 + 6 = 0 \\
	&\Leftrightarrow& (x+1)^2+(y-3)^2 = 4 = 2^2
\end{eqnarray*}
Ainsi, $\mathcal{C}$ est le cercle de centre $A(-1;3)$ et de rayon $2$.
}	
\end{prof}

\begin{eleve} 
\begin{solu_eleve}
\notes[1.8ex]{6}{\textwidth}
\end{solu_eleve}
\end{eleve}

\begin{proposition}
Soient $A$ et $B$ deux points distincts. Alors l'ensemble des points $M$ vérifiant $\vecteur{MA}\cdot\vecteur{MB} = 0$ est le cercle de diamètre $[AB]$.	
\end{proposition}


\begin{methode}
Lorsqu'on connait les coordonnées des extrémités d'un diamètre, on utilise la proposition précédente pour déterminer une équation cartésienne.	
\end{methode}


\exemple{Soient $A(1;2)$ et $B(-2;-3)$ deux points du plan. Déterminer une équation cartésienne du cercle $\mathcal{C}$ de diamètre $[AB]$.}

\begin{prof}
\solution{$M(x;y) \in \mathcal{C}$ si et seulement si $\vecteur{MA}\cdot \vecteur{MB}=0$. Ainsi
\begin{eqnarray*}
	\vecteur{MA}\cdot \vecteur{MB} =0 &\Leftrightarrow& (x-1)(x+2) + (y-2)(y+3) + 0 \\
	 	&\Leftrightarrow& x^2+x-2+y^2+y-6 = 0 \\
	 	&\Leftrightarrow& x^2+y^2+x+y-8=0
\end{eqnarray*}
Ainsi, une équation cartésienne de $\mathcal{C}$ est $x^2+y^2+x+y-8=0$.
}	
\end{prof}

\begin{eleve} 
\begin{solu_eleve}
\notes[1.8ex]{6}{\textwidth}
\end{solu_eleve}
\end{eleve}

\section{Triangles et quadrilatères}

	\subsection{Triangle}
	
\rappel{Un triangle $ABC$ est dit 
\begin{itemize}[label=\textbullet]
	\item \textbf{rectangle} en $A$ si $(\vecteur{AB}; \vecteur{AC})=\frac{\pi}{2}~[\pi]$. Le côté $[BC]$ est alors appelé hypoténuse.
	\item \textbf{isocèle} en $A$ si $AB=AC$, ce qui équivaut à $\widehat{ABC}=\widehat{BCA}$.
	\item \textbf{équilatéral} si $AB=AC=BC$, ce qui équivaut à $\widehat{ABC}=\widehat{BCA}=\widehat{BAC}$.
	\item \textbf{plat} si $A,B$ et $C$ sont alignés.
\end{itemize}
}



\rappel{$ABC$ désigne un triangle. 
\begin{itemize}[label=\textbullet]
	\item La \textbf{hauteur} issue de $A$ est la droite passant par $A$ et perpendiculaire à $(BC)$. Le pied $H$ de la hauteur issue de $A$ est le point d'intersection de la hauteur issue de $A$ avec $(BC)$.
	\begin{center}
		\input{tex/Chap4/pic/hauteur}
	\end{center}
	\item La \textbf{médiane} issue de $A$ est la droite passant par $A$ et par le milieu $A'$ du segment $[BC]$.
	\begin{center}
		\input{tex/Chap4/pic/mediane}
	\end{center}
	\item La \textbf{médiatrice} du segment $[BC]$ est la droite passant par le milieu $A'$ de $[BC]$ et perpendiculaire à $(BC)$.
	\begin{center}
		\input{tex/Chap4/pic/mediatrice}
	\end{center}
	\item Une \textbf{bissectrice} de l'angle $\widehat{BAC}$ est une demi-droite passant par $A$ divisant l'angle $\widehat{BAC}$ en deux angles égaux. 
	\begin{center}
		\input{tex/Chap4/pic/bissectrice}
	\end{center}	
\end{itemize}
}

\rappel{Soit $ABC$ un triangle.
\begin{itemize}[label=\textbullet]
	\item Les trois hauteurs sont concourantes en un point $H$ appelé \textbf{orthocentre}.
	\item Les trois médianes sont concourantes en un point $G$ appelé \textbf{centre de gravité}.
	\item Les trois médiatrices sont concourantes en un point $D$ qui est le \textbf{centre du cercle circonscrit}.
	\item Les trois bissectrices intérieures sont concourantes en un point $I$ qui est le \textbf{centre du cercle inscrit}.
\end{itemize}}
\begin{center}
\begin{tabular}{ccc}
	\input{tex/Chap4/pic/orthocentre}& \input{tex/Chap4/pic/gravite}& \input{tex/Chap4/pic/circonscrit}\\
	Orthocentre & Centre de gravité & Centre du cercle circonscrit \\
	&\input{tex/Chap4/pic/inscrit}&\\
	& Centre du cercle inscrit &
\end{tabular}
\end{center}


\propriete{~\begin{itemize}[label=\textbullet]
	\item Un triangle $ABC$ est isocèle en $A$ si et seulement si la hauteur issue de $A$, la médiatrice de $[BC]$, la bissectrice de l'angle $\widehat{BAC}$ et la médiane issue de $A$ sont confondues.
	\item Un triangle $ABC$ est rectangle en $A$ si et seulement si $[BC]$ est un diamètre de son cercle circonscrit.
\end{itemize}
}

	\subsection{Quadrilatères}
	
\rappel{~\begin{itemize}[label=\textbullet]
	\item Un \textbf{trapèze} est un quadrilatère ayant deux côtés opposés parallèles.
	\item Un \textbf{parallélogramme} est un quadrilatère $ABCD$ vérifiant $\vecteur{AB}=\vecteur{DC}$, ou encore que ses diagonales se coupent en leur milieux.
	\item Un \textbf{losange} est un quadrilatère ayant tous ses côtés égaux, ou encore que ses diagonales se coupent perpendiculairement en leurs milieux.
	\item Un \textbf{rectangle} est un parallélogramme ayant un angle droit, ou encore que ses diagonales sont de même longueur et se coupent en leurs milieux.
	\item Un \textbf{carré} est un rectangle losange.  
\end{itemize}
}