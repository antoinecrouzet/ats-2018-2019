// Méthode des rectangles
// Auteur : Crouzet
// Date : 5 mai 2018

// On définit la fonction 
function y=f(x)
  y=sqrt(1+x^2)
endfunction

// On détermine l'aire des rectangles inférieurs et supérieurs
// Arguments : 
//   a : borne inférieure du segment
//   b : borne supérieure du segment
//   n : nombre de subdivision
function [inf, sup]=rectangle(a,b,n)
  inf=0.0
  sup=0.0
  for i=0:n-1
	inf=inf+(b-a)/n*f(a+i*(b-a)/n)
        sup=sup+(b-a)/n*f(a+(i+1)*(b-a)/n)
  end;
endfunction

// Exemple : pour a=1, b=2, n=100
[inf,sup]=rectangle(1,2,100)
// Comparaison avec une valeur calculée par SciLab:
integrate('f(x)','x',1,2)
