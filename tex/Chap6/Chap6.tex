\objectif{L'objectif de ce chapitre est d'introduire rigoureusement la notion de système linéaire, déjà vue lors des années antérieures. On y voit, entre autre, la méthode de résolution du pivot de Gauss-Jordan.}

\large \textbf{Objectifs} \normalsize

\textit{La liste ci-dessous représente les éléments à maitriser absolument. Pour cela, il faut savoir refaire les exemples et exercices du cours, ainsi que ceux de la feuille de TD.}

\begin{numerote}
				\item Savoir résoudre un système simple par substitution\dotfill $\Box$
				\item Savoir appliquer la méthode du pivot de Gauss-Jordan pour transformer un système en un système triangulaire\dotfill $\Box$ 
				\item Résoudre un système ayant une infinité de solutions avec un (ou plusieurs) paramètres\dotfill $\Box$
				\item Savoir déterminer le rang d'un système\dotfill $\Box$
				\item Savoir résoudre un système ayant un paramètre\dotfill $\Box$
\end{numerote}

\newpage


\section{Définitions et propriétés}

	    \subsection{Définitions}
	   
\definition{Soient $n$ et $p$ deux nombres entiers non nuls. On appelle \textbf{système d'équations linéaires} de $n$ équations à $p$ inconnus (ou système $n$ fois $p$, $n\times p$) un système de la forme

\[(S) \left \{
\begin{array}{cccccccccr}
 a_{11}x_1 &+& a_{12}x_2&+&\cdots&+&a_{1p}x_p &=& b_1&(L_1)\\
 a_{21}x_1 &+& a_{22}x_2&+&\cdots&+&a_{2p}x_p &=& b_2&(L_2)\\
&&&&\vdots&&&&& \\
 a_{n1}x_1 &+& a_{n2}x_2&+&\cdots&+&a_{np}x_p &=& b_n&(L_n)\\
\end{array}
\right.
\]

où les $(a_{ij})_{\substack{1\leq i \leq n\\1\leq j \leq p}}$ et les $(b_i)_{1\leq i \leq n}$ sont des nombres réels, et $x_1, x_2,\cdots, x_n$ sont des inconnues.

Le nombres $a_{ij}$ est le \textbf{coefficient} de la $j^{\textrm{ème}}$ inconnue $x_j$ dans la $i^{\textrm{ème}}$ équations $(L_i)$.}

\remarque{Si $n=p$ on dit que le système $(S)$ est \textbf{carré d'ordre} $n$.}

\exemple{Le système 
\[(S_1) \left \{ \begin{array}{cccccr}
     2x_1 &-& 3x_2 & = & 1 & (L_1) \\
     3x_1 & + & x_2 &=& -2 & (L_2)
 \end{array}
\right.\] est un système linéaire de $2$ équations à $2$ inconnues. C'est ainsi un système carré d'ordre $2$.

Le système 
\[(S_2) \left \{ \begin{array}{cccccccr}
     x_1 &-& 2x_2 & + & 2x_3 & = & 4 & (L_1) \\
     2x_1 & + & 4x_2 & - & 2x_3 &=& 3 & (L_2)
 \end{array}
\right.\] est un système linéaire de $2$ équations à $3$ inconnues. }

    \subsection{Propriétés}

~\\
\definition{Soit \[(S) \left \{
\begin{array}{cccccccccr}
 a_{11}x_1 &+& a_{12}x_2&+&\cdots&+&a_{1p}x_p &=& b_1&(L_1)\\
 a_{21}x_1 &+& a_{22}x_2&+&\cdots&+&a_{2p}x_p &=& b_2&(L_2)\\
&&&&\vdots&&&&& \\
 a_{n1}x_1 &+& a_{n2}x_2&+&\cdots&+&a_{np}x_p &=& b_n&(L_n)\\
\end{array}
\right.
\]

\begin{itemize}[label=\textbullet]
    \item \textbf{Résoudre} le système $(S)$, c'est trouver toutes les $p$-listes $(x_1,\cdots,x_p)$ de réels vérifiant les $n$ équations $(L_1,\cdots, L_n)$.
    \item On dit qu'un système est \textbf{incompatible} s'il n'admet pas de solution.
    \item Deux systèmes $(S)$ et $(S')$ sont dits \textbf{équivalents} s'ils ont les mêmes solutions.
    \item Le système $(S)$ est dit \textbf{homogène} (ou \textbf{sans second membre}) si $b_1=\cdots=b_n=0$. Dans ce cas, la $p$-liste $(0,\cdots,0)$ est toujours solution de $(S)$.
    \item On appelle \textbf{système homogène associé} à $(S)$ le système obtenu à partir de $(S)$ en remplaçant tous les nombres $b_i$ par $0$.
\end{itemize}
}

\remarque{Dans le cas où $p<n$, il y a plus d'équations que d'inconnues. Soit certaines équations sont redondantes (et on peut donc les supprimer), soit le système est incompatible.\\
Dans la suite, on ne s'intéressera qu'au cas $n\leq p$.}

    \subsection{Résolution par substitution}
     
\begin{methode}
La méthode par résolution consiste à écrire une des inconnues (par exemple $x_1$) en fonction des autres ($x_2, x_3,\cdots$), puis à remplacer cette inconnue $x_1$ dans toutes les autres équations en fonction de $x_2, x_3,\cdots$. Cette méthode est efficace lorsqu'il y a peu d'inconnues ou d'équations.	
\end{methode}


\exemple{Résoudre le système suivant :

\[(S_1) \left \{ \begin{array}{cccccr}
     2x_1 &-& 3x_2 & = & 1 & (L_1) \\
     -3x_1 & + & x_2 &=& 2 & (L_2)
 \end{array}
\right.\]}

\begin{prof}
\solution{En utilisant $(L_2)$, on peut exprimer $x_2$ en fonction de $x_1$ : $x_2=3x_1+2$. On remplace alors cette égalité dans $(L_1)$ pour en déduire la valeur de $x_1$. On obtiendra enfin la valeur de $x_2$ :

\[(S_1) \Leftrightarrow \left \{ \begin{array}{cccccr}
     2x_1 &-& 3(3x_1+2) & = & 1 & (L_1) \\
     &  & x_2 &=& 3x_1+2 & (L_2)
 \end{array}
\right.\]
\[(S_1) \Leftrightarrow
\left \{ \begin{array}{cccccr}
     -7x_1 &&  & = & 7 & (L_1) \\
     &  & x_2 &=& 3x_1+2 & (L_2)
 \end{array}
\right.\]
\[(S_1) \Leftrightarrow
\left \{ \begin{array}{cccccr}
     x_1 &&  & = &-1 & (L_1) \\
     &  & x_2 &=& -1 & (L_2)
 \end{array}
\right.
\]
Ainsi, le système admet une unique solution : $\{(-1;-1)\}$.}
\end{prof}

\begin{eleve} 
\begin{solu_eleve}
\notes[1.8ex]{6}{\textwidth}
\end{solu_eleve}
\end{eleve}

    
    \subsection{Systèmes triangulaires}
    
 Les système triangulaires sont les plus simples  des systèmes, puisqu'ils se résolvent très facilement.
 
 \definition{On dit qu'un système $(S)$ $n\times p$ est \textbf{triangulaire} si 
 \[\forall~i\in \{1;\cdots;n\}, \forall~j \in \{1;\cdots;p\}~~i>j \Rightarrow a_{i,j}=0\]
 Ainsi, si $n<p$, le système est de la forme 
 \[(S) \left \{
 \begin{array}{cccccccccccccr}
  a_{11}x_1 &+& a_{12}x_2 &+& a_{13}x_3&+& \cdots & + &\cdots &+& a_{1p}x_p &=& b_1 & (L_1)\\
  && a_{22}x_2 &+& a_{23}x_3 &+& \cdots &+ & \cdots & +&a_{2p}x_p &=& b_2& (L_2) \\
  &&&&&&&\vdots&&&&&\\
  &&&&&&a_{nn}x_n&+&\cdots&+& a_{np}x_p &=& b_n & (L_n)
 \end{array}
 \right.\]
 Si $n=p$, on a alors le système suivant :
 \[(S) \left \{
 \begin{array}{cccccccccccccr}
  a_{11}x_1 &+& a_{12}x_2 &+& a_{13}x_3&+& \cdots & + &\cdots &+& a_{1n}x_n &=& b_1 & (L_1)\\
  && a_{22}x_2 &+& a_{23}x_3 &+& \cdots &+ & \cdots & +&a_{2n}x_n &=& b_n& (L_2) \\
  &&&&&&&&&&\vdots&&\\
  &&&&&&&&&& a_{nn}x_n &=& b_n & (L_n)
 \end{array}
 \right.\]
Les coefficients diagonaux $a_{11},\cdots,a_{nn}$ sont appelés les \textbf{pivots} du système.
 }

\remarque{Lorsque $n=p$ et que tous les pivots $a_{ii}$ (pour $i\in \{1;\cdots;n\}$) sont non nuls, le système se résout par substitutions successives, de $(L_n)$ à $(L_1)$. Il y a alors une \textbf{unique $n$-liste solution}.}

\begin{methode}
Dans le cas $n<p$, il y a une (ou plusieurs) inconnue(s) en trop. On choisit alors ces inconnues comme inconnues auxiliaires, et on résout comme pour la cas $n=p$. 	
\end{methode}


\exemple{Résoudre le système suivant :
\[(S) \left \{
\begin{array}{ccccccc}
2x&-&y&+&3z&=&-1 \\
&&2y &-&4z&=&2
\end{array}
\right.\]}

\begin{prof}
\solution{Il y a $3$ inconnues, pour deux équations. Exprimons $x$ et $y$ en fonction de $z$ : 
\[(S) \Leftrightarrow \left \{
\begin{array}{ccccccc}
2x&-&y&=&-1&-&3z \\
&&2y &=&2&+&4z
\end{array}
\right. \Leftrightarrow
\left \{
\begin{array}{ccccc}
x&=&&-&\frac{1}{2}z \\
y &=&1&+&2z
\end{array}
\right.
\]
L'ensemble des solutions est donc $\displaystyle{\mathcal{S}=\left\{\left(-\frac{1}{2}z;1+2z;z\right), ~z\in \R\right \}}$.
}
\end{prof}

\begin{eleve} 
\begin{solu_eleve}
\notes[1.8ex]{6}{\textwidth}
\end{solu_eleve}
\end{eleve}

\remarque{Bien évidemment, si on choisit une autre inconnue auxiliaire, le résultat ne sera pas sous la même forme, mais désignera bien le même ensemble de solutions.}

\section{Pivot de Gauss-Jordan}

    \subsection{Exemple}
    
On souhaite résoudre le système suivant 
\[(S) \left \{
 \begin{array}{cccccccr}
     x & + & 2y &+& 2z &=& 1 & L_1\\
     2x & - & 2y & + & 2z & = & 2 & L_2\\
     -x & + & y & + &3z & = & 1 & L_3
 \end{array}
\right.\] 

Pour faire cela, on va utiliser différentes opérations dites élémentaires, qui transforment le système $(S)$ en un système équivalent, mais triangulaire cette fois-ci. Il ne restera alors plus qu'à résoudre le système triangulaire associé.

Ici :
\[(S) \Leftrightarrow \left \{
 \begin{array}{cccccccc}
     x & + & 2y &+& 2z &=& 1 & L_1\textrm{ ligne Pivot }\\
      & - & 6y & - & 2z & = & 0 & L_2 \leftarrow L_2-2L_1\\
      &  & 3y & + &5z & = & 2 & L_3 \leftarrow L_3+L_1
 \end{array}
\right.\]

\[(S) \Leftrightarrow \left \{
 \begin{array}{cccccccc}
     x & + & 2y &+& 2z &=& 1 & L_1\\
      & - & 6y & - & 2z & = & 0 & L_2 \textrm{ ligne pivot}\\
      &  &  &  &8z & = & 4 & L_3 \leftarrow 2L_3+L_2
 \end{array}
\right.\]

On obtient ainsi un système triangulaire, qu'on résout :

\[(S) \Leftrightarrow \left \{
 \begin{array}{ccc}
    x &=& \frac{1}{3}\\
    y &=& -\frac{1}{6}\\
    z &=& \frac{1}{2}
 \end{array}
\right.\]

Ainsi, la solution de $(S)$ est $\mathcal{S}= \left \{ \left( \frac{1}{3}; -\frac{1}{6}; \frac{1}{2} \right) \right \}$.

    \subsection{Opérations élémentaires}
    
  \definition{Soit $(S)$ un système $n\times p$. On appelle \textbf{opération élémentaire} l'une des trois opérations suivantes :
  \begin{itemize}[label=\textbullet]
      \item $L_i \leftrightarrow L_j$ : \textbf{échange} de la $i^\textrm{ième}$ ligne $L_i$ et  de la $j^\textrm{ième}$ ligne $L_j$.
      \item $L_i \leftarrow aL_i$ où $a\neq 0$ : on \textbf{remplace} la $i^\textrm{ième}$ ligne par elle même \textbf{multipliée} par un nombre non nul $a$.
      \\\textit{Utilité} : lorsqu'on  a des fractions dans la ligne $L_i$, cela permet d'enlever les dénominateurs.
      \item $L_i \leftarrow L_i+bL_j$ où $b$ est quelconque : on \textbf{remplace} la $i^\textrm{ième}$ ligne par la \textbf{somme} d'elle même et d'un multiple d'une autre ligne.\\
      \textit{Utilité} : permet d'éliminer une inconnue.
  \end{itemize}
}

\remarque{En combinant la deuxième et la troisième opérations élémentaires, on obtient l'opération \\$L_i \leftarrow aL_i+bL_j$ où $a$ est non nul, et $b$ est quelconque.}

\begin{theoreme}
Tout système obtenu à partir d'un système $(S)$ en transformant l'une de ses équations par une opération élémentaire est équivalent à $(S)$, et a donc le même ensemble de solutions.	
\end{theoreme}


\remarque{Ainsi, en combinant différentes opérations élémentaires, on ne change pas l'ensemble de solutions du système.}

    \subsection{Pivot de Gauss-Jordan}
    
\begin{methode}
En utilisant les opérations élémentaires comme dans l'exemple, on va résoudre un système $(S)$ par la méthode du pivot de Gauss :
\begin{enumerate}
    \item On élimine successivement des inconnues via les opérations élémentaires, pour transformer le système initial en un système triangulaire;
    \item On résout le système triangulaire par substitutions.
\end{enumerate}	
\end{methode}


\exemple{~\begin{itemize}[label=\textbullet]
    \item (Une unique solution) 
    \[(S) \left \{
 \begin{array}{cccccccr}
     x & + & 2y &+& 3z &=& 2 & (L_1)\\
     3x & + & y & + & 2z & = & 1 & (L_2)\\
     2x & + & 3y & + &z & = & 0 & (L_3)
 \end{array}
\right.\] 
    \item (Une infinité de solution)
        \[(S) \left \{
 \begin{array}{cccccccccr}
    & - & y & + & 2z & + & 3t & = & 0 & (L_1) \\
    2x & + & 2y & - & z & && = & 0 & (L_2) \\
    3x & - & y & + & 2z & - & 2t & = & 0 & (L_3) \\
    5x & + & y & + & z & - & 2t & = & 0 & (L_4)
 \end{array}
\right.\]
    \item (Aucune solution)
        \[(S) \left \{
 \begin{array}{cccccccccr}
    & - & y & + & 2z & + & 3t & = & 0 & (L_1) \\
    2x & + & 2y & - & z & && = & 0 & (L_2) \\
    3x & - & y & + & 2z & - & 2t & = & 0 & (L_3) \\
    5x & + & y & + & z & - & 2t & = & 1 & (L_4)
 \end{array}
\right.\]
\end{itemize}
}

\begin{prof}
\solution{On utilise la méthode du Pivot de Gauss, en n'oubliant pas d'indiquer les opérations effectuées.
	\begin{itemize}[label=\textbullet]
		\item 
		\[(S) \Leftrightarrow \left \{
 		\begin{array}{cccccccr}
 		    x & + & 2y &+& 3z &=& 2 & \textrm{ligne pivot}\\
    		 & - & 5y & - & 7z & = & -5 & L_2\from L_2-3L_1\\
   		   & -  & y & - &5z & = & -4 & L_3 \from L_3-2L_1
 		\end{array}
\right.  \Leftrightarrow 
	\left \{
 		\begin{array}{cccccccr}
 		    x & + & 2y &+& 3z &=& 2 & \\
    		 & - & 5y & - & 7z & = & -5 & \textrm{ligne pivot}\\
   		   &   &  & - &18z & = & -15 & L_3 \from 5L_3-L_2
 		\end{array}
	\right.\] 
	Le système est triangulaire avec tous ses pivots non nuls : on remonte celui-ci pour trouver une unique solution.
	\[(S) \Leftrightarrow 
	\left \{ \begin{array}{ccc}
 			x&=&-\frac{1}{6}\\
 			y&=&-\frac{1}{6}\\
 			z&=&\frac{5}{6}
 		\end{array}\right.\]
	Ainsi, \[\boxed{\mathcal{S}=\left \{ \left(-\frac{1}{6}  ;-\frac{1}{6}  ;\frac{5}{6}  \right) \right\}}\]
	\item On n'hésite pas à échanger des lignes pour avoir une ligne pivot pratique.
 \[(S) \left \{
 \begin{array}{cccccccccr}
    & - & y & + & 2z & + & 3t & = & 0 & (L_1) \\
    2x & + & 2y & - & z & && = & 0 & (L_2) \\
    3x & - & y & + & 2z & - & 2t & = & 0 & (L_3) \\
    5x & + & y & + & z & - & 2t & = & 0 & (L_4)
 \end{array}
\right. \Leftrightarrow 
\left \{
 \begin{array}{cccccccccr}
    2x & + & 2y & - & z & && = & 0 & L_1 \leftrightarrow L2 \\
    & - & y & + & 2z & + & 3t & = & 0 &  \\
    3x & - & y & + & 2z & - & 2t & = & 0 & \\
    5x & + & y & + & z & - & 2t & = & 0 & 
 \end{array}
\right.\]
Ainsi,
\[ (S) \Leftrightarrow
\left \{
 \begin{array}{cccccccccr}
    2x & + & 2y & - & z & && = & 0 & \textrm{ligne pivot} \\
    & - & y & + & 2z & + & 3t & = & 0 &  L_2\\
     & - & 8y & + & 7z & - & 4t & = & 0 & L_3\from 2L_3-3L_1\\
     & - & 8y & + & 7z & - & 4t & = & 0 & L_4\from 2L_4-5L_1
 \end{array}
\right.\]
	Les deux dernières lignes sont les mêmes. On en élimine une, et on continue la méthode du Pivot de Gauss :
\[ (S) \Leftrightarrow
\left \{
 \begin{array}{cccccccccr}
    2x & + & 2y & - & z & && = & 0 & \\
    & - & y & + & 2z & + & 3t & = & 0 &  \textrm{ligne pivot} \\
     &  &  & - & 9z & - & 28t & = & 0 & L_3\from L_3-8L_2\\
 \end{array}
\right.\]
 On obtient un système triangulaire, qu'on résout en utilisant une variable auxiliaire (par exemple ici, $t$) :
 	\[(S) \Leftrightarrow 
	\left \{ \begin{array}{ccc}
 			x&=&\frac{15}{9}t\\
 			y&=&-\frac{29}{9}t\\
 			z&=&-\frac{28}{9}t
 		\end{array}\right.\]
	Ainsi, \[\boxed{\mathcal{S}=\left \{ \left(\frac{5}{3}t  ;-\frac{29}{9}t  ;-\frac{28}{9}t; t  \right),~t \in \R \right\}}\]
	\item Le système est le même que précédemment, excepté la dernière ligne. Par les deux mêmes opérations, on obtient :
	\[ (S) \Leftrightarrow
	\left \{
 \begin{array}{ccccccccc}
    2x & + & 2y & - & z & && = & 0  \\
    & - & y & + & 2z & + & 3t & = & 0   \\
    3x & - & y & + & 2z & - & 2t & = & 0  \\
    5x & + & y & + & z & - & 2t & = & 1 
 \end{array}
\right.
\Leftrightarrow
\left \{
 \begin{array}{cccccccccr}
    2x & + & 2y & - & z & && = & 0 & \textrm{ligne pivot} \\
    & - & y & + & 2z & + & 3t & = & 0 &  L_2\\
     & - & 8y & + & 7z & - & 4t & = & 0 & L_3\from 2L_3-3L_1\\
     & - & 8y & + & 7z & - & 4t & = & 2 & L_4\from 2L_4-5L_1
 \end{array}
\right.\]
Les deux dernières lignes étant incompatibles, le système est incompatible. Ainsi, \[\boxed{\mathcal{S}=\emptyset }\]
	\end{itemize}	
}
\end{prof}

%%%%%%% A FAIRE %%%%%%%%

\begin{eleve} 
\begin{solu_eleve}
\notes[1.8ex]{42}{\textwidth}
\end{solu_eleve}
\end{eleve}


\remarque{Un système linéaire admet :
\begin{itemize}[label=\textbullet]
    \item soit aucune solution (il est donc incompatible);
    \item soit une unique solution;
    \item soit une infinité de solutions (quand il y a une (ou des) inconnues auxiliaires)
\end{itemize}
}

\definition{Après avoir appliqué la méthode du pivot de Gauss, on obtient un système triangulaire. On appelle \textbf{rang} d'un système le nombre de pivot non nuls.}

\exemple{Dans les exemples précédents, le premier était de rang $3$, et le deuxième est également de rang $3$.}

\section{Systèmes de Cramer}

    \subsection{Définition}

\definition{Un système carré d'ordre $n$ est dit \textbf{de Cramer} s'il possède une unique $n$-liste solution.}

\consequence{Un système homogène $(S)$ de $n$ équations linéaires à $n$ inconnues est un système de Cramer si son unique solution est la $n$-liste $(0,0,\cdots, 0)$.}

    \subsection{Systèmes de Cramer et pivot de Gauss}
    
\begin{theoreme}
Un système $(S)$ carré d'ordre $n$ est de Cramer si et seulement si la méthode du pivot de Gauss fait apparaitre $n$ pivots successifs non-nuls, c'est-à-dire si et seulement si $(S)$ est de rang $n$.	
\end{theoreme}


\exemple{Montrer que le système suivant est de Cramer 
\[(S) \left \{\begin{array}{cccccccl}
     x & + & y & - & 2z &=& 0&\\
     2x & - & y & + & z & = & 0&\\
     2x & + & y & - & 2z & = & 0&
 \end{array}
 \right.\] 
}
 
\begin{prof}
\solution{En appliquant la méthode du pivot de Gauss :
\begin{align*}
	(S) &\Leftrightarrow
 \left \{\begin{array}{cccccccl}
     x & + & y & - & 2z &=& 0&\\
      &  -& 3y & + & 5z & = & 0 &L_2 \leftarrow L_2-2L_1 \\
      & - & y & + & 2z & = & 0 &L_3 \leftarrow L_3-2L_1
 \end{array} 
 \right. \\
& \Leftrightarrow
\left \{\begin{array}{cccccccl}
     x & + & y & - & 2z &=& 0&\\
     & - & y & + & 2z & = & 0 &L_2 \leftrightarrow L_3 \\
       &  -& 3y & + & 5z & = & 0 & 
 \end{array}
 \right.\\
 & \Leftrightarrow
 \left \{\begin{array}{cccccccl}
     x & + & y & - & 2z &=& 0&\\
     & - & y & + & 2z & = & 0&\\
       &  &  & - & z & = & 0  &L_3 \leftarrow L_3-3L_2
 \end{array}
 \right.
\end{align*}
On a ainsi fait apparaitre les pivots $1, -1$ et $-1$ qui sont tous les trois non nuls : le système $(S)$ est bien de Cramer.
 }
\end{prof}


\begin{eleve} 
\begin{solu_eleve}
\notes[1.8ex]{8}{\textwidth}
\end{solu_eleve}
\end{eleve}

    \subsection{Système de Cramer et système homogène associé}
    
  \begin{theoreme}
  Un système $(S)$ est de Cramer si et seulement si son système homogène associé est aussi de Cramer.	
  \end{theoreme}

  
 \preuve{En effet, le choix des pivots dans la méthode des pivots de Gauss ne dépend pas du second membre.}

\begin{methode}
Pour montrer qu'un système quelconque est de Cramer, il suffit donc de montrer que son système homogène associé l'est, ce qui est plus simple.	
\end{methode}

