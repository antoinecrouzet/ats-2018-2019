\objectif{Dans ce chapitre, on introduit la notion de série, qui est à rapprocher de l'intégrale généralisée, mais dans le cas d'une suite. On y verra des méthodes de calculs, mais aussi des théorèmes pour montrer des convergences ou divergences de séries, sans pouvoir nécessairement calculer la somme de la série.}

\textit{La liste ci-dessous représente les éléments à maitriser absolument. Pour cela, il faut savoir refaire les exemples et exercices du cours, ainsi que ceux de la feuille de TD.}

\begin{numerote}
	\item Connaître la notion de série :
			\begin{itemize}[label=\textbullet]
				\item connaître la définition d'une série, d'une somme et du reste d'une série\dotfill $\Box$
				\item connaître les différentes opérations usuelles\dotfill $\Box$
				\item connaître le lien entre suite et série\dotfill $\Box$
				\item connaître la condition nécessaire de convergence\dotfill $\Box$
				\item connaître les séries de référence\dotfill $\Box$
			\end{itemize}
	\item Concernant les théorèmes de convergence :
			\begin{itemize}[label=\textbullet]
				\item connaître le théorème de comparaison\dotfill $\Box$
				\item connaître le théorème d'équivalence\dotfill $\Box$
			\end{itemize}
	\item Concernant l'absolue convergence :
			\begin{itemize}[label=\textbullet]
				\item connaître la définition\dotfill $\Box$
				\item connaître le lien entre absolue convergence et convergence\dotfill $\Box$
				\item connaître le cas particulier des séries alternées\dotfill $\Box$
			\end{itemize}
\end{numerote}

\newpage 

\section{Définitions}

Dans l'ensemble de cette section, $\K$ désigne $\R$ ou $\C$.

    \subsection{Séries}

\definition{Soit $(u_n)$ une suite à valeur dans $\K$. On appelle \textbf{série de terme général} $u_n$, et on note $\ds{\sum_{n\geq 0} u_n}$ ou plus simplement $\sum u_n$, la suite des sommes partielles $(S_n)$ définie par $$S_n=\sum_{k=0}^n u_k$$ }

\remarque{Si la suite $(u_n)$ n'est définie qu'à partir d'un certain rang $n_0$, la série de terme général $u_n$ n'est également définie qu'à partir de $n_0$, ce que l'on note $\ds{\sum_{n\geq n_0} u_n}$. La suite des sommes partielles est $(S_n)_{n\geq n_0}$, avec $\ds{S_n=\sum_{k={n_0}}^n u_k}$.}

\exemple{Soit $u$ la suite définie pour tout $n\geq 1$ par $u_n=\frac{1}{n}$. La série de terme général $u_n$ est notée $\ds{\sum_{n\geq 1} \frac{1}{n}}$ est appelée la \textbf{série harmonique}.\\
Les premières sommes partielles sont $1, 1+\frac{1}{2}, 1+\frac{1}{2}+\frac{1}{3}\cdots$.}
      
    \subsection{Convergence}

La série $\sum u_n$ étant une suite, on peut s'intéresser à sa convergence.

\definition{Soit $(u_n)$ une suite à valeurs dans $\K$. On dit que la série $\sum u_n$ \textbf{converge} si la suite des sommes partielles $(S_n)$ converge. Dans ce cas :
\begin{itemize}[label=\textbullet]
	\item la limite de la suite $(S_n)$ est alors appelée \textbf{somme} de la série, et est notée $\ds{\sum_{k=0}^{+\infty} u_k}$. On a ainsi
$$\sum_{k=0}^{+\infty} u_k = \lim_{n\rightarrow +\infty} \sum_{k=0}^n u_k$$
	\item on appelle \textbf{reste} de la série la suite $(R_n)$ définie par \[R_n=\sum_{k=n+1}^{+\infty} u_k = \sum_{k=0}^{+\infty} u_k-S_{n} \]
\end{itemize} 
}

\remarque{\danger{~L'écriture $\ds{\sum_{k=0}^{+\infty} u_k}$ n'a de sens que si la série converge, alors que l'écriture $\sum u_n$ a bien un sens, puisqu'elle désigne une suite.}}

\remarque{Les sommes infinies ne se manipulent pas comme les sommes finies (puisqu'en réalité, ce sont des limites, et il faut donc toujours s'assurer de la convergence). C'est pourquoi on calculera (presque) toujours les sommes partielles, qui sont des sommes finies, avant de passer à la limite.}

    \subsection{Premiers exemples}

\exemple{Soit $(u_n)$ la suite définie pour tout $n$ par $$u_n=\left(\frac{1}{2}\right)^n$$ Etudier la série $\ds{\sum u_n}$.}

\begin{prof}
\solution{
Notons $\ds{S_n=\sum_{k=0}^n \left(\frac{1}{2}\right)^k}$. Alors 

$$\forall~n,~S_n=\sum_{k=0}^n \left(\frac{1}{2}\right)^k=\frac{1-\left(\frac{1}{2}\right)^{n+1}}{1-\frac{1}{2}} = 2\left(1-\left(\frac{1}{2}\right)^{n+1}\right)$$


Puisque $-1< \frac{1}{2} < 1$, on a $\ds{\lim_{n\rightarrow +\infty} \left(\frac{1}{2}\right)^{n+1}=0}$. Par somme et produit, on en déduit donc que la série $\ds{\sum_{n\geq 0} \left(\frac{1}{2}\right)^n}$ converge, et on a $$\lim_{n\rightarrow +\infty} S_n=\sum_{k=0}^{+\infty} \left(\frac{1}{2}\right)^k=2$$}
\end{prof}

\lignes{8}
    

\exemple{Montrer que la série harmonique, de terme général $\frac{1}{n}$, $\ds{\sum_{n\geq 1} \frac{1}{n}}$, est divergente.}

\begin{prof} 
\solution{Pour tout $n\geq 1$, notons $H_n=\ds{\sum_{k=1}^n \frac{1}{n}}$. 
\\Nous avons vu dans le chapitre sur le calcul différentiel que l'on a, pour tout $k\geq 1$, $$\ln(k+1)-\ln(k)\leq \frac{1}{k}$$
En additionnant ces inégalités, on obtient alors
$$\sum_{k=1}^n (\ln(k+1)-\ln(k)) \leq \sum_{k=1}^n \frac{1}{k}=H_n$$
Or, on a
$$\sum_{k=1}^n (\ln(k+1)-\ln(k)) =  \ln(n+1)-\ln(1)=\ln(n+1) \textrm{, les termes se téléscopant.}$$
Puisque $\ds{\lim_{n\rightarrow +\infty} \ln(n+1)=+\infty}$, par comparaison, on en déduit que
$$\lim_{n\rightarrow +\infty} \sum_{k=1}^n \frac{1}{k} = +\infty$$
}	
\end{prof}

\lignes{10}
    
\section{Propriétés}

    \subsection{Opérations sur les séries}

Les opérations sur les sommes finies se transposent, dans certains cas, aux séries :
\begin{theoreme}[Linéarité] 
Soient $(u_n)$ et $(v_n)$ deux suites à valeurs dans $\K$, et $\lambda$ un réel non nul.
\begin{itemize}
    \item[$\bullet$] Les séries $\sum u_n$ et $\sum \lambda u_n$ sont de même nature (c'est-à-dire qu'elles sont soit toutes les deux convergentes, soit toutes les deux divergentes). Si elles sont convergentes, on a alors
    $$\sum_{k=0}^{+\infty} \lambda u_k= \lambda \sum_{k=0}^{+\infty} u_k$$
    \item[$\bullet$] Si les séries $\sum u_n$ et $\sum v_n$ sont toutes les deux convergentes, alors la série $\sum (u_n+v_n)$ est également convergente, et on a
    $$\sum_{k=0}^{+\infty} (u_k+v_k)=\sum_{k=0}^{+\infty} u_k+\sum_{k=0}^{+\infty} v_k$$
\end{itemize}	
\end{theoreme}

\remarque{La réciproque du deuxième point n'est pas vraie. Par exemple, si pour tout $n\geq 1$, $u_n=\frac{1}{n}$ et $v_n=-\frac{1}{n}$, alors la série $\sum u_n+v_n$ converge (vers $0$) alors que ni $\sum u_n$ ni $\sum v_n$ ne convergent.}

    \subsection{Suite et série}


\begin{theoreme}
Soit $(u_n)$ une suite. Alors $(u_n)$ converge si, et seulement si, la série $\sum (u_{n+1}-u_n)$ converge. Dans ce cas, en notant $\ell$ la limite de $(u_n)$, on a $$\sum_{k=0}^{+\infty} (u_{k+1}-u_k)=\ell-u_0$$	
\end{theoreme}


\begin{prof}
\preuve{Notons $\ds{S_n=\sum_{k=0}^n (u_{k+1}-u_k)}$. On constate que $S_n=u_{n+1}-u_0$  par télescopage. Ainsi, $(S_n)$ converge si et seulement si $(u_{n})$ converge. Si $(u_n)$ converge vers $\ell$, par passage à la limite, on obtient bien $\ds{\sum_{k=0}^{+\infty} (u_{k+1}-u_k)=\ell-u_0}$.}
\end{prof}

\lignes{2}
    
    \subsection{Limite de la suite et convergence}
    
\begin{theoreme}
Soit $(u_n)$ une suite à valeurs dans $\K$. Si la série $\sum u_n$ est convergente, alors $\ds{\lim_{n\rightarrow +\infty} u_n=0}$.	
\end{theoreme}


\begin{prof}
\preuve{Pour tout $n$, notons $\ds{S_n=\sum_{k=0}^n u_k}$. Alors, pour tout $n\geq 1$, on a $u_n=S_n-S_{n-1}$. Si la série $\sum u_n$ converge, alors la suite $(S_n)$ admet, par définition, une limite que l'on note $S$. Mais alors
$$\lim_{n\rightarrow +\infty} S_n=\lim_{n\rightarrow +\infty} S_{n-1}=S$$
et donc
$$\lim_{n\rightarrow +\infty} u_n = S-S = 0$$}    
\end{prof}

\lignes{5}

\remarque{La contraposée du théorème est intéressante : si la suite $(u_n)$ ne converge pas vers $0$, alors la série $\sum u_n$ n'est pas convergente. Par exemple, la série $\ds{\sum \frac{2n}{n+1}}$ diverge, car son terme général ne tend pas vers $0$.}


\remarque{Cette condition est nécessaire, mais pas suffisante : en effet, on a $\ds{\lim_{n\rightarrow +\infty} \frac{1}{n}=0}$ et pourtant la série harmonique $\ds{\sum_{n\geq 1} \frac{1}{n}}$ diverge.}   
    
\exemple{Soit $q$ un réel. On s'intéresse à la série $\sum q^n$. Alors, pour que la série $\sum q^n$ converge, il faut que $\ds{\lim_{n\rightarrow +\infty} q^n =0}$, c'est à dire $|q|<1$. On verra plus tard que la réciproque, dans ce cas, est vraie.}

    \subsection{Séries réelle à termes positifs}

Le cas des séries à coefficients réels positifs est plus simple à étudier.

\begin{theoreme}
Soit $(u_n)$ une suite à termes positifs. Alors la série $\sum u_n$ est convergente, si et seulement si, la suite des sommes partielles $(S_n)$ est majorée.	
\end{theoreme}


\begin{prof}
\preuve{Notons $\ds{S_n=\sum_{k=0}^n u_k}$. On a alors $S_{n+1}-S_n=u_{n+1}\geq 0$ : la suite $(S_n)$ est donc croissante. D'après les théorèmes sur les suites monotones, $(S_n)$ converge si et seulement si la suite $(S_n)$ est majorée.}
\end{prof}

\lignes{2}

		\subsection{Lien entre intégrale et série}
		
On dispose d'un lien fort entre série et intégrale généralisée :

\begin{theoreme}
[Critère de comparaison série-intégrale] Soit $n_0$ un entier. Soit $f:[n_0;+\infty[\rightarrow \R$ une fonction continue, positive et décroissante. Alors $\ds{\sum_{ n\geq n_0} f(n)}$ et $\ds{\int_{n_0}^{+\infty} f(t)dt}$ sont de même nature.\\
S'il y a convergence, on a alors
\[ \int_{n+1}^{+\infty} f(t)dt \leq \sum_{k=n+1}^{+\infty} f(k) \leq \int_{n}^{+\infty} f(t)dt \]
S'il y a divergence, on a alors
\[ \int_{n_0}^{n+1} f(t)dt \leq \sum_{k=n_0}^n f(k) \leq f({n_0})+\int_{n_0}^n f(t)dt\]	
\end{theoreme}

\preuve{$f$ étant décroissante, pour tout entier $n\geq n_0$, on a
\[ \forall~t\in [n;n+1]~\quad  f(n+1)\leq f(t) \leq f(n) \]
D'après l'inégalité de la moyenne :
\[  f(n+1)(n+1-n) \leq \int_n^{n+1} f(t)dt \leq f(n)(n+1-n) \]
soit
\[ f(n+1) \leq \int_n^{n+1} f(t)dt \leq f(n) \]
Pour $N\geq n_0$, en additionnant ces inégalités, nous avons alors
\[ \sum_{n=n_0}^{N} f(n+1) \leq \int_{n_0}^{N+1} f(t)dt \leq \sum_{n=n_0}^N f(n) \]
où encore, en notant $\ds{S_n=\sum_{n=n_0}^N f(n)}$, et après changement de variable :
\[ S_N-f(n_0) \leq \int_{n_0}^N f(t)dt \leq S_N \quad\quad(*)\]
ce qui permet de déduire que (par positivité de la fonction $f$) $(S_N)$ converge si et seulement si $\ds{\int_{n_0}^{+\infty} f(t)dt}$ converge.
Le résultat suivant convergence/divergence n'est ensuite qu'une ré-éecriture de $(*)$.

}

\remarque{Ce résultat permet d'obtenir un équivalent de la somme partielle ou du reste, selon qu'il y ait convergence ou divergence.}

\exercice{On a vu que $\ds{\sum_{n\geq 1}\frac{1}{n}}$ diverge. On note $\ds{H_n=\sum_{k=1}^n \frac{1}{k}}$.Montrer que $\ds{H_n\sim_{+\infty} \ln(n) }$.}

\begin{prof}
\solution{La fonction $f:t\mapsto \frac{1}{t}$ est continue, positive et décroissante sur $[1;+\infty[$. D'après le théorème précédent, l'intégrale diverge et on a, pour tout $n\geq 1$ :
\[ \int_1^{n+1} \frac{1}{t}dt \leq \sum_{k=1}^n \frac{1}{k} \leq 1+\int_1^n \frac{1}{t}dt\]
ce qui nous donne
\[ \ln(n+1) \leq H_n \leq 1+\ln(n) \]
et donc ($\ln(n)>0$)
\[ 1+\frac{\ln\left(1+\frac{1}{n}\right)}{\ln(n)}=\frac{\ln(n+1)}{\ln(n)} \leq \frac{H_n}{\ln n} \leq \frac{1}{\ln(n)}+1 \]
Par équivalence, les membres de gauche et droite tendent vers $1$. Par encadrement, la limite $\ds{\lim_{n\rightarrow +\infty} \frac{H_n}{\ln(n)}}$ existe et vaut $1$.
\\\textbf{Bilan} : $H_n\sim_{+\infty} \ln(n)$.
}	
\end{prof}


\lignes{15}

\section{Séries de référence}

    \subsection{Séries géométriques}
    
\definition{Pour tout entier $p$, la série $\sum q^n$ s'appelle \textbf{série géométrique} de raison $q$.}

\begin{theoreme}
La série $\sum q^n$ est convergente si et seulement si $|q|<1$. Dans ce cas, 
$$\sum_{n=0}^{+\infty} q^n=\frac{1}{1-q}$$	
\end{theoreme}


\remarque{Plus généralement, la série $\ds{\sum_{n\geq p} q^n}$ est convergente si et seulement si $|q|<1$, et dans ce cas, $\ds{\sum_{n=p}^{+\infty} q^n=\frac{q^p}{1-q}}$}

\begin{prof}
\preuve{La suite $(q^n)$ converge vers $0$ si et seulement si $|q|<1$. Par condition nécessaire de convergence, la série $\sum q^n$ ne peut pas converger si $|q|\geq 1$.\\
Supposons alors que $|q|<1$. Notons $\ds{S_n=\sum_{k=0}^n q^k}$. On a $\ds{S_n=\frac{1-q^{n+1}}{1-q}}$. Or, $\ds{\lim_{n\rightarrow +\infty} q^{n+1}=0}$ car $|q|<1$. Donc la suite $(S_n)$ converge vers $\frac{1}{1-q}$ : la série converge, et sa somme vaut $\frac{1}{1-q}$.}
\end{prof}

\lignes{5}
    
    \subsection{Séries de Riemann}
    
\definition{La série de terme général $\frac{1}{n^\alpha}$ ($\alpha \in \R$) est appelée \textbf{série de Riemann}.     }

\begin{theoreme}
La série de Riemann $\ds{\sum_{n\geq 1} \frac{1}{n^\alpha}}$ converge si et seulement si $\alpha >1$.	
\end{theoreme}



\preuve{On utilise le critère de comparaison série-intégrale. Remarquons tout d'abord que si $\alpha\leq 0$, le terme général ne tend pas vers $0$ et la série ne peut donc converge.\\
Pour $\alpha>0$, la fonction $f_\alpha:t \mapsto \frac{1}{t^\alpha}$ est continue, positive et décroissante sur $[1;+\infty[$. Ainsi, la série $\ds{\sum_{n\geq 1} \frac{1}{n^\alpha}}$ est de même nature que l'intégrale $\ds{\int_1^{+\infty} \frac{1}{t^\alpha}dt}$. Or, on a vu, dans le chapitre précédent, que cette intégrale converge si et seulement si $\alpha>1$. Ainsi, la série elle-même converge si et seulement si $\alpha>1$.
}


\remarque{Même si la série converge, on ne connait pas explicitement la valeur de la somme $\ds{\sum_{n\geq 1} \frac{1}{n^\alpha}}$ sauf dans certains cas particuliers.}
        

\section{Théorèmes de convergence}
	
		\subsection{Théorème de comparaison}
		
Pour majorer $(S_n)$, on commence en général par majorer $(u_n)$. On somme alors ces majorants pour en déduire un majorant de $(S_n)$. On dispose ainsi du théorème suivant :

\begin{theoreme}[Théorème de comparaison] 
Soient $(u_n)$ et $(v_n)$ deux suites \textbf{à termes positifs}. On suppose que pour tout $n$,
$$0 \leq u_n \leq v_n$$
Alors, si la série $\sum v_n$ est convergente, la série $\sum u_n$ est également convergente. Dans ce cas, 
$$\sum_{k=0}^{+\infty} u_k \leq \sum_{k=0}^{+\infty} v_k$$	
\end{theoreme}


\begin{prof}
\preuve{Si on note $\ds{S_n=\sum_{k=0}^n u_k}$ et $\ds{T_n=\sum_{k=0}^n v_k}$, on a, pour tout $n$, $S_n \leq T_n$ (addition des inégalités). De plus, la suite $(T_n)$ est également croissante, de limite $T$. Donc pour tout $n$, $T_n\leq T$. Donc
$$\forall~n, S_n \leq T_n \leq T$$
La suite $(S_n)$ est donc majorée, et d'après le théorème précédent, la série $\sum u_n$ converge. L'inégalité précédente donne alors 
$$\sum_{n=0}^{+\infty} u_n=\lim_{n\rightarrow +\infty} S_n \leq T=\lim_{n\rightarrow +\infty} T_n=\sum_{n=0}^{+\infty} v_n$$}
\end{prof}

\lignes{8}

\exemple{Soit $(u_n)$ une suite à termes positifs vérifiant, pour tout $n$, $\ds{u_n\leq \frac{1}{2^n}}$. Puisque la série $\ds{\sum \left(\frac{1}{2}\right)^n}$ est une série convergente, la série $\sum u_n$ est donc convergente, et on a $\ds{\sum_{n=0}^{+\infty} u_n\leq 2}$.}
    
On dispose également d'un critère de divergence :

\begin{theoreme}
Soient $(u_n)$ et $(v_n)$ deux suites à \textbf{termes positifs}. On suppose que pour tout $n$, $0\leq u_n \leq v_n$. Alors, si la série $\sum u_n$ diverge vers $+\infty$, alors la série $\sum v_n$ diverge également vers $+\infty$.	
\end{theoreme}


\exemple{Soit $(u_n)$ une suite à termes positifs vérifiant pour tout entier $n\geq 1$, $u_n\geq \frac{1}{n}$. Alors, puisque la série $\sum \frac{1}{n}$ est divergente, la série $\sum u_n$ est également divergente.} 
 
 
 		\subsection{Equivalence et négligeabilité}
 		
On peut enfin utiliser les équivalents :

%%% A FAIRE
\begin{theoreme}
Soient $(u_n)$ et $(v_n)$ deux suites \textbf{à termes positifs}. On suppose que $\ds{u_n \sim_{+\infty} v_n}$. 
Alors, la série $\sum v_n$ est convergente si et seulement si la série $\sum u_n$ est également convergente.	
\end{theoreme}

%%% A FAIRE 

\exemple{Montrer que $\ds{\sum_{n\geq 0} \frac{1}{2^n+3^n}}$ converge.}

\begin{prof}
\solution{Remarquons que \[ \frac{1}{2^n+3^n} \sim \frac{1}{3^n}\]
Puisque les deux suites sont à termes positifs, et que la série $\ds{\sum \frac{1}{3^n}}$ converge (série géométrique), on en déduit que par équivalent, la série $\ds{\sum_{n\geq 0} \frac{1}{2^n+3^n}}$ converge.
} 	
\end{prof}

\lignes{4}

On dispose également d'un critère en cas de négligeabilité :
\begin{theoreme}
Soient $(u_n)$ et $(v_n)$ deux suites \textbf{à termes positifs}. On suppose que $\ds{u_n =o_{+\infty}(v_n)}$. 
Si la série $\sum v_n$ est convergente, alors la série $\sum u_n$ est également convergente.	
\end{theoreme}


\exemple{Montrer que $\ds{\sum_{n\geq 0} \eu{-n^2}}$ est une série convergente.}

\begin{prof}
\solution{Remarquons que $\ds{\eu{-n^2}=o_{+\infty}\left(\frac{1}{n^2}\right)}$. En effet, \[ \mylim[n]{+\infty}{\frac{\eu{-n^2}}{1/n^2}}=\mylim[n]{+\infty}{n^2\eu{-n^2}} = 0 \text{ par croissances comparées}\]	
Les deux suites $(\eu{-n^2})$ et $\left(\frac{1}{n^2}\right)$ sont positives, et la série $\sum \frac{1}{n^2}$ est convergente (Riemann). Par comparaison de série à termes positifs, on en déduit que la série $\sum \eu{-n^2}$ converge.}
\end{prof}


\lignes{5}


\section{Convergence absolue}

    \subsection{Définition}
    
\definition{Soit $(u_n)$ une suite à valeurs dans $\K$. On dit que la série $\sum u_n$ est \textbf{absolument convergente} si la série $\sum |u_n|$ est convergente.}

\exemple{La série $\ds{\sum_{n\geq 1} \frac{(-1)^n}{n^2}}$ est absolument convergente : en effet, la série $\ds{\sum_{n\geq 1} \left|\frac{(-1)^n}{n^2}\right| = \sum_{n\geq 1} \frac{1}{n^2}}$ est convergente (série de Riemann).}
    
    \subsection{Absolue convergence et convergence}
    
\begin{theoreme}
Soit $(u_n)$ une suite à valeurs dans $\K$. Si la série $\sum u_n$ est absolument convergente, alors elle est convergente.	
\end{theoreme}


\remarque{Pour démontrer qu'une série de signe quelconque est convergente, il peut ainsi être judicieux de montrer qu'elle est absolument convergente, et se ramener donc à une série à termes positifs.}  
    
    \subsection{Convergence et absolue convergence}

\remarque{La réciproque n'est pas vraie : une série peut être convergente sans être absolument convergente (on dit que la série est \textbf{semi-convergente}). ~\\Par exemple, la série $\ds{\sum_{n\geq 1} \frac{(-1)^n}{n}}$ est convergente, mais n'est pas absolument convergente, puisque la série $$\ds{\sum_{n\geq 1} \left| \frac{(-1)^n}{n}\right| = \sum_{n\geq 1} \frac{1}{n}}$$ n'est pas convergente.}  

	\subsection{Séries alternées}

Un exemple de séries classiques qui ne converge pas forcément absolument, mais qui converge : les séries alternées.

\definition{Soit $(u_n)$ une suite positive, décroissante de limite nulle. La série $\ds{\sum_{n\geq 0} (-1)^n u_n}$ est appelée \textbf{série alternée}.
}

\remarque{De manière plus générale, on appelle série alternée une série $\ds{\sum u_n}$ où le terme générale $(|u_n|)$ est décroissante de limite nulle. Ainsi, si $(u_n)$ est croissante et négative, de limite nulle, $(|u_n|)$ est croissante, positive de limite nulle.}

\begin{theoreme}[Théorème spécial des séries alternées] 
Soit $(u_n)$ une suite positive, décroissante, de limite nulle. Alors la série alternée $\ds{\sum_{n\geq 0} (-1)^nu_n}$ converge.	
\end{theoreme}

\preuve{L'idée est de poser deux suites, $a$ et $b$ définies par $\ds{a_n=\sum_{k=0}^{2n} u_k}$ et $\ds{b_n=\sum_{k=0}^{2n+1} u_k}$. On montre alors que $(a_n)$ et $(b_n)$ sont adjacentes, donc ont la même limite. Or, si une suite $(c_n)$ vérifie que $(c_{2n})$ et $(c_{2n+1})$ convergent vers la même limite, alors la suite $(c_n)$ elle-même converge.}

\exercice{Montrer que la série $\ds{\sum_{n\geq 0} \frac{(-1)^n}{2^n}}$ converge. Montrer que la série $\ds{\sum_{n\geq 1} \frac{(-1)^n}{n}}$ est convergente, mais pas absolument convergente.}

\begin{prof}
\solution{La suite définie pour tout $n$ par $u_n=\frac{1}{2^n}$ est positive, décroissante, de limite nulle. Par théorème des séries alternées, la série $\ds{\sum_{n\geq 0} \frac{(-1)^n}{2^n}}$ converge. De même, la suite $v$ définie pour tout $n\geq 1$ par $v_n=\frac{1}{n}$ est une suite positive, décroissante de limite nulle. Par le même théorème, la série $\ds{\sum_{n\geq 1}\frac{(-1)^n}{n}}$ converge. En revanche, \[ \left | \frac{(-1)^n}{n}\right|= \frac{1}{n}\]
et la série $\ds{\sum_{n\geq 1}\frac{1}{n}}$ diverge.

}	
\end{prof}


\lignes{8}


    \subsection{Etudier une série}

\begin{methode}
Pour étudier une série $\sum u_n$, on suit différentes étapes :
\begin{enumerate}
    \item On vérifie si la suite $(u_n)$ tend vers $0$. Si non, la série est divergente.
    \item On pose la suite des sommes partielles $(S_n)$ et on vérifie si on peut la calculer. Si oui, on peut conclure quant à la convergence, et la valeur de la somme le cas échéant.
    \item On vérifie si on peut se ramener à une série de référence (géométrique, Riemann, exponentielle), éventuellement par équivalence. Si oui,  on peut conclure quant à la convergence, et la valeur de la somme le cas échéant.
    \item On essaie de voir si la série n'est pas alternée, en s'intéressant à $(|u_n|)$.
    \item Si tout ce qui précède n'a pas abouti, on essaie de majorer (ou minorer) les sommes partielles $(S_n)$ si la série est à termes positifs, ou alors on s'intéresse à l'absolue convergence sinon. 
\end{enumerate}	
\end{methode}


\remarque{On ne peut pas forcément calculer la somme de la série, même si on arrive à prouver que la série converge.}
%
%\exemple{Soit $u$ la suite définie pour $n\geq 1$ par $\ds{u_n=\frac{1}{n^2(n^2+1)}}$. Etudier la nature de la suite $\ds{\sum_{n\geq 1} u_n}$}
%
%%%% BEAUCOUP PLUS SIMPLE : n^2(n^2+1)=n^4+n^2>= n^4 donc 0<=u_n<=1/n^2
%
%\begin{prof}
%\solution{Remarquons tout d'abord que la suite $(u_n)$ converge vers $0$. On peut écrire $u_n$ sous la forme 
%$$u_n=\frac{1}{n^2}-\frac{1}{n^2+1}=v_n-w_n$$
%Or, la série $\ds{\sum_{n\geq 1} v_n}$ converge (série de Riemann). De plus, pour tout $n\geq 1$,
%$$0<w_n\leq \frac{1}{n^2}=v_n$$
%Par comparaison, $(w_n)$ étant à termes positifs et la série $\ds{\sum_{n\geq 1} v_n}$ étant convergente, on en déduit que la série $\ds{\sum_{n\geq 1} w_n}$ converge également.\\
%Par somme, la série $\ds{\sum_{n\geq 1} u_n}$ converge également, et 
%$$\sum_{n\geq 1} \frac{1}{n^2(n^2+1)} = \sum_{n\geq 1} \frac{1}{n^2} - \sum_{n\geq 1} \frac{1}{n^2+1}$$
%}
%\end{prof}
%
%
%\lignes{8}