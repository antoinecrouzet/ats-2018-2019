/// Résolution de l'équation différentielle y'=-2y
/// Avec y(0)=1
/// Principe : on part d'un point, et on avance par pas (petit nombre) en utilisant l'approximation affine f(x+h)=f(x)+hf'(x)
// On utilise alors l'équation différentielle pour remplacer f' par sa valeur.


// Pas entre chaque point
pas=0.001
// Nombre de point
N=1000
// Ainsi, on va de 0 à 0.001*10000=10

// Point de départ
//f(0)=1
x(1)=0
y(1)=1

// On calcule les N points
for i=1:N
    // De pas en pas
    x(i+1)=x(i)+pas
    // On utilise f(x+h)=f(x)+hf'(x)
    // avec f'=2f
    y(i+1)=y(i)+pas*(-2)*y(i)
end

clf
plot(x,y,'b')
//plot(x, exp(-2*x),'g')
xs2pdf(0,"23_courbe_euler.pdf");
