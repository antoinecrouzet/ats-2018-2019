\objectif{Dans ce chapitre, on revient sur la notion de déterminant, vue dans les chapitres de Géométrie du plan et de l'espace, que l'on étend à l'ensemble des matrices carrées. On utilisera ensuite le déterminant pour démontrer des résultats (inversibilité de matrices, bijectivité d'application linéaire).}

\textit{La liste ci-dessous représente les éléments à maitriser absolument. Pour cela, il faut savoir refaire les exemples et exercices du cours, ainsi que ceux de la feuille de TD.}

\begin{numerote}
	\item Connaître la définition de déterminant :
			\begin{itemize}[label=\textbullet]
				\item connaître les propriétés de la définition\dotfill $\Box$
				\item savoir calculer à l'aide du développement selon ligne ou colonne\dotfill $\Box$
				\item savoir calculer à l'aide des opérations élémentaires\dotfill $\Box$
			\end{itemize}
	\item Concernant les propriétés du déterminant :
			\begin{itemize}[label=\textbullet]
				\item connaître les conditions d'un déterminant nul\dotfill $\Box$
				\item maîtriser les propriétés du déterminant d'un produit et d'une transposée\dotfill $\Box$
				\item savoir calculer le déterminant d'une matrice par bloc\dotfill $\Box$
			\end{itemize}
	\item Savoir utiliser le déterminant pour montrer qu'une matrice est inversible\dotfill $\Box$
	\item Connaître le lien avec endomorphisme et famille de vecteurs :
			\begin{itemize}[label=\textbullet]
				\item savoir calculer le déterminant d'une famille de vecteurs\dotfill $\Box$
				\item savoir démontrer qu'une famille est une base à l'aide du déterminant\dotfill $\Box$
				\item savoir calculer le déterminant d'un endomorphisme\dotfill $\Box$
				\item savoir montrer qu'un endomorphisme est un automorphisme à l'aide du déterminant\dotfill $\Box$
			\end{itemize}
\end{numerote}

\newpage 

Dans l'ensemble de chapitre, $\K$ désignera $\R$ ou $\C$.

%%%%%%%%%%%%%%%%%%%%%
\section{Déterminant}
%%%%%%%%%%%%%%%%%%%%%

	%%%%%%%%%%%%%%%%%%%%%%%
	\subsection{Définition}
	%%%%%%%%%%%%%%%%%%%%%%%
	
Nous avons vu dans les chapitres Géométrie élémentaire du plan et Géométrie élémentaire de l'espace la notion de déterminant de deux vecteurs du plan ou de trois vecteurs de l'espace. Nous allons généraliser cette notation.

\remarque{Une matrice $A=(a_{ij})_{\substack{1\leq i \leq n\\1\leq j\leq n}}$ de $\MM_n(\K)$ peut aussi se noter $(C_1,\cdots, C_n)$ où $C_i$ désigne la colonne $i$.}

\begin{proposition}
Soit $n\geq 1$. Il existe une unique application $\det:\MM_n(\K)\rightarrow \K$, appelée \textbf{déterminant}, vérifiant les trois propriétés suivantes :
\begin{itemize}[label=\textbullet]
	\item $\det$ est \textbf{linéaire} par rapport à chacune des colonnes :
	\[ \det(\lambda C_1+C'_1,C_2,\cdots, C_n) = \lambda \det(C_1,C_2,\cdots,C_n)+\det(C_1',C_2,\cdots,C_n) \]
	(et de même pour les autres colonnes).
	\item $\det$ est \textbf{alterné} : lorsqu'on permute deux colonnes, le déterminant change de signe, par exemple 
		\[ \det(C_2,C_1,C_3,\cdots, C_n)=-\det(C_1,C_2,C_3,\cdots,C_n) \]
	\item Enfin $\det(I_n)=1$.
\end{itemize}	
\end{proposition}


\exemple{Le déterminant d'une matrice $\matrice{a&b\\c&d}$ correspond au déterminant dans le plan des vecteurs $\matrice{a\\c}$ et $\matrice{b\\d}$. De même dans l'espace.}

\remarque{On dit que le déterminant est une forme multilinéaire alternée.}

	%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
	\subsection{Développement par rapport aux lignes et colonnes}
	%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
	
\definition{[Matrice extraite] Soit $A\in \MM_n(\K)$ une matrice carrée d'ordre $n$. Soient $i$ et $j$ deux entiers de $\ll 1;n \rr$. On note $A_{ij}\in \MM_{n-1}(\K)$ la matrice extraite de $A$, obtenue en supprimant la ligne $i$ et la colonne $j$.}

\exemple{Soit $A=\matrice{1&2&3\\4&5	&6\\7&8&9}$. Déterminer $A_{1,1}$, $A_{2,1}$ et $A_{3,2}$.}

\begin{prof}
\solution{En utilisant la définition :
\[ A_{1,1}=\matrice{5&6\\8&9},\quad A_{2,1}=\matrice{2&3\\8&9} \qeq A_{3,2}=\matrice{1&3\\4&6} \]	
}
\end{prof}

\lignes{4}

\begin{proposition}[Développement par rapport à une ligne] 
Soit $A$ une matrice de $\MM_n(\K)$. On peut calculer le déterminant de $A=\left(a_{ij}\right)_{\substack{1\leq i\leq n\\1\leq j \leq n}}$,  par récurrence :
\begin{itemize}[label=\textbullet]
	\item Si $n=1$, $\det(A)=a_{11}$ où $A=(a_{11})$.
	\item Si $n\geq 2$, 
		\[ \det(A)=\sum_{j=1}^n (-1)^{i+j}a_{ij}\det(A_{ij}) \]
\end{itemize}
Si $A=\matrice{a_{11}&a_{12}&\hdots & a_{1n}\\a_{21}&a_{22}&\hdots & a_{2n}\\\vdots & \vdots & &\vdots\\a_{n1}&a_{n2}&\hdots&a_{nn}}$, on notera
$\det(A)=\determinant{a_{11}&a_{12}&\hdots & a_{1n}\\a_{21}&a_{22}&\hdots & a_{2n}\\\vdots & \vdots & &\vdots\\a_{n1}&a_{n2}&\hdots &a_{nn}}$.	
\end{proposition}

\exemple{Vérifier que $\determinant{a&b\\c&d}=ad-bc$. Déterminer $\determinant{1&1&0\\2&0&1\\1&1&0}$.}

\begin{prof}
\solution{En appliquant la définition
\[ \determinant{a&b\\c&d} = (-1)^{1+1}a\determinant{d}+(-1)^{2+1}b\determinant{c}=ad-bc \]
De même
\[ \determinant{1&1&0\\2&0&1\\1&1&0}=1\determinant{0&1\\1&0}-1\determinant{2&1\\1&0}+0\determinant{2&0\\1&1}=0 \]
}	
\end{prof}

\lignes{4}

\begin{proposition}[Développement par rapport à une colonne] 
Soit $A$ une matrice de $\MM_n(\K)$. On peut calculer le déterminant de $A=\left(a_{ij}\right)_{\substack{1\leq i\leq n\\1\leq j \leq n}}$,  par récurrence :
\begin{itemize}[label=\textbullet]
	\item Si $n=1$, $\det(A)=a_{11}$ où $A=(a_{11})$.
	\item Si $n\geq 2$, 
		\[ \det(A)=\sum_{i=1}^n (-1)^{i+j}a_{ij}\det(A_{ij}) \]
\end{itemize}	
\end{proposition}

\begin{proposition}
Soit $T=\matrice{a_{11}&a_{12}&\hdots & a_{1 n-1}& a_{1n}\\0&a_{22}&\hdots & a_{2 n-1}& a_{2n}\\\vdots & \vdots & &\vdots\\0&0&\hdots&0&a_{nn}}$ une matrice triangulaire supérieure. Alors \[\det(T)=a_{11}\times a_{22}\times\hdots\times a_{nn} \]	
\end{proposition}


\preuve{On procède par récurrence, en développant suivant la première colonne}

	%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
	\subsection{Opérations élémentaires}
	%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
	
Pour calculer en pratique un déterminant, on peut utiliser les opérations élémentaires (celles de la méthode de Gauss-Jordan) :

\begin{proposition}
Soit $A \in \MM_n(\K)$.
\begin{itemize}[label=\textbullet]
	\item Si on multiplie une colonne ou une ligne de $A$ par $\alpha$, le déterminant est également multiplié par $\alpha$.
	\item Si on ajoute à une colonne de $A$ un multiple d'une autre colonne, le déterminant de $A$ ne change pas.
	\item Si on ajoute à une ligne de $A$ un multiple d'une autre ligne, le déterminant de $A$ ne change pas.
	\item Si on échange deux lignes (ou deux colonnes) de $A$, on multiplie son déterminant par $-1$. 
\end{itemize}	
\end{proposition}


\preuve{Les opérations élémentaires reviennent à multiplier la matrice $A$ par des matrices du type 
$t_{ij}(\alpha) = I_n +\alpha E_{ji}$ (matrice de transvection pour $i\neq j$) de déterminant $1$, ou par $e_{ij}=I_n+E_{ij}+E_{ji}-E_{ii}-E{jj}$ (pour l'échange), de déterminant $-1$.}

\exercice{Calculer $\determinant{1&\alpha & \alpha^2\\1&\beta&\beta^2\\1&\gamma&\gamma^2}$.}

\begin{prof}
\solution{En soustrayant la première ligne à la deuxième et à la troisième (ce qui ne change pas le déterminant) :
\[ \determinant{1&\alpha&\alpha^2\\1&\beta&\beta^2\\1&\gamma&\gamma^2} = \determinant{1&\alpha&\alpha^2\\0&\beta-\alpha&\beta^2-\alpha^2\\0&\gamma-\alpha&\gamma^2-\alpha^2} \]
En développant par rapport à la première colonne :
\[ \determinant{1&\alpha&\alpha^2\\1&\beta&\beta^2\\1&\gamma&\gamma^2} = \determinant{\beta-\alpha&\beta^2-\alpha^2\\\gamma-\alpha&\gamma^2-\alpha^2} = (\beta-\alpha)(\gamma^2-\alpha^2)-(\gamma-\alpha)(\beta^2-\alpha^2) \]
Soit, après factorisation
\[ \determinant{1&\alpha&\alpha^2\\1&\beta&\beta^2\\1&\gamma&\gamma^2} = (\beta-\alpha)(\gamma-\alpha)(\gamma+\alpha) - (\gamma-\alpha)(\beta-\alpha)(\beta+\alpha)=(\beta-\alpha)(\gamma-\alpha)(\gamma-\beta)\]
}	
\end{prof}

\lignes{11}

\remarque{Ce type de déterminant est appelé \textbf{déterminant de Vandermonde}. Cette méthode est générale pour calculer ces déterminants.}

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
\section{Déterminant et opération sur les matrices}
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

	%%%%%%%%%%%%%%%%%%%%%%%%%%%%
	\subsection{Déterminant nul}
	%%%%%%%%%%%%%%%%%%%%%%%%%%%%

\begin{proposition}
Soit $A \in \MM_n(\K)$. 
\begin{itemize}[label=\textbullet]
	\item Si $A$ a une colonne (ou une ligne) nulle, $\det(A)=0$.
	\item Si une colonne de $A$ est combinaison linéaire des autres, $\det(A)=0$.
	\item Si $A$ n'est pas inversible, $\det(A)=0$.
	\item Si $\rg(A)\leq n-1$, $\det(A)=0$.
\end{itemize}	
\end{proposition}


\preuve{Les trois derniers points sont équivalents (cf. chapitre \textit{Espaces vectoriels en dimension finie}). Le premier point découle de la multilinéarité du déterminant : en supposons la colonne $C_1$ nulle (par exemple), on peut alors écrire
\[ \det(A)=\det(0,C_2,\cdots, C_n) = \det(0+0, C_2,\cdots , C_n = \det(0,C_2,\cdots, C_n)+\det(0,C_2,\cdots, C_n) \]
ainsi, $\det(A)=\det(A)+\det(A)$ et donc $\det(A)=0$.}

\exemple{Calculer le déterminant de $A=\matrice{1&a&b+c\\1&b&a+c\\1&c&a+b}$.}

\begin{prof}
\solution{On remarque, en notant $C_1, C_2, C_3$ les trois colonnes de $A$ que
\[ (a+b+c)C_1-C_2=C_3 \]
Ainsi, $C_3$ s'écrit comme une combinaison linéaire de $C_1$ et $C_2$ : $\det(A)=0$.
}	
\end{prof}

\lignes{5}

	%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
	\subsection{Produit et transposition}
	%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
	
\begin{proposition}[Multiplicativité du déterminant] 
Soient $A$ et $B$ deux matrices de $\MM_n(\K)$. Alors $\det(AB)=\det(A)\det(B)$.	
\end{proposition}


\preuve{Admise.}

\begin{proposition}
Soit $A\in \MM_n(\K)$ et $\lambda \in \K$. Alors
\[ \det(\lambda A)=\lambda^n \det(A) \]	
\end{proposition}


\preuve{Notons $(C_1,\cdots, C_n)$ les colonnes de $A$. Mais alors
\[ \lambda A = (\lambda C_1, \lambda C_2,\cdots, \lambda C_n) \]
et par multilinéarité
\[ \det(\lambda A) = \underbrace{\lambda \times \cdots\times \lambda}_{n\text{ fois}} \det(C_1,\cdots, C_n)= \lambda^n \det(A) \]
}

\begin{proposition}[Invariance par transposition] 
Soit $A\in \MM_n(\K)$. On a $\det(\,^tA)=\det(A)$. Ainsi, toute propriété du déterminant valable sur les colonnes est également valable sur les lignes.	
\end{proposition}


\preuve{Vient du fait que le développement suivant les lignes ou les colonnes donne le même déterminant.}

	%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
	\subsection{Matrices inversibles}
	%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
	
Le résultat suivant est très important, et déjà vu dans le cas des matrices de $\MM_2(\K)$.

\begin{theoreme}
Soit $A \in \MM_n(\K)$. Alors $A$ est inversible si et seulement si $\det(A)\neq 0$.	
\end{theoreme}


\preuve{Si $A\in \MM_n(\R)$ est inversible, il existe $B$ telle que $AB=I_n$. Ainsi, $\det(AB)=1$. Or, $\det(AB)=\det(A)\det(B)$, et donc $\det(A)\det(B)=1$ : $\det(A)$ ne peut être nul.\\
Si $A$ n'est pas inversible, alors $\rg(A)\leq n-1$	et d'après une propriété précédente, $\det(A)=0$.}
 
\remarque{La démonstration précédente permet de montrer également que si $A$ est inversible, alors
\[ \det\left(A^{-1}\right)=\frac{1}{\det(A)} \]}

	%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
	\subsection{Matrice par bloc}
	%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
	
\begin{proposition}
Soit $A\in \MM_n(\K)$, $B\in \MM_{n,p}(\K)$ et $D\in \MM_{p}(K)$. On note $L \in \MM_{n+p}(\K)$ la matrice par blocs définie par 
\[ L = \matrice{A & B \\O_{p,n}&D} \]
Alors
\[ \det(L) = \det(A) \times \det (D) \]	
\end{proposition}


\preuve{La preuve se fait par récurrence sur la taille de $A$.}

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
\section{Applications aux endomorphismes et aux vecteurs}
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

	%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
	\subsection{Déterminant d'une famille de vecteurs}
	%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
	
\definition{Soit $E$ un $\K$-espace-vectoriel de dimension $n$, et $\mathcal{B}=(e_1,\cdots, e_n)$ une base de $E$. Soit $(u_1,\cdots, u_n)$ une famille de $n$ vecteurs de $E$. On appelle \textbf{déterminant} de $(u_1,\cdots,u_n)$ dans la base $\mathcal{B}$ le déterminant de la matrice dont les colonnes sont les coordonnées des $u_i$ dans la base $\mathcal{B}$ :
\[ \det_{\mathcal{B}}(u_1,\cdots,u_n) = \det\left(\Mat_{\mathcal{B}}(u_1),\cdots,\Mat_{\mathcal{B}}(u_n)\right)\] 
}

\exemple{Soit $((1,1), (1,-1))$ une famille de vecteurs de $\R^2$. Déterminer le déterminant de cette famille dans la base canonique.}

\begin{prof}
AF	
\end{prof}

\lignes{5}

\begin{proposition}
Soit $E$ un $\K$-espace-vectoriel de dimension $n$, et $\mathcal{B}=(e_1,\cdots, e_n)$ une base de $E$. Soit $(u_1,\cdots, u_n)$ une famille de $n$ vecteurs de $E$. \\$(u_1,\cdots, u_n)$ est une base de $E$ si et seulement si $\det_{\mathcal{B}}(u_1,\cdots,u_n)\neq 0$.	
\end{proposition}


	%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
	\subsection{Déterminant d'un endomorphisme}
	%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
	
\propriete{Soient $A$ et $B$ deux matrices semblables de $\MM_n(\K)$. Alors $\det(A)=\det(B)$.}

\preuve{En effet, il existe $P\in GL_n(\K)$ telle que $A=PBP^{-1}$. Mais alors, par multiplicativité du déterminant :
\[ \det(A)=\det(PBP^{-1})=\det(P)\det(B)\det(P^{-1})=\det(B) \]
puisque $\det(P^{-1})=\frac{1}{\det(P)}$.}

La propriété précédente permet de définir la notion de déterminant d'endomorphisme :

\definition{Soit $E$ un $\K$-espace vectoriel de dimension finie. Soit $f$ un endomorphisme de $E$. On appelle \textbf{déterminant} de $f$, et on note $\det(f)$, le déterminant de la matrice de $f$ dans une base quelconque de $E$ : \[ \det(f)=\det\left(\Mat_{\mathcal{B}}(f)\right) \text{ où } \mathcal{B} \text{ est une base de } E\]
La propriété précédente nous assure que cela ne dépend pas de la base choisie.
}
 
On dispose d'une caractérisation d'un automorphisme :

\begin{proposition}
Soit $E$ un $\K$-espace vectoriel de dimension finie. Soit $f$ un endomorphisme de $E$. $f$ est un automorphisme si et seulement si $\det(f)\neq 0$.	
\end{proposition}


\preuve{En effet, $f$ est un automorphisme si et seulement si la matrice de $f$ dans une base est inversible.}