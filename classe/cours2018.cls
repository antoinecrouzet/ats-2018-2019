\NeedsTeXFormat{LaTeX2e}

% Une page
\LoadClass[11pt]{book}
\usepackage{geometry}

%% Deux pages
%\LoadClass[11pt,twocolumn]{book}
%\usepackage[landscape]{geometry}

\geometry{a4paper,
	  left=2cm,
	  right=2.1cm,
	  vmargin=2.5cm
}

\input{preambule_cours.tex}
\input{script_scilab.tex}
